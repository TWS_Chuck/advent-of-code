const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day6.txt", 'utf-8');
let lines = inputString.split('\n');


for (var i = 0; i < lines.length; i++) {

    let responses = {};

    while (lines[i] != "") {
        for (var j = 0; j < lines[i].length; j++) {
            responses[lines[i].charAt(j)] = true;
        }
        i++;
    }

    result += Object.keys(responses).length;
}

console.log("Result is ", result);
