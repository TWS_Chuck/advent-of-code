const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day9.txt", 'utf-8');
let lines = inputString.split('\n');

let preamble = 25;

// for example /([^x]*)x([^x]*)x([^x]*)/;
let regexp = /./;

let previousValueIndex = preamble - 1;
let previousValues = {};

for (var i = 0; i < preamble; i++) {
    previousValues[i] = parseInt(lines[i]);
}

for (var i = preamble; i < lines.length; i++) {
    let value = parseInt(lines[i]);

    if (!doPreviousValuesAddUpToValue(value)) {
        console.log("Found it, first value that breaks is", value);
        calculateRange(value);
        return;
    } else {
        previousValues[i] = value;
        previousValueIndex++;
    }
}

function doPreviousValuesAddUpToValue(value) {
    for (var i = previousValueIndex - preamble; i <= previousValueIndex; i++) {
        for (var j = previousValueIndex - preamble; j <= previousValueIndex; j++) {
            let first = previousValues[i];
            let second = previousValues[j];
            if (first + second == value) {
                return true;
            }
        }
    }

    return false;
}

function calculateRange(value) {

    for (var i = 0; i <= lines.length; i++) {
        let smallestValue = 99999999999999999999;
        let largestValue = 0;
        let currentSum = 0;
        for (var j = i; j <= lines.length; j++) {
            currentSum += previousValues[j];

            if (previousValues[j] > largestValue) {
                largestValue = previousValues[j];
            }
            if (previousValues[j] < smallestValue) {
                smallestValue = previousValues[j];
            }

            if (currentSum > value) {
                break;
            }

            if (currentSum == value) {
                console.log("got to", value, i, j, "with this stuff", smallestValue, largestValue, smallestValue + largestValue, "and this adds to", currentSum);
            }
        }
    }

    return false;
}