const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day10.txt", 'utf-8');
let lines = inputString.split('\n');

let jolts = {};
for (let i = 0; i < lines.length; i++) {
    let joltValue = parseInt(lines[i]);
    jolts[joltValue] = true;
}
// jolts.push(0);
// jolts.push(174);

// jolts.sort((a, b) => {
//     return a - b;
// });

let memo = {};
result = waysToReachEnd(jolts, 0, 174);

console.log(result);

function waysToReachEnd(joltChain, startValue, finalValue) {
    console.log(startValue);
    if (memo[startValue] !== undefined) {
        return memo[startValue];
    }
    if (startValue == finalValue) {
        return 1;
    }

    let possibilities = 0;
    for (let i = startValue + 1; i <= startValue + 3; i++) {
        if (joltChain[i] !== undefined) {
            possibilities += waysToReachEnd(joltChain, i, finalValue);
        }
    }

    memo[startValue] = possibilities;

    return possibilities;
}