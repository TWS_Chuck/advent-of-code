const fs = require('fs');
const Passport = require('./passport.js');

let result = 0;

let inputString = fs.readFileSync("day4.txt", 'utf-8');
let lines = inputString.split('\n');

// for example /([^x]*)x([^x]*)x([^x]*)/;
let regexp = /^(.*):(.*)$/;

let i = 0;
while (i < lines.length) {
    let passportDetails = {};

    while (lines[i] !== "" && i < lines.length) {
        let regexResult = lines[i].match(regexp);
        passportDetails[regexResult[1]] = regexResult[2];
        i++;
    }

    i++;

    let newPassport;
    try {
        newPassport = new Passport(passportDetails);
    } catch (e) {
        console.log(e);
        continue;
    }

    result++;
}

console.log("Result is ", result);
