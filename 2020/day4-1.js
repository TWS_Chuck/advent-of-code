const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day4.txt", 'utf-8');
let lines = inputString.split('\n');

// for example /([^x]*)x([^x]*)x([^x]*)/;
let regexp = /^(.*):(.*)$/;


let i = 0;
while (i < lines.length) {
    let expectation = {
        byr: false,
        iyr: false,
        eyr: false,
        hgt: false,
        hcl: false,
        ecl: false,
        pid: false,
    }

    while (lines[i] !== "" && i < lines.length) {
        let regexResult = lines[i].match(regexp);
        expectation[regexResult[1]] = true;
        i++;
    }

    console.log(i);
    console.log(expectation);

    if (expectation["byr"] && expectation["iyr"] && expectation["eyr"] && expectation["hgt"] && expectation["hcl"] && expectation["ecl"] && expectation["pid"]) result++;

    i++;
}

console.log("Result is ", result);