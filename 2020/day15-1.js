const fs = require('fs');

let result = 0;

let inputString = "0,14,1,3,7,9";
let lines = inputString.split(',');

let memory = {};
let lastNumberInserted = "";

for (let i = 0; i < lines.length; i++) {
    memory[lines[i]] = {
        "mostRecentTime": i,
        "timeBefore": -1,
    };
    lastNumberInserted = lines[i];
    console.log("number", i, lines[i]);
}

for (let i = lines.length; i < 2020; i++) {
    let nextNumber = -1;

    if (memory[lastNumberInserted]["timeBefore"] === -1) {
        nextNumber = "0";
    } else {
        nextNumber = memory[lastNumberInserted]["mostRecentTime"] - memory[lastNumberInserted]["timeBefore"];
    }

    console.log("number", lastNumberInserted, i, nextNumber);
    if (memory[nextNumber] === undefined) {
        memory[nextNumber] = {
            "mostRecentTime": i,
            "timeBefore": -1,
        }
    } else {
        memory[nextNumber]["timeBefore"] = memory[nextNumber]["mostRecentTime"];
        memory[nextNumber]["mostRecentTime"] = i;
    }

    lastNumberInserted = nextNumber;
}

console.log("Result is ", result);
