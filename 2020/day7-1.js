const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day7.txt", 'utf-8');
let lines = inputString.split('\n');

// for example /([^x]*)x([^x]*)x([^x]*)/;
let regexp = /(.*) bags contain (.*)./;
let detailregexp = /(\d+) (.*) bag/;

let bagSchema = {};

for (var i = 0; i < lines.length; i++) {
    let regexResult = lines[i].match(regexp);
    let bagUnderQuestion = regexResult[1];
    let details = regexResult[2].split(',');

    if (bagSchema[bagUnderQuestion] == undefined) bagSchema[bagUnderQuestion] = {};

    details.forEach(detail => {
        let detailRegex = detail.match(detailregexp);
        if (detailRegex != null) {
            bagSchema[bagUnderQuestion][detailRegex[2]] = detailRegex[1];
        }
    })
}

let memo = {};

for (var key in bagSchema) {
    canThisBagHoldAShinyGoldBag(bagSchema, key);
}

for (var key in memo) {
    if (memo[key]) {
        result++;
    }
}

console.log("Result is ", result);

function canThisBagHoldAShinyGoldBag(bagSchema, bag) {
    for (var bagContained in bagSchema[bag]) {
        if (bagContained == "shiny gold") {
            memo[bag] = true;
            return true;
        } else if (memo[bag] != undefined && memo[bag]) {
            memo[bag] = true;
            return true;
        } else if (memo[bag] != undefined && !memo[bag]) {
            continue;
        } else if (canThisBagHoldAShinyGoldBag(bagSchema, bagContained)) {
            memo[bag] = true;
            return true;
        }
    }

    if (memo[bag] == undefined) {
        memo[bag] = false;
        return false;
    } else {
        return memo[bag];
    }
}