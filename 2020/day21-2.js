const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day21.txt", 'utf-8');
let lines = inputString.split('\n');

// for example /([^x]*)x([^x]*)x([^x]*)/;
let regexp = /(.*) contains (.*)/;

let ingredientTotalCount = {};
let ingredientAllergenRelation = {};
let allergenCount = {};
for (let i = 0; i < lines.length; i++) {
    let regexResult = lines[i].match(regexp);

    let ingredients = regexResult[1].split(' ');
    let allergens = regexResult[2].split(' ');

    for (let i in allergens) {
        if (allergenCount[allergens[i]] === undefined) allergenCount[allergens[i]] = 0;
        allergenCount[allergens[i]]++;
    }

    for (let i in ingredients) {
        if (ingredientAllergenRelation[ingredients[i]] === undefined) ingredientAllergenRelation[ingredients[i]] = {};
        if (ingredientTotalCount[ingredients[i]] === undefined) ingredientTotalCount[ingredients[i]] = 0;
        ingredientTotalCount[ingredients[i]]++;

        for (let j in allergens) {
            if (ingredientAllergenRelation[ingredients[i]][allergens[j]] === undefined) ingredientAllergenRelation[ingredients[i]][allergens[j]] = 0;
            ingredientAllergenRelation[ingredients[i]][allergens[j]]++;
        }
    }
}

console.log(allergenCount);

let potentialAllergens = {};
for (let ingredient in ingredientAllergenRelation) {
    let ingCount = ingredientAllergenRelation[ingredient];

    for (let i in allergenCount) {
        let allergenOccurrence = allergenCount[i];
        let ingAllergenCount = ingCount[i];

        if (allergenOccurrence === ingAllergenCount) {
            if (potentialAllergens[ingredient] === undefined) potentialAllergens[ingredient] = [];
            console.log("This could be it", i);
            potentialAllergens[ingredient].push(i);
        }
    }
}

console.log(potentialAllergens);

console.log("Result is ", result);
