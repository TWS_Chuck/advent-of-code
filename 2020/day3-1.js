const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day3.txt", 'utf-8');
let lines = inputString.split('\n');

let map = {};

let lineLength = lines[0].length;

let currentX = 3;

for (var i = 1; i < lines.length; i++) {
    if (lines[i].charAt(currentX) == '#') result++;

    currentX = (currentX + 3) % lineLength;
}

console.log("Result is ", result);