const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day5.txt", 'utf-8');
let lines = inputString.split('\n');

let maxSeatID = 0;

for (var i = 0; i < lines.length; i++) {
    let rowValue = 0;

    for (var j = 0; j < 7; j++) {
        let powerOfTwo = 2 ** (6 - j);

        if (lines[i].charAt(j) === "B") {
            rowValue += powerOfTwo;
        }
    }

    let seatValue = 0;

    for (var j = 7; j < 10; j++) {
        let powerOfTwo = 2 ** (9 - j);

        if (lines[i].charAt(j) === "R") {
            seatValue += powerOfTwo;
        }
    }

    let seatID = rowValue * 8 + seatValue;

    if (seatID > maxSeatID) {
        maxSeatID = seatID;
    }
}

console.log("Result is ", maxSeatID);

// TODO
// More with regex, figure out how to do some if else with regex (so that you do certain work if it matches certain patterns)