const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day2.txt", 'utf-8');
let lines = inputString.split('\n');

// for example /([^x]*)x([^x]*)x([^x]*)/;
let regexp = /([^-]*)-([^-]*) (.): (.*)/;

for (var i = 0; i < lines.length; i++) {
    // console.log(i, lines[i]);
    let regexResult = lines[i].match(regexp);
    // console.log(regexResult);

    let lowBound = regexResult[1];
    let highBound = regexResult[2];

    let charToFind = regexResult[3];

    let password = regexResult[4];

    let charCount = 0;

    for (var j = 0; j < password.length; j++) {
        if (password.charAt(j) == charToFind) charCount++;
    }

    if (charCount >= lowBound && charCount <= highBound) result++;
}

console.log("Result is ", result);

// TODO
// More with regex, figure out how to do some if else with regex (so that you do certain work if it matches certain patterns)