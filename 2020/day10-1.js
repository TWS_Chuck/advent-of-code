const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day10.txt", 'utf-8');
let lines = inputString.split('\n');

let jolts = [];
for (let i = 0; i < lines.length; i++) {
    let joltValue = parseInt(lines[i]);
    jolts.push(joltValue);
}
jolts.push(0);

jolts.sort((a, b) => {
    return a - b;
});

console.log(jolts[jolts.length - 1]);

let oneDiffs = 0;
let threeDiffs = 0;

for (let i = 1; i < jolts.length; i++) {
    if (jolts[i] - jolts[i - 1] === 1) oneDiffs++;
    if (jolts[i] - jolts[i - 1] === 3) threeDiffs++;
}
threeDiffs++; // hack

console.log("Result is ", oneDiffs * threeDiffs);