const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day14.txt", 'utf-8');
let lines = inputString.split('\n');

let maskRegexp = /mask = (.*)/;
let memRegexp = /mem\[(\d+)\] = (\d+)/;

let currentMaskString = "";
let memory = {};
for (var i = 0; i < lines.length; i++) {
    // console.log(i, lines[i]);
    let maskRegexResult = lines[i].match(maskRegexp);
    let memRegexResult = lines[i].match(memRegexp);

    if (maskRegexResult !== null) {
        // mask
        currentMaskString = maskRegexResult[1];
    } else if (memRegexResult !== null) {
        // mem
        let address = parseInt(memRegexResult[1]);
        let value = memRegexResult[2];
        let binaryValue = dec2bin(parseInt(value));

        memory[address] = applyMask(currentMaskString, binaryValue);
    }
}

for (let memoryAddress in memory) {
    console.log(memoryAddress, memory[memoryAddress]);
    memory[memoryAddress] = parseInt(memory[memoryAddress], 2);
    result += memory[memoryAddress];
}

console.log("Result is ", result);

function dec2bin(dec) {
    return (dec >>> 0).toString(2);
}

function applyMask(mask, value) {
    let newValue = "";

    for (let i = 0; i < 36; i++) {
        let thisMaskBit = mask.charAt(mask.length - 1 - i);
        let thisValueBit = value.charAt(value.length - 1 - i);

        if (thisValueBit === "") {
            thisValueBit = "0";
        }

        let newBit = thisValueBit;
        if (thisMaskBit === "1") {
            newBit = "1";
        } else if (thisMaskBit === "0") {
            newBit = "0";
        }

        newValue = newBit + newValue;
    }

    return newValue;
}