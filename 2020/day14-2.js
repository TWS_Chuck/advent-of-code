const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day14.txt", 'utf-8');
let lines = inputString.split('\n');

let maskRegexp = /mask = (.*)/;
let memRegexp = /mem\[(\d+)\] = (\d+)/;

let currentMaskString = "";
let memory = {};
for (var i = 0; i < lines.length; i++) {
    // console.log(i, lines[i]);
    let maskRegexResult = lines[i].match(maskRegexp);
    let memRegexResult = lines[i].match(memRegexp);

    if (maskRegexResult !== null) {
        // mask
        currentMaskString = maskRegexResult[1];
    } else if (memRegexResult !== null) {
        // mem
        let addressValue = parseInt(memRegexResult[1]);
        let address = dec2bin(addressValue);
        let value = parseInt(memRegexResult[2]);

        let addressesToModify = applyMaskToAddress(currentMaskString, address);
        for (address in addressesToModify) {
            memory[addressesToModify[address]] = value;
        }
    }
}

for (let memoryAddress in memory) {
    result += memory[memoryAddress];
}

console.log("Result is ", result);

function dec2bin(dec) {
    return (dec >>> 0).toString(2);
}

function applyMaskToAddress(mask, value) {
    let newAddress = "";

    for (let i = 0; i < 36; i++) {
        let thisMaskBit = mask.charAt(mask.length - 1 - i);
        let thisValueBit = value.charAt(value.length - 1 - i);

        if (thisValueBit === "") {
            thisValueBit = "0";
        }

        let newBit = thisValueBit;
        if (thisMaskBit === "1") {
            newBit = "1";
        } else if (thisMaskBit === "X") {
            newBit = "X";
        }

        newAddress = newBit + newAddress;
    }

    // Now get every combo of 1s and 0s in the address floating bits
    let addresses = [newAddress];
    let didNoWork = false;
    while (!didNoWork) {
        didNoWork = true;

        let nextAddresses = [];

        for (let addressIndex in addresses) {
            let address = addresses[addressIndex];
            bitToWorkOn = -1;
            for (let i = 0; i < address.length; i++) {
                if (address.charAt(i) === "X" && bitToWorkOn === -1) {
                    bitToWorkOn = i;
                }
            }

            if (bitToWorkOn !== -1) {
                // do work
                didNoWork = false;

                let firstNewAddress = address.substring(0, bitToWorkOn) + "0" + address.substring(bitToWorkOn + 1, address.length);
                let secondNewAddress = address.substring(0, bitToWorkOn) + "1" + address.substring(bitToWorkOn + 1, address.length);

                nextAddresses.push(firstNewAddress, secondNewAddress);
            }
        }

        if (!didNoWork) {
            addresses = nextAddresses;
        }
    }

    return addresses;
}