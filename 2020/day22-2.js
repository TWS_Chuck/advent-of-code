const fs = require('fs');

let result = 0;

let firstPlayer = [21, 50, 9, 45, 16, 47, 27, 38, 29, 48, 10, 42, 32, 31, 41, 11, 8, 33, 25, 30, 12, 40, 7, 23, 46];
let secondPlayer = [22, 20, 44, 2, 26, 17, 34, 37, 43, 5, 15, 18, 36, 19, 24, 35, 3, 13, 14, 1, 6, 39, 49, 4, 28];

// let firstPlayer = [9, 2, 6, 3, 1];
// let secondPlayer = [5, 8, 4, 7, 10];

let playerResult = recursiveCombat(firstPlayer, secondPlayer);
if (playerResult[0] !== undefined) {
    winningPlayer = playerResult[0];
} else {
    winningPlayer = playerResult[1];
}

for (let i = 0; i < winningPlayer.length; i++) {
    console.log(i + 1, winningPlayer[winningPlayer.length - 1 - i]);
    result += (i + 1) * winningPlayer[winningPlayer.length - 1 - i];
}

console.log("Result is ", result);

function generateCardKey(playerCards) {
    let key = "";
    for (let i = 0; i < playerCards.length; i++) {
        key += playerCards[i] + ",";
    }
    return key;
}

function recursiveCombat(player1, player2) {
    let previousStates = {};

    // console.log("Just got into a game with");
    // console.log("player1", player1);
    // console.log("player2", player2);

    // let round = 1;

    while (true) {
        // check if we've seen this state before
        // console.log("round", round);
        let player1Key = generateCardKey(player1);
        let player2Key = generateCardKey(player2);

        if (previousStates[player1Key] !== undefined) {
            if (previousStates[player1Key][player2Key] === undefined) {
                previousStates[player1Key][player2Key] = true;
            } else {
                // console.log("We have been here before, player 1 wins");
                return [
                    player1,
                    undefined,
                ];
            }
        } else {
            previousStates[player1Key] = {};
            previousStates[player1Key][player2Key] = true;
        }

        // nope, so draw cards
        let player1Card = player1[0];
        let player2Card = player2[0];

        // console.log("round", round, player1Card, player2Card);
        // console.log("current cards", player1, player2);

        if (player1Card === undefined) {
            // player 2 won
            // console.log("player 1 has no cards left, player 2 wins");
            return [
                undefined,
                player2
            ];
        } else if (player2Card === undefined) {
            // player 1 won
            // console.log("player 2 has no cards left, player 1 wins");
            return [
                player1,
                undefined
            ];
        }

        // check if each player has more cards than the value of current card
        let player1Won = false;
        if (player1Card <= player1.length - 1 && player2Card <= player2.length - 1) {
            // we have enough cards for recursive combat
            let response = recursiveCombat(player1.slice(1, 1 + player1Card), player2.slice(1, 1 + player2Card));
            if (response[0] !== undefined) {
                player1Won = true;
            }
        } else {
            // regular combat, b/c we cannot recurse
            if (player1Card > player2Card) {
                player1Won = true;
            }
        }

        if (player1Won) {
            player1 = [...player1.slice(1), player1Card, player2Card];
            player2 = [...player2.slice(1)];
        } else {
            player1 = [...player1.slice(1)];
            player2 = [...player2.slice(1), player2Card, player1Card];
        }

        // round++;
    }
}