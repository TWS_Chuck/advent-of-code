const { time } = require('console');
const fs = require('fs');

let result = 0;

let earliestTimestamp = 1000655;

let inputString = fs.readFileSync("day13.txt", 'utf-8');
let ids = inputString.split(',');

// for example /([^x]*)x([^x]*)x([^x]*)/;
let regexp = /./;

let busIDs = {};

for (var i = 0; i < ids.length; i++) {
    if (ids[i] !== "x") {
        let busIDValue = parseInt(ids[i]);
        busIDs[busIDValue] = true;
    }
}

let smallestLargerTimestamp = 100000000000000000;
let smallestBusID = -1;
for (var busID in busIDs) {
    let thing = Math.ceil(earliestTimestamp / busID);
    let timestamp = thing * busID;
    if (timestamp < smallestLargerTimestamp) {
        smallestLargerTimestamp = timestamp;
        smallestBusID = busID;
    }
}

result = (smallestLargerTimestamp - earliestTimestamp) * smallestBusID;

console.log("Result is ", result);
