const fs = require('fs');

let result = 0;

let cups = "476138259".split('');

for (let i = 0; i < 100; i++) {
    // console.log("\n\n");
    let currentCupLabel = parseInt(cups[0]);

    // first crabby move
    let currentCups = [...cups.slice(1, 4)];
    cups = [cups[0], ...cups.slice(4)];
    // console.log(currentCups, cups);

    // chose destination cup
    let destinationLabel = currentCupLabel - 1;
    while (!cups.includes("" + destinationLabel)) {
        destinationLabel--;
        if (destinationLabel <= 0) destinationLabel = 9;
        // console.log(destinationLabel);
    }

    let indexOfDestinationCup = cups.indexOf("" + destinationLabel);
    // console.log(destinationLabel, indexOfDestinationCup);

    let newCups = [...cups.slice(0, indexOfDestinationCup + 1), ...currentCups, ...cups.slice(indexOfDestinationCup + 1)];
    cups = [...newCups.slice(1), newCups[0]];
    // console.log(i, cups);
}

console.log("Result is ", cups.join(''));
