const fs = require('fs');

let result = 0;

let cross = 131;

let inputString = fs.readFileSync("day19.txt", 'utf-8');
let lines = inputString.split('\n');

// for example /([^x]*)x([^x]*)x([^x]*)/;
let regexp = /(\d+): (.*)/;

let rules = new Array(cross - 1);

for (let i = 0; i < cross; i++) {
    let regexResult = lines[i].match(regexp);

    let ruleNumber = regexResult[1];
    let ruleStuff = regexResult[2].split(' ');

    rules[ruleNumber] = {};

    if (ruleStuff.includes('|')) {
        let thing = [];
        for (let j = 0; j < ruleStuff.length; j++) {
            if (ruleStuff[j] !== '|') {
                thing.push(ruleStuff[j]);
            } else {
                rules[ruleNumber]["rule"] = thing;
                thing = [];
            }
            rules[ruleNumber]["orRule"] = thing;
        }
    } else {
        rules[ruleNumber]["rule"] = ruleStuff;
    }
}

for (let i = cross + 1; i < lines.length; i++) {
    // determine, for each row, if rule 0 is met
    let message = lines[i];
    let response = calaculateRegex(message, rules[0], 0);
    response = "^" + response + "$";
    console.log(response);

    if (message.match(response)) result++;
}

console.log("Result is ", result);

function calaculateRegex(message, rule) {
    // check what the rule is doing

    // specific value rule case
    let regexSoFar = "(";
    let potentialValue = rule["rule"][0];
    if (isNaN(potentialValue)) {
        // we are just looking for a character;
        return potentialValue;
    }

    if (rule["rule"] !== undefined) {
        let ruleToSeeIfWeFollow = rule["rule"];

        for (let i = 0; i < ruleToSeeIfWeFollow.length; i++) {
            let ruleToApply = parseInt(ruleToSeeIfWeFollow[i]);
            regexSoFar += calaculateRegex(message, rules[ruleToApply]);
        }
    }

    if (rule["orRule"] !== undefined) {
        regexSoFar += "|";

        let ruleToSeeIfWeFollow = rule["orRule"];

        for (let i = 0; i < ruleToSeeIfWeFollow.length; i++) {
            let ruleToApply = parseInt(ruleToSeeIfWeFollow[i]);
            regexSoFar += calaculateRegex(message, rules[ruleToApply]);
        }
    }

    return regexSoFar;
}