const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day2.txt", 'utf-8');
let lines = inputString.split('\n');

// for example /([^x]*)x([^x]*)x([^x]*)/;
let regexp = /([^-]*)-([^-]*) (.): (.*)/;

for (var i = 0; i < lines.length; i++) {
    let regexResult = lines[i].match(regexp);

    let firstAddress = parseInt(regexResult[1]);
    let secondAddress = parseInt(regexResult[2]);

    let charToFind = regexResult[3];

    let password = regexResult[4];

    let firstCharIsThere = (password.charAt(firstAddress - 1) == charToFind);
    let secondCharIsThere = (password.charAt(secondAddress - 1) == charToFind);

    if ((firstCharIsThere ^ secondCharIsThere)) result++;
}

console.log("Result is ", result);

// TODO
// More with regex, figure out how to do some if else with regex (so that you do certain work if it matches certain patterns)