const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day24.txt", 'utf-8');
let lines = inputString.split('\n');

let instructions = [];
for (let i = 0; i < lines.length; i++) {
    instructions.push(lines[i].split(','));
}

// white side initially, white true, black false
let grid = {};

for (let i = 0; i < instructions.length; i++) {
    let currentInstructions = instructions[i];

    let x = 0;
    let y = 0;

    // move
    for (let j = 0; j < currentInstructions.length; j++) {
        switch (currentInstructions[j]) {
            case 'nw':
                x--; y++;
                break;
            case 'ne':
                y++;
                break;
            case 'sw':
                y--;
                break;
            case 'se':
                x++; y--;
                break;
            case 'w':
                x--;
                break;
            case 'e':
                x++;
                break;
            default:
                console.log("OH FUCK", currentInstructions[j]);
                break;
        }
    }

    // flip
    let key = gridKey(x, y);
    if (grid[key] === undefined) grid[key] = true;
    grid[key] = !grid[key];
}

for (let gridPosition in grid) {
    if (!grid[gridPosition]) result++;
}

console.log("Result is ", result);

function gridKey(x, y) {
    return "" + x + "," + y;
}