const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day17.txt", 'utf-8');
let lines = inputString.split('\n');

let gridLimit = 3;

let grid = {};
for (let j = 0; j < lines.length; j++) {
    let line = lines[j];
    for (let i = 0; i < line.length; i++) {
        grid[gridKey(i, j, 0, 0)] = line.charAt(i);
    }
}

let cycles = 1;
let activeCount = 0;
while (cycles <= 6) {
    let newGrid = {};
    console.log("on cycle", cycles);

    activeCount = 0;
    for (let l = -6; l <= 6; l++) {
        for (let k = -6; k <= 6; k++) {
            for (let j = -6; j < 15; j++) {
                for (let i = -6; i < 15; i++) {
                    let newKey = gridKey(i, j, k, l);
                    let currentState = grid[newKey];
                    let count = countNeighboursThatAreCubes(i, j, k, l, grid);

                    if (currentState === '#') {
                        if (count === 2 || count === 3) {
                            activeCount++;
                            newGrid[newKey] = '#';
                        } else {
                            newGrid[newKey] = '.';
                        }
                    } else if (currentState === '.') {
                        if (count === 3) {
                            activeCount++;
                            newGrid[newKey] = '#';
                        } else {
                            newGrid[newKey] = '.';
                        }
                    }
                }
            }
        }
    }

    grid = newGrid;
    cycles++;
    console.log("After this cycle", cycles, activeCount);
}

console.log("Result is ", activeCount);

function countNeighboursThatAreCubes(x, y, z, t, grid) {
    let count = 0;

    for (let l = -1; l <= 1; l++) {
        for (let k = -1; k <= 1; k++) {
            for (let j = -1; j <= 1; j++) {
                for (let i = -1; i <= 1; i++) {

                    if (i === 0 && j === 0 && k === 0 && l === 0) continue;
                    let key = gridKey(x + i, y + j, z + k, t + l);

                    if (grid[key] === undefined) grid[key] = '.';

                    if (grid[key] === '#') {
                        count++
                    }
                }
            }
        }
    }

    return count;
}

function gridKey(x, y, z, t) {
    return "" + x + "," + y + "," + z + "," + t;
}