const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day12.txt", 'utf-8');
let lines = inputString.split('\n');

// for example /([^x]*)x([^x]*)x([^x]*)/;
let regexp = /([A-Z])(\d+)/;

let currentDir = "E";
let currX = 0;
let currY = 0;

for (var i = 0; i < lines.length; i++) {
    // console.log(i, lines[i]);
    let regexResult = lines[i].match(regexp);

    let direction = regexResult[1];
    let mag = parseInt(regexResult[2]);

    switch (direction) {
        case "N":
            currY += mag;
            break;
        case "S":
            currY -= mag;
            break;
        case "E":
            currX += mag;
            break;
        case "W":
            currX -= mag;
            break;
        case "L":
            while (mag > 0) {
                switch (currentDir) {
                    case "N":
                        currentDir = "W";
                        break;
                    case "S":
                        currentDir = "E";
                        break;
                    case "E":
                        currentDir = "N";
                        break;
                    case "W":
                        currentDir = "S";
                        break;
                }
                mag -= 90;
            }
            break;
        case "R":
            while (mag > 0) {
                switch (currentDir) {
                    case "N":
                        currentDir = "E";
                        break;
                    case "S":
                        currentDir = "W";
                        break;
                    case "E":
                        currentDir = "S";
                        break;
                    case "W":
                        currentDir = "N";
                        break;
                }
                mag -= 90;
            }
            break;
        case "F":
            switch (currentDir) {
                case "N":
                    currY += mag;
                    break;
                case "S":
                    currY -= mag;
                    break;
                case "E":
                    currX += mag;
                    break;
                case "W":
                    currX -= mag;
                    break;
                default:
                    console.log("HOW DID THIS HAPPEN");
            }
            break;
        default:
            console.log("OH FUCK!");
    }
}

console.log("Result is ", currX, currY);
