const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day12.txt", 'utf-8');
let lines = inputString.split('\n');

// for example /([^x]*)x([^x]*)x([^x]*)/;
let regexp = /([A-Z])(\d+)/;

let currX = 0;
let currY = 0;

let currentDir = "E";
let waypointX = 10;
let waypointY = 1;

for (var i = 0; i < lines.length; i++) {
    // console.log(i, lines[i]);
    let regexResult = lines[i].match(regexp);

    let direction = regexResult[1];
    let mag = parseInt(regexResult[2]);

    switch (direction) {
        case "N":
            waypointY += mag;
            break;
        case "S":
            waypointY -= mag;
            break;
        case "E":
            waypointX += mag;
            break;
        case "W":
            waypointX -= mag;
            break;
        case "L":
            let newX = waypointX;
            let newY = waypointY;
            while (mag > 0) {
                newX = -waypointY;
                newY = waypointX;
                mag -= 90;
                waypointX = newX;
                waypointY = newY;
            }
            break;
        case "R":
            let newXR = waypointX;
            let newYR = waypointY;
            while (mag > 0) {
                newXR = waypointY;
                newYR = -waypointX;
                mag -= 90;
                waypointX = newXR;
                waypointY = newYR;
            }
            break;
        case "F":
            currX += mag * (waypointX);
            currY += mag * (waypointY);
            break;
        default:
            console.log("OH FUCK!");
    }
}

console.log("Result is ", currX, currY);
