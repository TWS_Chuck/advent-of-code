const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day6.txt", 'utf-8');
let lines = inputString.split('\n');

for (var i = 0; i < lines.length; i++) {

    let responses = {};
    let sizeOfGroup = 0;

    while (lines[i] != "") {
        for (var j = 0; j < lines[i].length; j++) {
            if (responses[lines[i].charAt(j)] == undefined) {
                responses[lines[i].charAt(j)] = 0;
            }
            responses[lines[i].charAt(j)]++;
        }
        i++;
        sizeOfGroup++;
    }

    for (var key in responses) {
        if (responses[key] == sizeOfGroup) result++;
    }
}

console.log("Result is ", result);
