const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day18.txt", 'utf-8');
let lines = inputString.split('\n');

for (let i = 0; i < lines.length; i++) {
    result += evaluateExpressionBadly(lines[i], 0)["value"];
}

console.log("Result is ", result);

function evaluateExpressionBadly(expression, start) {
    let parts = expression.split('');

    let finalResult = 0;
    let currentExpression = '';
    for (let i = start; i < parts.length; i++) {
        let thing = parts[i];
        if (thing === ' ') continue;
        if (thing === '(') {
            response = evaluateExpressionBadly(expression, i + 1);
            thing = response["value"];
            i = response["index"];
        }
        if (thing === ')') {
            return {
                "value": finalResult,
                "index": i,
            };
        }

        if (isNaN(thing)) {
            // string
            currentExpression = thing;
        } else {
            let value = parseInt(thing);
            switch (currentExpression) {
                case '+':
                    finalResult += value;
                    break;
                case '*':
                    finalResult *= value;
                    break;
                case '-':
                    finalResult -= value;
                    break;
                case '/':
                    finalResult /= value;
                    break;
                case '':
                    finalResult = value;
                    break;
                default:
                    console.log("OH FUCK", currentExpression);
                    break;
            }
        }
    }
    return {
        "value": finalResult,
        "index": parts.length,
    };
}