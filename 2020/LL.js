ListNode = class {
    constructor(data) {
        this.data = data
        this.next = null
    }

    insert(nodes) {
        let currentNode = this;
        let lastNode = this.next;

        currentNode.next = nodes[0];
        nodes[nodes.length - 1].next = lastNode;
    }
}

LinkedList = class {
    constructor(head = null) {
        this.head = head;
        this.tail = head;
        this.lookup = {};

        if (head !== null) {
            lookup[head.data] = head;
        }
    }

    appendNode(node) {
        if (this.head === null) {
            this.head = node;
            this.tail = node;
        } else {
            this.tail.next = node;
            this.tail = node;
        }

        this.lookup[node.data] = node;
    }

    find(data) {
        return this.lookup[data];
    }
}

PopSequenceAfterNode = function (node, n) {
    let currentNode = node;

    let arrayOfNodes = [];

    currentNode = currentNode.next;

    for (let i = 0; i < n; i++) {
        arrayOfNodes.push(currentNode);
        currentNode = currentNode.next;
    }

    node.next = currentNode;

    return arrayOfNodes;
}