const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day16.txt", 'utf-8');
let lines = inputString.split('\n');

// for example /([^x]*)x([^x]*)x([^x]*)/;
let regexp = /(.*): (\d+)-(\d+) or (\d+)-(\d+)/;

let ranges = {};

for (var i = 0; i < 20; i++) {
    let regexResult = lines[i].match(regexp);
    let key = regexResult[1];
    let firstRangeLow = parseInt(regexResult[2]);
    let firstRangeHigh = parseInt(regexResult[3]);
    let secondRangeLow = parseInt(regexResult[4]);
    let secondRangeHigh = parseInt(regexResult[5]);

    ranges[key] = {
        "aLow": firstRangeLow,
        "aHigh": firstRangeHigh,
        "bLow": secondRangeLow,
        "bHigh": secondRangeHigh,
    }
}

let ruleBreakers = new Array(261);
for (let i = 25; i < lines.length; i++) {
    let ticketValues = lines[i].split(',');

    for (let j in ticketValues) {
        let followsRule = false;
        let currentValue = parseInt(ticketValues[j]);

        for (let rule in ranges) {
            let ruleALow = ranges[rule]["aLow"];
            let ruleAHigh = ranges[rule]["aHigh"];
            let ruleBLow = ranges[rule]["bLow"];
            let ruleBHigh = ranges[rule]["bHigh"];

            if (currentValue >= ruleALow && currentValue <= ruleAHigh) followsRule = true;
            if (currentValue >= ruleBLow && currentValue <= ruleBHigh) followsRule = true;
        }

        if (!followsRule) {
            lines[i] = "";
            ruleBreakers[i] = true;
            break;
        } else {
            ruleBreakers[i] = false;
        }
    }
}

let fields = [
    "departure location",
    "departure station",
    "departure platform",
    "departure track",
    "departure date",
    "departure time",
    "arrival location",
    "arrival station",
    "arrival platform",
    "arrival track",
    "class",
    "duration",
    "price",
    "route",
    "row",
    "seat",
    "train",
    "type",
    "wagon",
    "zone"
];

let fieldPlacements = {};

for (let f in fields) {
    let currentField = fields[f];
    console.log("Current field", currentField);

    for (let p = 0; p < 20; p++) {
        let fieldCanBeForP = true;

        for (let i = 25; i < lines.length; i++) {
            if (ruleBreakers[i]) continue;

            let ticketValues = lines[i].split(',');
            let currentValue = parseInt(ticketValues[p]);

            let followsRule = false;

            let ruleALow = ranges[currentField]["aLow"];
            let ruleAHigh = ranges[currentField]["aHigh"];
            let ruleBLow = ranges[currentField]["bLow"];
            let ruleBHigh = ranges[currentField]["bHigh"];

            if (currentValue >= ruleALow && currentValue <= ruleAHigh) followsRule = true;
            if (currentValue >= ruleBLow && currentValue <= ruleBHigh) followsRule = true;

            if (!followsRule) {
                fieldCanBeForP = false;
                break;
            };
        }

        if (fieldCanBeForP) {
            if (fieldPlacements[currentField] === undefined) {
                fieldPlacements[currentField] = [];
            }

            fieldPlacements[currentField].push(p);
        }
    }
}

let finalPlacements = {};
let alreadyPlaced = [];
for (let i = 1; i <= 20; i++) {
    for (f in fieldPlacements) {
        let currentField = fieldPlacements[f];
        if (currentField.length === i) {
            console.log(f, currentField);
        }
    }
}
