const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day3.txt", 'utf-8');
let lines = inputString.split('\n');

let map = {};

let lineLength = lines[0].length;

let currentX = 1;

for (var i = 2; i < lines.length; i += 2) {
    if (lines[i].charAt(currentX) == '#') result++;

    currentX = (currentX + 1) % lineLength;
}

console.log("Result is ", result);