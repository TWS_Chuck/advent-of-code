const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day8.txt", 'utf-8');
let lines = inputString.split('\n');

// for example /([^x]*)x([^x]*)x([^x]*)/;
let nopRegex = /nop (.*)/;
let accRegex = /acc ([+-])(.*)/;
let jmpRegex = /jmp ([+-])(.*)/;

let i = 0;
let acc = 0;
let instructionsExecuted = {};
while (i < lines.length) {
    if (instructionsExecuted[i] != undefined) {
        console.log("Accumulator is ", acc);
        break;
    } else if (instructionsExecuted[i] == undefined) {
        instructionsExecuted[i] = true;
    }

    let currentInstruction = lines[acc];

    let nopResult = lines[i].match(nopRegex);
    if (nopResult !== null) {
        i++;
        continue;
    }

    let accResult = lines[i].match(accRegex);
    if (accResult !== null) {
        if (accResult[1] == "+") {
            acc += parseInt(accResult[2]);
        } else if (accResult[1] == "-") {
            acc -= parseInt(accResult[2]);
        }
        i++;
        continue;
    }

    let jmpResult = lines[i].match(jmpRegex);
    if (jmpResult !== null) {
        if (jmpResult[1] == "+") {
            i += parseInt(jmpResult[2]);
        } else if (jmpResult[1] == "-") {
            i -= parseInt(jmpResult[2]);
        }
        continue;
    }
}
console.log("Result is ", result);

// TODO
// More with regex, figure out how to do some if else with regex (so that you do certain work if it matches certain patterns)