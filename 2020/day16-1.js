const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day16.txt", 'utf-8');
let lines = inputString.split('\n');

// for example /([^x]*)x([^x]*)x([^x]*)/;
let regexp = /(.*): (\d+)-(\d+) or (\d+)-(\d+)/;

let ranges = {};

for (var i = 0; i < 20; i++) {
    let regexResult = lines[i].match(regexp);
    let key = regexResult[1];
    let firstRangeLow = parseInt(regexResult[2]);
    let firstRangeHigh = parseInt(regexResult[3]);
    let secondRangeLow = parseInt(regexResult[4]);
    let secondRangeHigh = parseInt(regexResult[5]);

    ranges[key] = {
        "aLow": firstRangeLow,
        "aHigh": firstRangeHigh,
        "bLow": secondRangeLow,
        "bHigh": secondRangeHigh,
    }
}

for (var i = 25; i < lines.length; i++) {
    let ticketValues = lines[i].split(',');

    for (let j in ticketValues) {
        let doesntFollowRule = true;
        let currentValue = parseInt(ticketValues[j]);

        for (let rule in ranges) {
            let ruleALow = ranges[rule]["aLow"];
            let ruleAHigh = ranges[rule]["aHigh"];
            let ruleBLow = ranges[rule]["bLow"];
            let ruleBHigh = ranges[rule]["bHigh"];

            if (currentValue >= ruleALow && currentValue <= ruleAHigh) doesntFollowRule = false;
            if (currentValue >= ruleBLow && currentValue <= ruleBHigh) doesntFollowRule = false;
        }

        if (doesntFollowRule) result += currentValue;
    }
}

console.log("Result is ", result);
