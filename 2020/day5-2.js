const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day5.txt", 'utf-8');
let lines = inputString.split('\n');

let seats = {};

for (var i = 0; i < lines.length; i++) {
    let rowValue = 0;

    for (var j = 0; j < 7; j++) {
        let powerOfTwo = 2 ** (6 - j);

        if (lines[i].charAt(j) === "B") {
            rowValue += powerOfTwo;
        }
    }

    let seatValue = 0;

    for (var j = 7; j < 10; j++) {
        let powerOfTwo = 2 ** (9 - j);

        if (lines[i].charAt(j) === "R") {
            seatValue += powerOfTwo;
        }
    }

    let seatID = rowValue * 8 + seatValue;

    seats[seatID] = true;
}

for (var x = 919; x > 0; x--) {
    if (seats[x] == null) {
        console.log("Found a gap at ", x);
        break;
    }
}

console.log("Result is ", result);

// TODO
// More with regex, figure out how to do some if else with regex (so that you do certain work if it matches certain patterns)