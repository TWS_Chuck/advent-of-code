const fs = require('fs');
const ll = require('./LL.js');

let result = 0;

let fakeCups = [4, 7, 6, 1, 3, 8, 2, 5, 9];
// let fakeCups = [3, 8, 9, 1, 2, 5, 4, 6, 7];

let cupsList = new LinkedList();

for (let i = 1; i <= 1_000_000; i++) {
    let newNode;
    if (i <= fakeCups.length) {
        newNode = new ListNode(fakeCups[i - 1]);
    } else {
        if (i === 9) continue;
        newNode = new ListNode(i);
    }

    cupsList.appendNode(newNode);
}

cupsList.tail.next = cupsList.head;

console.log("Done creating cups");

let currentCup = cupsList.head;

for (let i = 0; i < 10_000_000; i++) {
    // first crabby move
    let currentCups = PopSequenceAfterNode(currentCup, 3, cupsList);

    // chose destination cup
    let destinationLabel = currentCup.data - 1;
    if (destinationLabel === 0) destinationLabel = 1_000_000;
    while (currentCups[0].data === destinationLabel || currentCups[1].data === destinationLabel || currentCups[2].data === destinationLabel) {
        destinationLabel--;
        if (destinationLabel === 0) destinationLabel = 1_000_000;
    }

    let destinationCup = cupsList.find(destinationLabel);
    destinationCup.insert(currentCups);

    currentCup = currentCup.next;
}

let indexOf1 = cupsList.find(1);
console.log(indexOf1, indexOf1.next, indexOf1.next.next);
result = indexOf1.next.data * indexOf1.next.next.data;

console.log("Result is ", result);
