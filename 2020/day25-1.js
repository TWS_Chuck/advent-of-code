const fs = require('fs');

let result = 0;

let cardPublic = 12090988;
let doorPublic = 240583;

let value = 1;
let subject = 7;

let secretCardLoopSize = 0;
let secretDoorLoopSize = 0;

// determine loop sizes for each
let loopSize = 0;
while (secretCardLoopSize === 0 || secretDoorLoopSize === 0) {
    value = (subject * value) % 20201227;
    loopSize++;
    // console.log(loopSize, value)
    if (value === cardPublic) {
        secretCardLoopSize = loopSize;
    }
    if (value === doorPublic) {
        secretDoorLoopSize = loopSize;
    }
}
console.log("secretCardLoopSize", secretCardLoopSize);
console.log("secretDoorLoopSize", secretDoorLoopSize);

value = 1;
for (let i = 0; i < secretCardLoopSize; i++) {
    value = (value * doorPublic) % 20201227;
}

console.log("Result is ", value);
