const fs = require('fs');

let result = 0;

let cross = 131;

let inputString = fs.readFileSync("day19.txt", 'utf-8');
let lines = inputString.split('\n');

// for example /([^x]*)x([^x]*)x([^x]*)/;
let regexp = /(\d+): (.*)/;

let rules = new Array(cross - 1);

for (let i = 0; i < cross; i++) {
    let regexResult = lines[i].match(regexp);

    let ruleNumber = regexResult[1];
    let ruleStuff = regexResult[2].split(' ');

    rules[ruleNumber] = [];

    if (ruleStuff.includes('|')) {
        let thing = [];
        for (let j = 0; j < ruleStuff.length; j++) {
            if (ruleStuff[j] !== '|') {
                thing.push(ruleStuff[j]);
            } else {
                rules[ruleNumber].push(thing);
                thing = [];
            }
        }
        rules[ruleNumber].push(thing);
    } else {
        rules[ruleNumber].push(ruleStuff);
    }
}

for (let i = cross + 1; i < lines.length; i++) {
    // determine, for each row, if rule 0 is met
    let message = lines[i];
    let response = determineIfRuleAppliesToMessage(message, rules[0], 0, true);
    console.log(message, response, message.length);
    if (response !== -1) {
        console.log("success\n");
        result++;
    } else {
        console.log("failure\n");
    }
}

console.log("Result is ", result);

function determineIfRuleAppliesToMessage(message, rule, start, yeet) {
    // check what the rule is doing
    // console.log("On point", start, "we have", rule);

    // specific value rule case
    if (isNaN(rule[0][0])) {
        // we are just looking for a character;
        let charToFind = rule[0][0];
        if (message.charAt(start) === charToFind) {
            return start + 1;
        } else {
            return -1;
        }
    }

    for (let r = 0; r < rule.length; r++) {
        let currentIndex = start;
        let ruleToSeeIfWeFollow = rule[r];
        let rulesCouldBeFollowed = true;
        for (let i = 0; i < ruleToSeeIfWeFollow.length; i++) {
            let ruleToApply = parseInt(ruleToSeeIfWeFollow[i]);
            let needsToTerminate = (i === ruleToSeeIfWeFollow.length - 1) && yeet;
            let newIndex = determineIfRuleAppliesToMessage(message, rules[ruleToApply], currentIndex, needsToTerminate);
            if (newIndex === -1) {
                rulesCouldBeFollowed = false;
                break;
            }
            currentIndex = newIndex;
        }

        if (rulesCouldBeFollowed) {
            if (yeet) {
                if (currentIndex === message.length) return currentIndex;
            } else {
                return currentIndex;
            }
        }
    }

    // console.log("FUCK IT", rule, start);
    return -1;
}