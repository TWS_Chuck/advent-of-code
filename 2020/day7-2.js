const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day7.txt", 'utf-8');
let lines = inputString.split('\n');

// for example /([^x]*)x([^x]*)x([^x]*)/;
let regexp = /(.*) bags contain (.*)./;
let detailregexp = /(\d+) (.*) bag/;

let bagSchema = {};

for (var i = 0; i < lines.length; i++) {
    let regexResult = lines[i].match(regexp);
    let bagUnderQuestion = regexResult[1];
    let details = regexResult[2].split(',');

    if (bagSchema[bagUnderQuestion] == undefined) bagSchema[bagUnderQuestion] = {};

    details.forEach(detail => {
        let detailRegex = detail.match(detailregexp);
        if (detailRegex != null) {
            bagSchema[bagUnderQuestion][detailRegex[2]] = detailRegex[1];
        }
    })
}

let memo = {};

console.log("Result is ", howManyBagsInsideThisBag(bagSchema, "shiny gold"));

function howManyBagsInsideThisBag(bagSchema, bag) {
    if (memo[bag] != undefined) {
        return memo[bag];
    }

    let totalBags = 1;

    for (var subbag in bagSchema[bag]) {
        let factor = bagSchema[bag][subbag];
        totalBags += factor * howManyBagsInsideThisBag(bagSchema, subbag);
    }

    memo[bag] = totalBags;
    return totalBags;
}