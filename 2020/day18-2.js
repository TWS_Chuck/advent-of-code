const fs = require('fs');

let sum = 0;

let inputString = fs.readFileSync("day18.txt", 'utf-8');
let lines = inputString.split('\n');

for (let i = 0; i < lines.length; i++) {
    let parts = lines[i].split('');
    sum += evaluateExpressionBadly(parts);
}

console.log("Result is ", sum);

function evaluateExpressionBadly(expression) {
    let parts = buildParts(expression);
    // console.log("Got into this, with", parts);

    // add
    let finalResult = 0;
    let currentExpression = '';
    let partialExpression = [];
    for (let i = 0; i < parts.length; i++) {
        let thing = parts[i];

        if (isNaN(thing)) {
            // string
            currentExpression = thing;
        } else {
            let value = parseInt(thing);
            switch (currentExpression) {
                case '+':
                    finalResult += value;
                    break;
                case '*':
                    partialExpression.push(finalResult + "*");
                    finalResult = value;
                    currentExpression = '';
                    break;
                case '':
                    finalResult = value;
                    break;
                default:
                    console.log("OH FUCK", currentExpression);
                    break;
            }
        }
    }
    partialExpression.push(finalResult);
    let total = eval(partialExpression.join(''));

    // console.log("About to return this value", total);
    return total;
}

function buildParts(expression) {
    let parts = [];

    for (let i = 0; i < expression.length; i++) {
        let currentThing = expression[i];
        if (!isNaN(currentThing)) {
            parts.push(parseInt(currentThing));
            continue;
        }

        if (currentThing === '(') {
            let endIndex = findMatchingParen(expression, i);
            let result = evaluateExpressionBadly(expression.slice(i + 1, endIndex));
            parts.push(result);
            i = endIndex;
            continue;
        }

        parts.push(currentThing);
    }

    return parts;
}

function findMatchingParen(expression, start) {
    let currentDepth = 1;
    for (let i = start + 1; i < expression.length; i++) {
        let current = expression[i];
        if (current === '(') currentDepth++;
        if (current === ')') {
            currentDepth--;
            if (currentDepth === 0) {
                return i;
            }
        }
    }
    return -1;
}