CGL = class {
    constructor(data) {
        this.map = data["map"];
        this.neighbourOffsets = data["neighbourOffsets"];
        this.doesNeighbourMeetCondition = data["doesNeighbourMeetCondition"];
        this.determineNextValue = data["determineNextValue"];
        this.defaultMapValue = data["defaultMapValue"];
    }

    simulate(cycles) {
        for (let i = 0; i < cycles; i++) {
            let newMap = {};

            for (let position of Object.keys(this.map)) {
                let neighbourConditionCount = this.countNeighboursThatMeetCondition(position);
                let nextValue = this.determineNextValue(
                    this.getValueAtPositionInMap(this.map, position),
                    neighbourConditionCount
                );
                this.setValueAtPositionInMap(newMap, nextValue);
                this.ensureNeighboursExistInNewMap(newMap, position);
            }

            this.map = newMap;
        }
    }

    ensureNeighboursExistInNewMap(newMap, position) {
        for (const offset of this.neighbourOffsets) {
            let neighbourPosition = this.calculatePositionFromPositionAndOffset(position, offset);
            if (newMap[neighbourPosition] === undefined) newMap[neighbourPosition] = this.defaultMapValue;
        }
    }

    countNeighboursThatMeetCondition(position) {
        let count = 0;

        for (const offset of this.neighbourOffsets) {
            let neighbourPosition = this.calculatePositionFromPositionAndOffset(position, offset);
            let neighbourValue = this.getValueAtPositionInMap(neighbourPosition);

            if (this.doesNeighbourMeetCondition(neighbourValue)) count++;
        }

        return count;
    }

    calculatePositionFromPositionAndOffset(position, offset) {
        let point = this.convertPositionToPoint(position);
        for (let i = 0; i < point.length; i++) {
            point[i] += offset[i];
        }
        return this.convertPointToPosition(point);
    }

    getValueAtPositionInMap(map, position) {
        return map[position];
    }

    setValueAtPositionInMap(map, position, value) {
        map[position] = value;
    }

    convertPositionToPoint(position) {
        return position.split(',');
    }

    convertPointToPosition(point) {
        return point.join('');
    }

    countPositionsOnMapThatMeetCondition(condition = this.doesNeighbourMeetCondition) {
        let count = 0;

        for (let position of Object.keys(this.map)) {
            let value = this.getValueAtPositionInMap(this.map, position);
            if (condition(value)) count++;
        }

        return count;
    }
}