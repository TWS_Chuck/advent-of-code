const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day24.txt", 'utf-8');
let lines = inputString.split('\n');

let instructions = [];
for (let i = 0; i < lines.length; i++) {
    instructions.push(lines[i].split(','));
}

// white side initially, white true, black false
let grid = {};
// flip some tiles

let lowX = 0;
let lowY = 0;
let highX = 0;
let highY = 0;

for (let i = 0; i < instructions.length; i++) {
    let currentInstructions = instructions[i];

    let x = 0;
    let y = 0;

    // move
    for (let j = 0; j < currentInstructions.length; j++) {
        switch (currentInstructions[j]) {
            case 'nw':
                x--; y++;
                break;
            case 'ne':
                y++;
                break;
            case 'sw':
                y--;
                break;
            case 'se':
                x++; y--;
                break;
            case 'w':
                x--;
                break;
            case 'e':
                x++;
                break;
            default:
                console.log("OH FUCK", currentInstructions[j]);
                break;
        }
    }

    if (x < lowX) lowX = x;
    if (y < lowY) lowY = y;
    if (x > highX) highX = x;
    if (y > highY) highY = y;

    // flip
    let key = gridKey(x, y);
    if (grid[key] === undefined) grid[key] = true;
    grid[key] = !grid[key];
}

let count = 0;
for (let gridPosition in grid) {
    if (!grid[gridPosition]) count++;
}
console.log("Count after flipping tiles", count);

lowX--;
lowY--;
highX++;
highY++;

for (let cycle = 0; cycle < 100; cycle++) {
    // apply conway garbage
    let newGrid = {};
    for (let i = lowX; i <= highX; i++) {
        for (let j = lowY; j <= highY; j++) {
            let gridPosition = gridKey(i, j);

            if (grid[gridPosition] === undefined) grid[gridPosition] = true;
            newGrid[gridPosition] = grid[gridPosition];

            let isCurrentTileWhite = grid[gridPosition];

            let count = countHexNeighboursThatAreBlack(grid, gridPosition);
            // console.log(gridPosition, isCurrentTileWhite, count);

            if (isCurrentTileWhite && count === 2) newGrid[gridPosition] = false;
            if (!isCurrentTileWhite && (count === 0 || count > 2)) newGrid[gridPosition] = true;
        }
    }

    lowX--;
    lowY--;
    highX++;
    highY++;

    grid = newGrid;
}

for (let gridPosition in grid) {
    if (!grid[gridPosition]) result++;
}

console.log("Result is ", result);

function gridKey(x, y) {
    return "" + x + "," + y;
}

function countHexNeighboursThatAreBlack(map, position) {
    let stuff = position.split(',');
    let x = parseInt(stuff[0]);
    let y = parseInt(stuff[1]);

    let blackCount = 0;

    let offsets = [[1, 0], [-1, 0], [-1, 1], [0, 1], [0, -1], [1, -1]];
    for (let offset in offsets) {
        let nX = x + offsets[offset][0];
        let nY = y + offsets[offset][1];

        let nKey = gridKey(nX, nY);
        // console.log(position, offsets[offset], nKey);

        if (map[nKey] !== undefined && !map[nKey]) blackCount++;
    }

    return blackCount;
}