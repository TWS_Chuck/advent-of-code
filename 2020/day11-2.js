const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day11.txt", 'utf-8');
let lines = inputString.split('\n');

let grid = {};

for (var j = 0; j < lines.length; j++) {
    for (var i = 0; i < lines[j].length; i++) {
        grid[gridKey(i, j)] = lines[j].charAt(i);
    }
}

let seatsChanged = 0;
do {
    seatsChanged = 0;

    let newGrid = {};
    for (var j = 0; j < lines.length; j++) {
        for (var i = 0; i < lines[j].length; i++) {
            let key = gridKey(i, j);
            if (grid[key] == ".") {
                newGrid[key] = "."
                continue;
            }

            let occupiedNeighbours = getDataOnNeighbours(grid, i, j);
            if (grid[key] == "L" && occupiedNeighbours == 0) {
                seatsChanged++;
                newGrid[key] = "#";
            } else if (grid[key] == "#" && occupiedNeighbours >= 5) {
                seatsChanged++;
                newGrid[key] = "L";
            } else {
                newGrid[key] = grid[key];
            }
        }
    }

    grid = newGrid;

    console.log("Now there are this many seat changes:", seatsChanged);
} while (seatsChanged != 0);

for (var j = 0; j < lines.length; j++) {
    for (var i = 0; i < lines[j].length; i++) {
        if (grid[gridKey(i, j)] == "#") result++;
    }
}

// console.log(grid["1,6"]);

console.log("Result is ", result);

function gridKey(x, y) {
    return "" + x + "," + y;
}

function getDataOnNeighbours(map, x, y) {
    let occupiedSeatNeighbours = 0;

    let logging = false;
    // if (x == 0 && y == 0) logging = true;

    if (logging) console.log("This seat is", map[gridKey(x, y)]);

    for (let j = -1; j <= 1; j++) {
        for (let i = -1; i <= 1; i++) {
            if (i == 0 && j == 0) {
                continue;
            }

            let seatType = getFirstSeatInDirection(map, x, y, i, j);

            if (seatType === undefined) {
                continue;
            }

            if (seatType == "#") {
                if (logging) console.log("We get in here though, right?");
                occupiedSeatNeighbours++;
            }
        }
    }

    if (logging) console.log("This many neighbours", occupiedSeatNeighbours);
    return occupiedSeatNeighbours;
}

function getFirstSeatInDirection(map, x, y, dirX, dirY) {
    let logging = false;
    // if (x == 0 && y == 0) logging = true;

    if (logging) console.log("Check each direction");

    let currX = x + dirX;
    let currY = y + dirY;

    if (logging) console.log(x, y, dirX, dirY, currX, currY);
    while (map[gridKey(currX, currY)] !== undefined) {
        if (logging) console.log("Now we are at:", currX, currY);
        if (logging) console.log(map[gridKey(currX, currY)]);

        if (map[gridKey(currX, currY)] !== ".") return map[gridKey(currX, currY)];

        currX += dirX;
        currY += dirY;
    }

    return undefined;
}