// 0 silent
// 1 important
// 2 informational 
const LOG_LEVEL = 2;

const ALLOWED_EYE_COLOURS = {
    "amb": true,
    "blu": true,
    "brn": true,
    "gry": true,
    "grn": true,
    "hzl": true,
    "oth": true,
};

const MIN_BIRTH_YEAR = 1920;
const MAX_BIRTH_YEAR = 2002;
const MIN_ISSUE_YEAR = 2010;
const MAX_ISSUE_YEAR = 2020;
const MIN_EXPIRATION_YEAR = 2020;
const MAX_EXPIRATION_YEAR = 2030;

const MIN_HEIGHT_CM = 150;
const MAX_HEIGHT_CM = 193;
const MIN_HEIGHT_IN = 59;
const MAX_HEIGHT_IN = 76;

const heightRegexPattern = /(\d+)([a-z]*)/;
const hairColourRegexPattern = /^#[a-f0-9]{6}$/;
const passportIdRegexPattern = /^\d{9}$/;

class Passport {
    constructor(passportParameters) {
        let fieldsValid = validatePassportParameters(passportParameters);

        if (!fieldsValid) {
            throw new Error('Fields are not valid');
        }

        this.assignParameters(passportParameters);
    }

    assignParameters(passportParameters) {
        this.byr = parseInt(passportParameters["byr"]);
        this.iyr = parseInt(passportParameters["iyr"]);
        this.eyr = parseInt(passportParameters["eyr"]);

        this.hgt = getHeightDetails(passportParameters["hgt"]);

        this.hcl = passportParameters["hcl"];
        this.ecl = passportParameters["ecl"];

        this.pid = parseInt(passportParameters["pid"]);
        this.cid = parseInt(passportParameters["cid"]);
    }
}

function validatePassportParameters(passportParameters) {
    let validators = {
        "byr": validateBirthYear,
        "iyr": validateIssueYear,
        "eyr": validateExpirationYear,
        "hgt": validateHeight,
        "hcl": validateHairColour,
        "ecl": validateEyeColour,
        "pid": validatePassportId,
        "cid": validateCountryId,
    };

    for (passportKey in validators) {
        if (!validators[passportKey](passportParameters[passportKey])) {
            return false;
        }
    }

    return true;
}

function validateBirthYear(birthYear) {
    if (birthYear == null) {
        passportLog(2, "birth year is null");
        return false;
    }

    let birthYearValue = parseInt(birthYear);

    if (birthYearValue < MIN_BIRTH_YEAR || birthYearValue > MAX_BIRTH_YEAR) {
        passportLog(2, "birth year not in range");
        return false;
    }

    return true;
}

function validateIssueYear(issueYear) {
    if (issueYear == null) {
        passportLog(2, "issue year is null");
        return false;
    }

    let issueYearValue = parseInt(issueYear);

    if (issueYearValue < MIN_ISSUE_YEAR || issueYearValue > MAX_ISSUE_YEAR) {
        passportLog(2, "issue year is not in range");
        return false;
    }

    return true;
}

function validateExpirationYear(expirationYear) {
    if (expirationYear == null) {
        passportLog(2, "expiration year is null");
        return false;
    }

    let expirationYearValue = parseInt(expirationYear);

    if (expirationYearValue < MIN_EXPIRATION_YEAR || expirationYearValue > MAX_EXPIRATION_YEAR) {
        passportLog(2, "expiration year is not in range");
        return false;
    }

    return true;
}

function getHeightDetails(height) {
    let heightDetails = {};

    let heightRegex = height.match(heightRegexPattern);

    heightDetails.value = heightRegex[1];
    heightDetails.unit = heightRegex[2];

    return heightDetails;
}

function validateHeight(height) {
    if (height == null) {
        passportLog(2, "height year is null");
        return false;
    }

    let heightDetails = getHeightDetails(height);

    switch (heightDetails.unit) {
        case "cm":
            return validateCmHeight(heightDetails.value);
        case "in":
            return validateInHeight(heightDetails.value);
        default:
            passportLog(2, "Unexpected height unit");
            return false;
    }
}

// TODO These two functions can probably be combined, and confs could be used to handle limits
function validateCmHeight(cmHeight) {
    if (cmHeight < MIN_HEIGHT_CM || cmHeight > MAX_HEIGHT_CM) {
        passportLog(2, "height cm not in range");
        return false;
    }

    return true;
}

function validateInHeight(inHeight) {
    if (inHeight < MIN_HEIGHT_IN || inHeight > MAX_HEIGHT_IN) {
        passportLog(2, "height in not in range");
        return false;
    }

    return true;
}

function validateHairColour(hairColour) {
    if (hairColour == null) {
        passportLog(2, "hair colour is null");
        return false;
    }

    if (!hairColour.match(hairColourRegexPattern)) {
        passportLog(2, "hair colour is not valid");
        return false;
    }

    return true;
}

function validateEyeColour(eyeColour) {
    if (eyeColour == null) {
        passportLog(2, "eye colour is null");
        return false;
    }

    if (!ALLOWED_EYE_COLOURS[eyeColour]) {
        passportLog(2, "eye colour not allowed");
        return false;
    }

    return true;
}

function validatePassportId(passportId) {
    if (passportId == null) {
        passportLog(2, "passport id is null");
        return false;
    }

    if (!passportId.match(passportIdRegexPattern)) {
        passportLog(2, "passport id is not valid");
        return false;
    }

    return true;
}

function validateCountryId(countryId) {
    passportLog(1, "Country id is " + countryId);

    return true;
}

function passportLog(logLevel, message) {
    if (logLevel <= LOG_LEVEL) {
        console.log(message);
    }
}

module.exports = Passport;
