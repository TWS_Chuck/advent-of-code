const fs = require('fs');

let result = 0;

let inputString = "0,14,1,3,7,9";
let lines = inputString.split(',');

let memory = new Array(30000000);
let lastNumberInserted = "";

for (let i = 0; i < lines.length; i++) {
    let value = parseInt(lines[i]);
    memory[value] = [i, -1];
    lastNumberInserted = value;
}

for (let i = lines.length; i < 30000000; i++) {
    let nextNumber = -1;

    if (memory[lastNumberInserted][1] === -1) {
        nextNumber = 0;
    } else {
        nextNumber = memory[lastNumberInserted][0] - memory[lastNumberInserted][1];
    }

    if (memory[nextNumber] === undefined) {
        memory[nextNumber] = [i, -1];
    } else {
        memory[nextNumber] = [i, memory[nextNumber][0]];
    }

    lastNumberInserted = nextNumber;
    if (i % 10000 === 0) console.log(i);
}

console.log("Result is ", lastNumberInserted);
