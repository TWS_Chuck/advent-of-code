const { Console } = require('console');
const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day1.txt", 'utf-8');
let lines = inputString.split('\n');

for (var i = 0; i < lines.length; i++) {
    for (var j = i + 1; j < lines.length; j++) {
        for (var k = j + 1; k < lines.length; k++) {
            let sum = parseInt(lines[i]) + parseInt(lines[j]) + parseInt(lines[k]);

            if (sum == 2020) {
                console.log(i, lines[i]);
                console.log(j, lines[j]);
                console.log(k, lines[k]);
                console.log(parseInt(lines[i]) * parseInt(lines[j]) * parseInt(lines[k]));
                break;
            }
        }
    }
}

console.log("Result is ", result);