const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day9.txt", 'utf-8');
let lines = inputString.split('\n');

let preamble = 25;

// for example /([^x]*)x([^x]*)x([^x]*)/;
let regexp = /./;

let previousValueIndex = preamble - 1;
let previousValues = {};

for (var i = 0; i < preamble; i++) {
    previousValues[i] = parseInt(lines[i]);
}

for (var i = preamble; i < lines.length; i++) {
    let value = parseInt(lines[i]);

    if (!doPreviousValuesAddUpToValue(value)) {
        console.log("Found it, first value that breaks is", value);
        return;
    } else {
        previousValues[i] = value;
        previousValueIndex++;
    }
}

function doPreviousValuesAddUpToValue(value) {
    for (var i = previousValueIndex - preamble; i <= previousValueIndex; i++) {
        for (var j = previousValueIndex - preamble; j <= previousValueIndex; j++) {
            let first = previousValues[i];
            let second = previousValues[j];
            if (first + second == value) {
                return true;
            }
        }
    }

    return false;
}