const { time } = require('console');
const fs = require('fs');

let result = 0;

let inputString = fs.readFileSync("day13.txt", 'utf-8');
let ids = inputString.split(',');

// for example /([^x]*)x([^x]*)x([^x]*)/;
let regexp = /./;

let busIDs = {};

for (var i = 0; i < ids.length; i++) {
    if (ids[i] !== "x") {
        let busIDValue = parseInt(ids[i]);
        busIDs[i] = busIDValue;
    }
}

let currentFrequency = 0;
let currentOffset = 0;
for (let tDeltaString in busIDs) {
    let tDelta = parseInt(tDeltaString);

    if (currentFrequency === 0) {
        currentFrequency = busIDs[tDelta];
        currentOffset = parseInt(tDelta);
        continue;
    }

    let n = 0;
    let done = false;
    while (!done) {
        n++;

        let currentValue = currentFrequency * n + currentOffset;

        // console.log("Trying this", n, currentValue, "and current", currentFrequency, currentOffset, "and bus", tDelta, busIDs[tDelta]);
        if ((currentValue + tDelta) % busIDs[tDelta] === 0) {
            currentFrequency = currentFrequency * busIDs[tDelta];
            currentOffset = currentValue;

            console.log("Now we have", currentFrequency, currentOffset);

            done = true;
        }
    }
}

console.log("Result is ", result);