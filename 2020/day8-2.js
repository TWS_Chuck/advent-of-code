const fs = require('fs');
const { exit } = require('process');

let result = 0;

let inputString = fs.readFileSync("day8.txt", 'utf-8');
let lines = inputString.split('\n');

// for example /([^x]*)x([^x]*)x([^x]*)/;
let nopRegex = /nop ([+-]\d+)/;
let accRegex = /acc ([+-]\d+)/;
let jmpRegex = /jmp ([+-]\d+)/;

let exitNotFound = true;
let overrideOpIndex = 0;
while (exitNotFound) {
    if (lines[overrideOpIndex].match(accRegex) != undefined) {
        overrideOpIndex++;
        continue;
    }

    let i = 0;
    let acc = 0;
    let instructionsExecuted = {};

    exitNotFound = false;

    while (i < lines.length) {
        if (instructionsExecuted[i] != undefined) {
            exitNotFound = true;
            break;
        } else if (instructionsExecuted[i] == undefined) {
            instructionsExecuted[i] = true;
        }

        let currentInstruction = lines[acc];

        let nopResult = lines[i].match(nopRegex);
        if (nopResult !== null) {
            if (i == overrideOpIndex) {
                i += parseInt(nopResult[1]);
            } else {
                i++;
            }
            continue;
        }

        let accResult = lines[i].match(accRegex);
        if (accResult !== null) {
            acc += parseInt(accResult[1]);
            i++;
            continue;
        }

        let jmpResult = lines[i].match(jmpRegex);
        if (jmpResult !== null) {
            if (i == overrideOpIndex) {
                i++;
            } else {
                i += parseInt(jmpResult[1]);
            }
            continue;
        }
    }
    if (!exitNotFound) {
        console.log("The acc is", acc, i, overrideOpIndex);
    }

    overrideOpIndex++;
}

console.log("Result is ", result);
