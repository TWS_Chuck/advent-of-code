const fs = require('fs');

let result = 1;

let inputString = fs.readFileSync("day20.txt", 'utf-8');
let lines = inputString.split('\n');

// for example /([^x]*)x([^x]*)x([^x]*)/;
let regexp = /Tile (\d+):/;

let tiles = {};
for (let i = 0; i < lines.length; i += 12) {
    let newGrid = {};
    let regexResult = lines[i].match(regexp);
    let tileID = regexResult[1];

    for (let y = i + 1; y <= i + 10; y++) {
        for (let x = 0; x < 10; x++) {
            newGrid[gridKey(x, y - i - 1)] = lines[y].charAt(x);
        }
    }

    tiles[tileID] = newGrid;
}

let ids = Object.keys(tiles).sort((a, b) => {
    return a - b;
});

let neighbourCount = {};
let neighbours = {};

for (let i = 0; i < ids.length; i++) {
    for (let j = i + 1; j < ids.length; j++) {
        if (areGridsNeighbours(tiles[ids[i]], tiles[ids[j]])) {
            if (neighbourCount[ids[i]] === undefined) neighbourCount[ids[i]] = 0;
            if (neighbourCount[ids[j]] === undefined) neighbourCount[ids[j]] = 0;
            neighbourCount[ids[i]]++;
            neighbourCount[ids[j]]++;

            if (neighbours[ids[i]] === undefined) neighbours[ids[i]] = [];
            if (neighbours[ids[j]] === undefined) neighbours[ids[j]] = [];
            neighbours[ids[i]].push(ids[j]);
            neighbours[ids[j]].push(ids[i]);
        }
    }
    if (neighbourCount[ids[i]] === 2) {
        console.log(ids[i]);
        result *= parseInt(ids[i])
    }
}

console.log("Result is ", result);

function gridKey(x, y) {
    return "" + x + "," + y;
}

function areGridsNeighbours(first, second) {
    let secondEdges = getEdgeStrings(second);

    // check arrays as they are
    let firstEdges = getEdgeStrings(first);
    if (compareEdges(firstEdges, secondEdges)) return true;

    return false;
}

function compareEdges(firstEdges, secondEdges) {
    for (let i = 0; i < firstEdges.length; i++) {
        let index = secondEdges.indexOf(firstEdges[i]);
        if (index !== -1) return true;

        let reversedString = firstEdges[i].split("").reverse().join("");
        index = secondEdges.indexOf(reversedString);
        if (index !== -1) return true;
    }
    return false;
}

function getEdgeStrings(grid) {
    let edgeStrings = [];

    let left = "";
    let right = "";
    let top = "";
    let bottom = "";
    for (let x = 0; x < 10; x++) {
        top += grid[gridKey(x, 0)];
        bottom += grid[gridKey(x, 9)];
    }
    for (let y = 0; y < 10; y++) {
        left += grid[gridKey(0, y)];
        right += grid[gridKey(9, y)];
    }

    edgeStrings.push(left, right, top, bottom);

    return edgeStrings;
}
