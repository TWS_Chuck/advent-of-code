const fs = require('fs');

let result = 0;

let cups = [4, 7, 6, 1, 3, 8, 2, 5, 9];

for (let i = 10; i <= 1_000_000; i++) {
    cups.push(i);
}
console.log("Done creating cups");

let previousStates = {};
for (let i = 0; i < 10; i++) {
    console.log("\n\n", i);
    let key = cups.join(',');
    if (previousStates[key] !== undefined) {
        console.log("Been here before")
    } else {
        previousStates[key] = true;
    }

    let currentCupLabel = parseInt(cups[0]);

    // first crabby move
    let currentCups = [...cups.slice(1, 4)];
    cups = [cups[0], ...cups.slice(4)];
    console.log(currentCupLabel, currentCups);

    // chose destination cup
    let destinationLabel = currentCupLabel - 1;
    while (!cups.includes(destinationLabel)) {
        destinationLabel--;
        if (destinationLabel <= 0) destinationLabel = 1000000;
    }

    let indexOfDestinationCup = cups.indexOf(destinationLabel);
    console.log(destinationLabel, indexOfDestinationCup);

    let newCups = [...cups.slice(0, indexOfDestinationCup + 1), ...currentCups, ...cups.slice(indexOfDestinationCup + 1)];
    cups = [...newCups.slice(1), newCups[0]];
}

let indexOf1 = cups.indexOf(1);
console.log(indexOf1, cups[indexOf1 + 1], cups[indexOf1 + 2])
result = cups[indexOf1 + 1] * cups[indexOf1 + 2];

console.log("Result is ", result);
