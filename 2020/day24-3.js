const fs = require('fs');
const cgl = require('./CGL.js');

let result = 0;

let inputString = fs.readFileSync("day24.txt", 'utf-8');
let lines = inputString.split('\n');

let instructions = [];
for (let i = 0; i < lines.length; i++) {
    instructions.push(lines[i].split(','));
}

// white side initially, white true, black false
let grid = {};
// flip some tiles

for (let i = 0; i < instructions.length; i++) {
    let currentInstructions = instructions[i];

    let x = 0;
    let y = 0;

    // move
    for (let j = 0; j < currentInstructions.length; j++) {
        switch (currentInstructions[j]) {
            case 'nw':
                x--; y++;
                break;
            case 'ne':
                y++;
                break;
            case 'sw':
                y--;
                break;
            case 'se':
                x++; y--;
                break;
            case 'w':
                x--;
                break;
            case 'e':
                x++;
                break;
            default:
                console.log("OH FUCK", currentInstructions[j]);
                break;
        }
    }

    // flip
    let key = gridKey(x, y);
    if (grid[key] === undefined) grid[key] = true;
    grid[key] = !grid[key];
}

let cGrid = new CGL({
    "map": grid,
    "neighbourOffsets": [[1, 0], [-1, 0], [-1, 1], [0, 1], [0, -1], [1, -1]],
    "doesNeighbourMeetCondition": function (value) {
        if (value === undefined) return false;
        return !value;
    },
    "determineNextValue": function (value, count) {
        if (value && count === 2) return false;
        if (!value && (count === 0 || count > 2)) return true;
        return value;
    },
    "defaultMapValue": true,
});

cGrid.simulate(100);

result = cGrid.countPositionsOnMapThatMeetCondition();

console.log("Result is ", result);

function gridKey(x, y) {
    return "" + x + "," + y;
}