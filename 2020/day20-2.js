const fs = require('fs');

let result = 1;

let inputString = fs.readFileSync("day20.txt", 'utf-8');
let lines = inputString.split('\n');

let monsterStr = fs.readFileSync("monster.txt", 'utf-8');
let monsterLines = monsterStr.split('\n');

let regexp = /Tile (\d+):/;

let tiles = {};
for (let i = 0; i < lines.length; i += 12) {
    let newGrid = {};
    let regexResult = lines[i].match(regexp);
    let tileID = regexResult[1];

    for (let y = i + 1; y <= i + 10; y++) {
        for (let x = 0; x < 10; x++) {
            newGrid[gridKey(x, y - i - 1)] = lines[y].charAt(x);
        }
    }

    tiles[tileID] = newGrid;
}

let monsterArray = new Array(monsterLines.length);
for (let j = 0; j < monsterLines.length; j++) {
    monsterArray[j] = new Array(monsterLines[j].length);
    for (let i = 0; i < monsterLines[j].length; i++) {
        monsterArray[j][i] = monsterLines[j].charAt(i);
    }
}

// flipArrayVertically(monsterArray);
flipArrayHorizontally(monsterArray);
// monsterArray = rotate2dArray(monsterArray);
// monsterArray = rotate2dArray(monsterArray);
// monsterArray = rotate2dArray(monsterArray);
print2dArray(monsterArray);

let ids = Object.keys(tiles).sort((a, b) => {
    return a - b;
});

let neighbourCount = {};
let neighbours = {};
let corners = [];
let edges = [];
let others = []

for (let i = 0; i < ids.length; i++) {
    for (let j = i + 1; j < ids.length; j++) {
        if (areGridsNeighbours(tiles[ids[i]], tiles[ids[j]])) {
            if (neighbourCount[ids[i]] === undefined) neighbourCount[ids[i]] = 0;
            if (neighbourCount[ids[j]] === undefined) neighbourCount[ids[j]] = 0;
            neighbourCount[ids[i]]++;
            neighbourCount[ids[j]]++;

            if (neighbours[ids[i]] === undefined) neighbours[ids[i]] = [];
            if (neighbours[ids[j]] === undefined) neighbours[ids[j]] = [];
            neighbours[ids[i]].push(ids[j]);
            neighbours[ids[j]].push(ids[i]);
        }
    }
    if (neighbourCount[ids[i]] === 2) {
        corners.push(ids[i]);
    } else if (neighbourCount[ids[i]] === 3) {
        edges.push(ids[i]);
    } else if (neighbourCount[ids[i]] === 4) {
        others.push(ids[i]);
    } else {
        console.log("OH GOD DAMNIT");
    }
}

console.log(corners);
console.log(edges);

let tileMap = getOuterCrust();
fillInTileMap();
// console.log("Filled map", tileMap);
orientTileMap();

let megaGrid = createMegaGridAndDissolveBorders();

console.log("Initial, rough water count", countRoughWater());
clearMonstersOut();
console.log("Final, rough water count", countRoughWater());

function gridKey(x, y) {
    return "" + x + "," + y;
}

function areGridsNeighbours(first, second) {
    let secondEdges = getEdgeStrings(second);

    // check arrays as they are
    let firstEdges = getEdgeStrings(first);
    if (compareEdges(firstEdges, secondEdges)) return true;

    return false;
}

function compareEdges(firstEdges, secondEdges) {
    for (let i = 0; i < firstEdges.length; i++) {
        let index = secondEdges.indexOf(firstEdges[i]);
        if (index !== -1) return true;

        let reversedString = firstEdges[i].split("").reverse().join("");
        index = secondEdges.indexOf(reversedString);
        if (index !== -1) return true;
    }
    return false;
}

function compareEdgesExactly(firstEdges, secondEdges) {
    // order of edges is [left, right, top, bottom]
    // we are comparing 0 to 1 and 2 to 3
    // we return what edge it lines up with
    // 0 left matches
    // 1 right matches
    // 2 top matches
    // 3 bottom matches
    if (secondEdges.includes(firstEdges[0])) return 0;
    if (secondEdges.includes(firstEdges[1])) return 1;
    if (secondEdges.includes(firstEdges[2])) return 2;
    if (secondEdges.includes(firstEdges[3])) return 3;
    return -1;
}

function getEdgeStrings(grid) {
    let edgeStrings = [];

    let left = "";
    let right = "";
    let top = "";
    let bottom = "";
    for (let x = 0; x < 10; x++) {
        top += grid[gridKey(x, 0)];
        bottom += grid[gridKey(x, 9)];
    }
    for (let y = 0; y < 10; y++) {
        left += grid[gridKey(0, y)];
        right += grid[gridKey(9, y)];
    }

    edgeStrings.push(left, right, top, bottom);

    return edgeStrings;
}

function getOuterCrust() {
    let outside = [];

    // pick one corner to start
    let lastCorner = corners[0];

    outside.push(lastCorner);

    let returnedToFirstCorner = false;
    while (!returnedToFirstCorner) {
        // choose which direction from corner to move
        let lastNeighbour;
        for (let i = 0; i < 2; i++) {
            let potentialNeighbour = neighbours[lastCorner][i];
            if (outside.includes(potentialNeighbour)) continue;
            lastNeighbour = potentialNeighbour;
        }
        if (lastNeighbour === undefined) console.log("OH FUCK 1");
        outside.push(lastNeighbour)

        let foundNextCorner = false;

        while (!foundNextCorner) {
            let newNeighbours = neighbours[lastNeighbour];

            let nextNeighbour = -1;
            for (let i = 0; i < newNeighbours.length; i++) {
                let thisNeighbour = newNeighbours[i];
                if (outside.includes(thisNeighbour) && !corners.includes(thisNeighbour)) continue;
                if (others.indexOf(thisNeighbour) !== -1) continue;
                nextNeighbour = thisNeighbour;
            }
            if (nextNeighbour === -1) console.log("OH FUCK 2");

            outside.push(nextNeighbour);
            lastNeighbour = nextNeighbour;

            if (corners.indexOf(lastNeighbour) !== -1) foundNextCorner = true;
        }

        lastCorner = outside[outside.length - 1];

        if (lastCorner === outside[0]) returnedToFirstCorner = true;
    }

    let tilePlacement = {};
    let x = 0;
    let y = 0;
    let index = 0;

    tilePlacement[gridKey(x, y)] = outside[index];

    index++;
    x++;

    let cornerCount = 1;

    do {
        let nextTile = outside[index];
        tilePlacement[gridKey(x, y)] = nextTile;

        if (corners.includes(nextTile)) cornerCount++;

        switch (cornerCount) {
            case 1:
                x++;
                break;
            case 2:
                y++;
                break;
            case 3:
                x--;
                break;
            case 4:
                y--;
                break;
            case 5:
                // we are done
                break;
            default:
                console.log("OH FUCK 3");
                break;
        }

        index++;
    } while (outside[index - 1] !== outside[0]);

    return tilePlacement;
}

function fillInTileMap() {
    let insertedTiles = {};
    // TODO I know the tileMap is 11x11, maybe do this better another time
    for (let j = 1; j < 11; j++) {
        for (let i = 1; i < 11; i++) {
            let leftNeighbour = tileMap[gridKey(i - 1, j)];
            let topNeighbour = tileMap[gridKey(i, j - 1)];

            let tileToInsert;
            for (let tile in others) {
                if (insertedTiles[others[tile]] !== undefined) continue;

                let thisTilesNeighbours = neighbours[others[tile]];
                if (thisTilesNeighbours.includes(leftNeighbour) && thisTilesNeighbours.includes(topNeighbour)) {
                    tileToInsert = others[tile];
                    insertedTiles[others[tile]] = true;
                }
            }
            if (tileToInsert === undefined) console.log("OH FUCK 4");

            tileMap[gridKey(i, j)] = tileToInsert;
        }
    }
}

function orientTileMap() {
    orientTileUntilItMatchesSpecificTile(tileMap[gridKey(0, 0)], tiles[tileMap[gridKey(1, 0)]], 1);

    for (let j = 0; j <= 11; j++) {
        for (let i = 0; i <= 11; i++) {
            let currentTileID = tileMap[gridKey(i, j)];

            let leftNeighbour = tileMap[gridKey(i - 1, j)];
            let topNeighbour = tileMap[gridKey(i, j - 1)];

            if (leftNeighbour !== undefined) {
                orientTileUntilItMatchesSpecificTile(currentTileID, tiles[leftNeighbour], 0);
            } else if (topNeighbour !== undefined) {
                orientTileUntilItMatchesSpecificTile(currentTileID, tiles[topNeighbour], 2);
            }
        }
    }
}

// reminder: edge 0 left, 1 right, 2 top, 3 bottom
function orientTileUntilItMatchesSpecificTile(tileID, otherTile, edge) {
    let tile = tiles[tileID];

    let thing = 0;
    let done = false;
    while (!done) {
        for (let i = 0; i < 4; i++) {
            response = compareEdgesExactly(getEdgeStrings(tile), getEdgeStrings(otherTile));
            if (response === edge) return;

            tile = rotateGrid(tileID);
        }

        switch (thing) {
            case 0:
                flipGridHorizontally(tile);
                break;
            case 1:
                flipGridVertically(tile);
                break;
            case 2:
                flipGridHorizontally(tile);
                break;
            case 3:
                done = true;
                break;
        }
        thing++;
    }
}

function flipGridVertically(grid) {
    for (let y = 0; y < 5; y++) {
        for (let x = 0; x < 10; x++) {
            let temp = grid[gridKey(x, y)];
            grid[gridKey(x, y)] = grid[gridKey(x, 9 - y)];
            grid[gridKey(x, 9 - y)] = temp;
        }
    }
}

function flipGridHorizontally(grid) {
    for (let y = 0; y < 10; y++) {
        for (let x = 0; x < 5; x++) {
            let temp = grid[gridKey(x, y)];
            grid[gridKey(x, y)] = grid[gridKey(9 - x, y)];
            grid[gridKey(9 - x, y)] = temp;
        }
    }
}

function rotateGrid(tileID) {
    let grid = tiles[tileID];

    let newGrid = {};
    for (let y = 0; y < 10; y++) {
        for (let x = 0; x < 10; x++) {
            newGrid[gridKey(x, y)] = grid[gridKey(9 - y, x)];
        }
    }

    tiles[tileID] = newGrid;
    return newGrid;
}

function printGrid(tile) {
    let line = "";
    for (let y = 0; y < 10; y++) {
        for (let x = 0; x < 10; x++) {
            line += tile[gridKey(x, y)];
        }
        console.log(line);
        line = "";
    }
}

function createMegaGridAndDissolveBorders() {
    let megaGrid = {};
    for (let j = 0; j <= 11; j++) {
        for (let i = 0; i <= 11; i++) {
            // transcribe this tile into the megaGrid
            let thisTile = tiles[tileMap[gridKey(i, j)]];

            for (let y = 1; y <= 8; y++) {
                for (let x = 1; x <= 8; x++) {
                    let tileValue = thisTile[gridKey(x, y)];
                    megaGrid[gridKey(i * 8 + x - 1, j * 8 + y - 1)] = tileValue;
                }
            }
        }
    }
    return megaGrid;
}

function clearMonstersOut() {
    for (let j = 0; j <= 100; j++) {
        for (let i = 0; i <= 100; i++) {
            let response = canMonsterShapeFit(i, j);
            if (response) {
                console.log("FOUND A MONSTER", i, j);
                clearOutMonsterShape(i, j)
            }
        }
    }
}

function canMonsterShapeFit(i, j) {
    for (let y = 0; y < monsterArray.length; y++) {
        for (let x = 0; x < monsterArray[y].length; x++) {
            if (monsterArray[y][x] === '#') {
                if (megaGrid[gridKey(i + x, j + y)] === '.' || megaGrid[gridKey(i + x, j + y)] === undefined) return false;
            }
        }
    }
    return true;
}

function clearOutMonsterShape(i, j) {
    for (let y = 0; y < monsterArray.length; y++) {
        for (let x = 0; x < monsterArray[y].length; x++) {
            if (monsterArray[y][x] === '#') {
                megaGrid[gridKey(i + x, j + y)] = 'O';
            }
        }
    }
}

function countRoughWater() {
    let count = 0;
    for (let j = 0; j < 96; j++) {
        for (let i = 0; i < 96; i++) {
            if (megaGrid[gridKey(i, j)] === '#') count++;
        }
    }
    return count;
}

function print2dArray(array) {
    for (let j = 0; j < array.length; j++) {
        let line = "";
        for (let i = 0; i < array[j].length; i++) {
            line += array[j][i];
        }
        console.log(line);
        line = "";
    }
}

function rotate2dArray(array) {
    let height = array.length;
    let width = array[0].length;

    let newArray = new Array(width);
    for (let i = 0; i < width; i++) {
        newArray[i] = new Array(height);
        for (let j = 0; j < height; j++) {
            newArray[i][height - j - 1] = array[j][i];
        }
    }

    return newArray;
}

function flipArrayVertically(array) {
    let height = array.length;
    for (let y = 0; y < Math.floor(height / 2); y++) {
        for (let x = 0; x < array[0].length; x++) {
            let temp = array[y][x];
            array[y][x] = array[height - y - 1][x];
            array[height - y - 1][x] = temp;
        }
    }
}

function flipArrayHorizontally(array) {
    let width = array[0].length;
    for (let y = 0; y < array.length; y++) {
        for (let x = 0; x < Math.floor(array[0].length / 2); x++) {
            let temp = array[y][x];
            array[y][x] = array[y][width - x - 1];
            array[y][width - x - 1] = temp;
        }
    }
}

// console.log("just to make sure", countAllTileInteriors());
// function countAllTileInteriors() {
//     let count = 0;
//     for (let tile in tiles) {
//         let thisTile = tiles[tile];
//         for (let y = 1; y <= 8; y++) {
//             for (let x = 1; x <= 8; x++) {
//                 if (thisTile[gridKey(x, y)] === '#') count++;
//             }
//         }
//     }
//     return count;
// }

// printAllTiles();
// function printAllTiles() {
//     for (let tile in tileMap) {
//         console.log("This tile is", tile, tileMap[tile]);
//         printGrid(tiles[tileMap[tile]]);
//     }
// }

printMegaGrid();
function printMegaGrid() {
    for (let y = 0; y < 96; y++) {
        let line = "";
        for (let x = 0; x < 96; x++) {
            line += megaGrid[gridKey(x, y)];
        }
        console.log(line);
        line = "";
    }
}