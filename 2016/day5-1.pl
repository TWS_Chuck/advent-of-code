use lib '../perl-common';

use strict;
use warnings;

use CL;

use Data::Dumper;
use Digest::MD5;

my $input = "cxdnnyjw";

my $result = "";

my $value = 0;
my $count = 0;
for (my $i = 0; ; $i++) {
    my $md5 = Digest::MD5->new;
    $md5->add($input.$value);
    
    my $digest = $md5->hexdigest;
    # print("$digest\n");

    if ($digest =~ /^00000(.)/) {
        $result .= $1;
        $count++;
        last if $count == 8;
    }

    $value++;
}

print("Result is $result\n");
