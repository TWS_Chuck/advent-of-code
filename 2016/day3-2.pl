use lib '../perl-common';

use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day3.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

for (my $i = 0; $i < scalar @lines; $i += 3) {
	my $line1 = $lines[$i];
	my $line2 = $lines[$i + 1];
	my $line3 = $lines[$i + 2];

    $line1 =~ /(\d+)[ ]*(\d+)[ ]*(\d+)/;
    my ($s1a, $s1b, $s1c) = ($1, $2, $3);

    $line2 =~ /(\d+)[ ]*(\d+)[ ]*(\d+)/;
    my ($s2a, $s2b, $s2c) = ($1, $2, $3);

    $line3 =~ /(\d+)[ ]*(\d+)[ ]*(\d+)/;
    my ($s3a, $s3b, $s3c) = ($1, $2, $3);

    $result += condition($s1a, $s2a, $s3a);
    $result += condition($s1b, $s2b, $s3b);
    $result += condition($s1c, $s2c, $s3c);
}

print("Result is $result\n");

sub condition {
    my ($s1, $s2, $s3) = @_;

    return 1 if ($s1 + $s2 > $s3 && $s1 + $s3 > $s2 && $s3 + $s2 > $s1);
    return 0;
}