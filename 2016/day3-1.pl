use lib '../perl-common';

use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day3.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
    if ($line =~ /(\d+)[ ]*(\d+)[ ]*(\d+)/) {
        my ($s1, $s2, $s3) = ($1, $2, $3);
        $result++ if ($s1 + $s2 > $s3 && $s1 + $s3 > $s2 && $s3 + $s2 > $s1);
    }
}

print("Result is $result\n");
