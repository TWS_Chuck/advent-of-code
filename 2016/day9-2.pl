use lib '../perl-common';

use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day9.txt";

my @line = CL::importSingleLineFromFile('', $filename);

my $result = "";

my $decryptMode = 0;
for (my $i = 0; $i < scalar @line;) {
    if ($line[$i] eq '(') {
        my $decryptString;
        while ($line[$i] ne ')') {
            $decryptString .= $line[$i];
            $i++;
        }
        $i++;

        $decryptString =~ /(\d+)x(\d+)/;
        my ($charsToGrab, $repeat) = ($1, $2);

        my $stringToRepeat = "";
        for (1..$charsToGrab) {
            $stringToRepeat .= $line[$i];
            $i++;
        }

        $result .= $stringToRepeat x $repeat;
    } else {
        while ($line[$i] ne '(') {
            $result .= $line[$i];
            $i++;
        }
    }
}

print("Result is ".scalar (split //, $result)."\n");
