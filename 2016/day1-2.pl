use lib '../perl-common';

use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day1.txt";

my @line = CL::importSingleLineFromFile(', ', $filename);

my $result = 0;

my ($x, $y) = (0, 0);
my $currentHeading = 'n';

my $previousLocations = {};
for (my $i = 0; $i < scalar @line; $i++) {
    $line[$i] =~ /([LR])(\d+)/;
    my ($direction, $scalar) = ($1, $2);

    $currentHeading = getNewHeading($currentHeading, $direction);

    for (my $j = 0; $j < $scalar; $j++) {
           if ($currentHeading eq 'n') { $y++ }
        elsif ($currentHeading eq 'e') { $x++ }
        elsif ($currentHeading eq 'w') { $x-- }
        elsif ($currentHeading eq 's') { $y-- }

        if ($previousLocations->{"$x,$y"}) {
            $result = abs($x) + abs($y);
            print("Result is $result\n");
            exit;
        }

        $previousLocations->{"$x,$y"} = $i;
    }
}

$result = abs($x) + abs($y);

print("Result is $result\n");

sub getNewHeading {
    my ($currentHeading, $direction) = @_;

        if ($currentHeading eq 'n') {
            return 'e' if $direction eq 'R';
            return 'w' if $direction eq 'L';
        } elsif ($currentHeading eq 'e') {
            return 's' if $direction eq 'R';
            return 'n' if $direction eq 'L';
        } elsif ($currentHeading eq 'w') {
            return 'n' if $direction eq 'R';
            return 's' if $direction eq 'L';
        } elsif ($currentHeading eq 's') {
            return 'w' if $direction eq 'R';
            return 'e' if $direction eq 'L';
        }
}