use lib '../perl-common';

use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day7.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

for (my $i = 0; $i < scalar @lines; $i++) {
    my $isGood = 1;
    my $isNotBad = 0;

	my $line = $lines[$i];
    my $inHypernet = 0;
    my @lastThreeChars = ("", "", "");
    foreach my $char (split //, $line) {
        if ($char eq '[') {
            $inHypernet++;
            @lastThreeChars = ("", "", "");
        } elsif ($char eq ']') {
            $inHypernet--;
            @lastThreeChars = ("", "", "");
        }

        my $patternMatched = 0;
        $patternMatched = 1 if (($lastThreeChars[0] eq $char && $lastThreeChars[1] eq $lastThreeChars[2] && $lastThreeChars[0] ne $lastThreeChars[1]));
        $isGood = 0 if ($inHypernet && $patternMatched);
        $isNotBad = 1 if (!$inHypernet && $patternMatched);

        $lastThreeChars[0] = $lastThreeChars[1];
        $lastThreeChars[1] = $lastThreeChars[2];
        $lastThreeChars[2] = $char;
    }

    $result += $isGood * $isNotBad;
}

print("Result is $result\n");
