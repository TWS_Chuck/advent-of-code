use lib '../perl-common';

use strict;
use warnings;

use CL;

use Data::Dumper;
use Digest::MD5;

my $input = "cxdnnyjw";

my $result = ();

my $count;
for (my $i = 0; ; $i++) {
    my $md5 = Digest::MD5->new;
    $md5->add($input.$i);
    
    my $digest = $md5->hexdigest;

    if ($digest =~ /^00000([0-7])(.)/) {
        next if ($result->[$1]);

        $result->[$1] = $2;
        $count++;
        last if $count == 8;
    }
}

print("Result is ".Dumper(join('', @$result)));
