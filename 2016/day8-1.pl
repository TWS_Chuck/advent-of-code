use lib '../perl-common';

use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day8.txt";

my $result = 0;

my @lines = CL::importLinesFromFile($filename);

my $grid = {};
for my $i (0..49) {
    for my $j (0..5) {
        $grid->{$j}{$i} = '.';
    }
}

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];

    if ($line =~ /rect (\d+)x(\d+)/) {
        my ($width, $height) = ($1, $2);
        print("Setting rect $1 x $2\n");
        for my $i (0..$width-1) {
            for my $j (0..$height-1) {
                $grid->{$j}{$i} = '#';
            }
        }
    } elsif ($line =~ /rotate row y=(\d+) by (\d+)/) {
        my ($row, $shift) = ($1, $2);

        my $last = '.';
        for my $j (1..$shift) {
            for my $x (0..49) {
                my $next = $grid->{$row}{$x};
                $grid->{$row}{$x} = $last;
                $last = $next;
            }
            $grid->{$row}{0} = $last;
        }
    } elsif ($line =~ /rotate column x=(\d+) by (\d+)/) {
        my ($col, $shift) = ($1, $2);

        my $y = 0;
        my $last = '.';
        for my $j (1..$shift) {
            for my $y (0..5) {
                my $next = $grid->{$y}{$col};
                $grid->{$y}{$col} = $last;
                $last = $next;
            }
            $grid->{0}{$col} = $last;
        }
    }

    print("\n$i\n");
    printGrid();
}

foreach my $y (sort {$a <=> $b} keys %$grid) {
        foreach my $x (sort {$a <=> $b} keys %{$grid->{$y}}) {
            $result++ if $grid->{$y}{$x} eq '#';
        }
}

print("Result is $result\n");

sub printGrid {
    foreach my $y (sort {$a <=> $b} keys %$grid) {
        foreach my $x (sort {$a <=> $b} keys %{$grid->{$y}}) {
            print $grid->{$y}{$x};
        }
        print "\n";
    }
}