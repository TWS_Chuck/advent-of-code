use lib '../perl-common';

use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day4.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
    $line =~ /([^\d]*)(\d+)\[(.*)\]/;
    my ($string, $sectorID, $check) = ($1, $2, $3);
    my $rotate = $sectorID % 26;

    my $decrypted = "";
    foreach my $char (split //, $string) {
        my $ascii = ord($char) + $rotate;
        $ascii -= 26 if ($ascii > 122);
        $decrypted .= chr($ascii);
    }
    print ($sectorID, Dumper($decrypted));
}
