use lib '../perl-common';

use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day7.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

for (my $i = 0; $i < scalar @lines; $i++) {
    my $isGood = 0;

	my $line = $lines[$i];
    my $inHypernet = 0;
    my @lastTwoChars = ("", "");

    my $inHypernetPatterns = {};
    my $outHypernetPatterns = {};
    foreach my $char (split //, $line) {
        if ($char eq '[') {
            $inHypernet++;
            @lastTwoChars = ("", "");
        } elsif ($char eq ']') {
            $inHypernet--;
            @lastTwoChars = ("", "");
        }

        my $patternMatched = 0;
        $patternMatched = 1 if ($lastTwoChars[0] eq $char && $lastTwoChars[0] ne $lastTwoChars[1]);
        if ($patternMatched) {
            my $pattern = $lastTwoChars[0].$lastTwoChars[1].$lastTwoChars[0];
            my $reversePattern = $lastTwoChars[1].$lastTwoChars[0].$lastTwoChars[1];

            if ($inHypernet) {
                $isGood = 1 if $outHypernetPatterns->{$reversePattern};
                $inHypernetPatterns->{$pattern} = 1;
            } else {
                $isGood = 1 if $inHypernetPatterns->{$reversePattern};
                $outHypernetPatterns->{$pattern} = 1;
            }
        }

        $lastTwoChars[0] = $lastTwoChars[1];
        $lastTwoChars[1] = $char;
    }

    $result += $isGood;
}

print("Result is $result\n");
