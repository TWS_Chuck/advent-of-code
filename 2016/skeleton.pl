use lib '../perl-common';

use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "dayXX.txt";

my @lines = CL::importLinesFromFile($filename);

my @line = CL::importSingleLineFromFile(',', $filename);

my $grid = CL::import2DGridFromFile($filename);
my $width = scalar (keys %$grid);
my $height = scalar (keys %{$grid->{0}});

my $result = 0;

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
}

for (my $i = 0; $i < scalar @line; $i++) {
}

for (my $x = 0; $x < $width; $x++) {
	for (my $y = 0; $y < $height; $y++) {
		print("$grid->{$x}{$y}");
	}
	print "\n";
}

print("Result is $result\n");
