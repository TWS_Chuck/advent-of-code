use lib '../perl-common';

use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day2.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = "";

my $lookup = {
    "0,2" => "1",
    "-1,1" => "2",
    "0,1" => "3",
    "1,1" => "4",
    "-2,0" => "5",
    "-1,0" => "6",
    "0,0" => "7",
    "1,0" => "8",
    "2,0" => "9",
    "-1,-1" => "A",
    "0,-1" => "B",
    "1,-1" => "C",
    "0,-2" => "D",
};

my $x = 0;
my $y = 0;
for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
    my @directions = split //, $line;

    for (my $j = 0; $j < scalar @directions; $j++) {
        my $currentDirection = $directions[$j];
        if ($currentDirection eq 'U') { $y++ if (abs($x) + abs($y + 1) <= 2) }
        if ($currentDirection eq 'L') { $x-- if (abs($x - 1) + abs($y) <= 2) }
        if ($currentDirection eq 'R') { $x++ if (abs($x + 1) + abs($y) <= 2) }
        if ($currentDirection eq 'D') { $y-- if (abs($x) + abs($y - 1) <= 2) }
    }

    $result = "$result".$lookup->{"$x,$y"};
}

print("Result is $result\n");
