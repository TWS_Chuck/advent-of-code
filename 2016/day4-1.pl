use lib '../perl-common';

use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day4.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
    $line =~ /([^\d]*)(\d+)\[(.*)\]/;
    my ($string, $sectorID, $check) = ($1, $2, $3);

    my $actual;
    foreach my $char (split //, $string) {
        next if ($char eq '-');
        $actual->{$char}++;
    }
    print (Dumper($actual));

    my $actualCheck = "";
    my $count = 0;
    foreach my $char (sort { $actual->{$b} <=> $actual->{$a} or $a cmp $b } keys %$actual) {
        $actualCheck .= $char;
        $count++;
        last if $count == 5;
    }
    print("\n$actualCheck, $check\n\n");

    $result += $sectorID if ($actualCheck eq $check);
}

print("Result is $result\n");
