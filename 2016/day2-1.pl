use lib '../perl-common';

use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day2.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = "";

my $button = 5;

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
    my @directions = split //, $line;

    for (my $j = 0; $j < scalar @directions; $j++) {
        my $currentDirection = $directions[$j];
        if ($currentDirection eq 'U') { $button -= 3 if ($button - 3 > 0) }
        if ($currentDirection eq 'L') { $button-- if ($button % 3 != 1) }
        if ($currentDirection eq 'R') { $button++ if ($button % 3 != 0) }
        if ($currentDirection eq 'D') { $button += 3 if ($button + 3 < 10) }
    }

    $result = "$result$button";
}

print("Result is $result\n");
