use lib '../perl-common';

use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day6.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = "";

my $columnCharCount = {};
for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
    my @chars = split //, $line;
    for (my $j = 0; $j < scalar @chars; $j++) {
        my $char = $chars[$j];
        $columnCharCount->{$j}{$char}++;
    }
}

foreach my $column (sort { $a <=> $b } keys %$columnCharCount) {
    my $thing = $columnCharCount->{$column};
    $result .= (sort { $thing->{$b} <=> $thing->{$a}} keys %$thing)[0]
}

print("Result is $result\n");
