use strict;
use warnings;

use CL;

use Data::Dumper;
use Math::Trig;

my $filename = "day10.txt";

my @multiArray = CL::importMultiArrayFromFile($filename);

my $result = 0;

my $width = scalar @multiArray;
my $height = scalar @{$multiArray[0]};

my $laserX = 11;
my $laserY = 19;

my $pointsByAngle = {};
for (my $i = 0; $i < scalar @multiArray; $i++) {
	for (my $j = 0; $j < scalar @{$multiArray[$i]}; $j++) {
		#current coordinate is ($j, $i), j is horizontal, and i is vertical
		next if ($multiArray[$i][$j] eq '.');
		next if ($j == $laserX && $i == $laserY);

		my $angle = rad2deg(atan2($i - $laserY, $j - $laserX));
		$angle += 360 if $angle < 0;
		push @{$pointsByAngle->{$angle}}, {x => $j, y => $i};
	}
	# print "\n";
}

my @angles = (sort {$a <=> $b} (keys %$pointsByAngle));

my $index = 144;

print "Just making sure: $angles[$index]\n";

my $asteroidsDestroyed = 0;
my $finishedAngles = {};

while(1) {
	next if not defined $pointsByAngle->{$angles[$index]};
	my @asteroids = $pointsByAngle->{$angles[$index]};
	my $didIt = 0;
	my $lasteroidX;
	my $lasteroidY;
	for (my $i = 0; $i < scalar @{$asteroids[0]}; $i++) {
		my $asteroid = $asteroids[0][$i];
		next if not defined $asteroid;
		if (canSeeAsteroid($laserX, $laserY, $asteroid->{x}, $asteroid->{y})) {
			$lasteroidX = $asteroid->{x};
			$lasteroidY = $asteroid->{y};
			$multiArray[$asteroid->{y}][$asteroid->{x}] = '.';
			$asteroids[$i] = undef;
			$didIt = 1;
			$asteroidsDestroyed++;
			print "Just destroyed an asteroid at $angles[$index] angle, with coords $lasteroidX, $lasteroidY\n";
			last;
		}
	}
	if (!$didIt) {
		# I guess there was nothing to destroy at this angle
		delete $pointsByAngle->{$angles[$index]};
		next;
	}

	if ($asteroidsDestroyed == 200) {
		my $result = $lasteroidX * 100 + $lasteroidY;
		print "FINALLY $result\n";
		exit;
	}

	$index++;
	$index = 0 if $index == scalar @angles;
}

print Dumper(@angles);

sub canSeeAsteroid {
	my $x = shift;
	my $y = shift;
	my $otherX = shift;
	my $otherY = shift;

	my $desiredPoint; # = ($x == 1 && $y == 0) ? 1 : 0;

	print("Now checking if $x $y can see $otherX $otherY\n") if $desiredPoint;

	my $vx = $otherX - $x;
	my $vy = $otherY - $y;

	my $canSee = scanInDirection($x, $y, $vx, $vy, $otherX, $otherY);
	print "Can it see? - $canSee\n" if $desiredPoint;
	return $canSee;
}

sub scanInDirection {
	my $x = shift;
	my $y = shift;
	my $vx = shift;
	my $vy = shift;
	my $otherX = shift;
	my $otherY = shift;

	return if ($vx == 0 && $vy == 0);

	my $desiredPoint; # = ($x == 1 && $y == 0) ? 1 : 0;

	my ($deltaX, $deltaY) = getRateOfChange($vx, $vy);

	print("from $x $y, and $vx $vy, we get $deltaX $deltaY\n") if $desiredPoint;

	if (not defined $deltaX) {
		print STDOUT "In scanInDirection with $x $y; $vx $vy\n";
		print STDOUT "Just got undefined deltaX\n";
	}
	if (not defined $deltaY) {
		print STDOUT "In scanInDirection with $x $y; $vx $vy\n";
		print STDOUT "Just got undefined deltaY\n";
	}

	my $scanX = $x;
	my $scanY = $y;

	do {
		$scanX += $deltaX;
		$scanY += $deltaY;

		if ($scanX == $otherX && $scanY == $otherY) {
			print STDOUT "I GIVE UP $scanX $scanY\n" if $desiredPoint;
			return 1;
		}

		if ($multiArray[$scanY][$scanX] ne '.') {
			print STDOUT "-- From $x $y, I found an asteroid at $scanX $scanY\n" if $desiredPoint;
			return 0;
		}
	} while (1);

	print STDOUT "I guess we didn't find it $scanX $scanY\n" if $desiredPoint;

	return 0;
}

sub getRateOfChange {
	my $x = shift;
	my $y = shift;

	if ($x == 0) {
		# Straight up or down
		return (0,1) if $y > 0;
		return (0,-1) if $y < 0;
	} elsif ($y == 0) {
		# Straight left or right
		return (1,0) if $x > 0;
		return (-1,0) if $x < 0;
	} else {
		my $divisor = gcd($x, $y);
		$divisor *= -1 if ($divisor < 0);

		$x /= $divisor;
		$y /= $divisor;

		return ($x,$y);
	}
}

sub gcd {
  my ($a, $b) = @_;
  ($a,$b) = ($b,$a) if $a > $b;
  while ($a) {
    ($a, $b) = ($b % $a, $a);
  }
  return $b;
}
