use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day21.txt";

my @line = CL::importSingleLineFromFile(',', $filename);

my $machine = Intcode->new(\@line);

my $instructions = "NOT C T\nAND D T\nOR T J\nNOT A T\nOR T J\nWALK\n";
my @instructionParts = split //, $instructions;
my @input;
foreach my $input (@instructionParts) {
	foreach my $char (split //, $input) {
		push @input, ord($char);
	}
}

$machine->addInput(\@input);

while(1) {
	my $result = $machine->execute();
	if ($result <= 127) {
		print chr($result);
	} else {
		print "We got it: $result\n";
	}
}