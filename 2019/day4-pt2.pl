use strict;
use warnings;

use CL;

use Data::Dumper;

my $result = 0;

for (my $i = 372037; $i < 905157; $i++) {
	my $iString = "$i";
	my @iStringParts = split //, $iString;

	my $doubleLetter = 0;
	my $increasing = 1;
	my $nums = {};
	for (my $j = 0; $j < 6; $j++) {
		if ($j < 5 && int($iStringParts[$j]) > $iStringParts[$j+1]) {
			$increasing = 0;
		}

		$nums->{$iStringParts[$j]} += 1;
	}

	next if ($increasing == 0);

	foreach my $number (keys %{$nums}) {
		$doubleLetter = 1 if ($nums->{$number} == 2);
	}

	$result++ if ($doubleLetter && $increasing);
}


print("$result\n");
