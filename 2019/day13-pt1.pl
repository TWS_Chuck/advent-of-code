use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day13.txt";

my @line = CL::importSingleLineFromFile(',', $filename);

my $result = 0;

my $machine = Intcode->new(\@line);

while (1) {
	my $x = $machine->execute();
	print Dumper($x);
	my $y = $machine->execute();
	print Dumper($y);
	my $tileID = $machine->execute();
	print Dumper($tileID);
	$result++ if $tileID == 2;

	last if $machine->{ptr} == -1;
}

print("$result\n");
