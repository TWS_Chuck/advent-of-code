use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day25.txt";

my @line = CL::importSingleLineFromFile(',', $filename);

my $machine = Intcode->new(\@line);

my $result = 0;

my $north = "north\n";
my $east = "east\n";
my $west = "west\n";
my $south = "south\n";
my $take = "take";

print "Starting...\n";

my $newLinesInARow = 0;
while(1) {
	my $output = $machine->execute();

	if (not defined $output) {
		my $thing = <STDIN>;

		my $chosenInstruction = $thing;
		my @instructionParts = split //, $chosenInstruction;

		my @input;
		foreach my $input (@instructionParts) {
			foreach my $char (split //, $input) {
				push @input, ord($char);
			}
		}

		$machine->addInput(\@input);
	} elsif ($output <= 127) {
		$newLinesInARow++ if $output == 10;
		$newLinesInARow = 0 if $output != 10;
		# print "Got something: $result\n";
		print chr($output);
	} else {
		print "Got something else: $output\n";
	}

	if ($newLinesInARow > 10) {
		# we probably dead
		$machine = Intcode->new(\@line);
		print "Let's try that again\n";
		$newLinesInARow = 0;
	}
}

print("$result\n");
