use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day3.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

my $grid = {};
my $currentX = 0;
my $currentY = 0;

# First line
my @directions = split /,/, $lines[0];
for (my $i = 0; $i < scalar @directions; $i++) {
	$directions[$i] =~ /(.)(\d+)/;
	my $direction = $1;
	my $distance = $2;

	if ($direction eq "L") {
		for (my $j = 0; $j < $distance; $j++) {
			$grid->{"$currentX"}{"$currentY"} = "X";
			$currentX--;
		}
	} elsif ($direction eq "R") {
		for (my $j = 0; $j < $distance; $j++) {
			$grid->{"$currentX"}{"$currentY"} = "X";
			$currentX++;
		}
	} elsif ($direction eq "U") {
		for (my $j = 0; $j < $distance; $j++) {
			$grid->{"$currentX"}{"$currentY"} = "X";
			$currentY++;
		}
	} elsif ($direction eq "D") {
		for (my $j = 0; $j < $distance; $j++) {
			$grid->{"$currentX"}{"$currentY"} = "X";
			$currentY--;
		}
	}
}

delete $grid->{"0"}{"0"};

$currentX = 0;
$currentY = 0;

my $smallestManDist = 9999999999;

# Second line
@directions = split /,/, $lines[1];
for (my $i = 0; $i < scalar @directions; $i++) {
	$directions[$i] =~ /(.)(\d+)/;
	my $direction = $1;
	my $distance = $2;

	if ($direction eq "L") {
		for (my $j = 0; $j < $distance; $j++) {
			if (defined $grid->{"$currentX"}{"$currentY"}) {
				my $manDistance = abs($currentX) + abs($currentY);
				$smallestManDist = $manDistance if $manDistance < $smallestManDist;
			}
			$currentX--;
		}
	} elsif ($direction eq "R") {
		for (my $j = 0; $j < $distance; $j++) {
			if (defined $grid->{"$currentX"}{"$currentY"}) {
				my $manDistance = abs($currentX) + abs($currentY);
				$smallestManDist = $manDistance if $manDistance < $smallestManDist;
			}
			$currentX++;
		}
	} elsif ($direction eq "U") {
		for (my $j = 0; $j < $distance; $j++) {
			if (defined $grid->{"$currentX"}{"$currentY"}) {
				my $manDistance = abs($currentX) + abs($currentY);
				$smallestManDist = $manDistance if $manDistance < $smallestManDist;
			}
			$currentY++;
		}
	} elsif ($direction eq "D") {
		for (my $j = 0; $j < $distance; $j++) {
			if (defined $grid->{"$currentX"}{"$currentY"}) {
				my $manDistance = abs($currentX) + abs($currentY);
				$smallestManDist = $manDistance if $manDistance < $smallestManDist;
			}
			$currentY--;
		}
	}
}

print("$smallestManDist\n");
