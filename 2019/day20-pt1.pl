use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day20.txt";

my $grid = CL::import2DGridFromFile($filename);
my $width = scalar (keys %$grid);
my $height = scalar (keys %{$grid->{0}});

my $portals = {};

findPortals();

# Start from AA, try to get to ZZ
my $startingX = $portals->{AA}{p1}{x};
my $startingY = $portals->{AA}{p1}{y};

my $result = searchForTarget($startingX, $startingY, 'ZZ');

print("$result\n");

sub searchForTarget {
	my $startingX = shift;
	my $startingY = shift;
	my $target = shift;

	# print "Looking for $target from pos $startingX, $startingY\n";
	# print Dumper($closedDoors);

	my $robots = {
		0 => {
			x => $startingX,
			y => $startingY,
			steps => 0,
		},
	};
	my $robotID = 1;

	my $gridStats = {};

	while(1) {
		# step in every direction
		my @lastRobots = keys %$robots;
		foreach my $robot (@lastRobots) {
			my $steps = $robots->{$robot}{steps};
			my $portalsRef = $robots->{$robot}{portals};
			my @portalsTaken;
			foreach my $portalTaken (@$portalsRef) {
				push @portalsTaken, $portalTaken;
			}

			foreach my $direction (1,2,3,4) {
				# 1 up 2 right 3 down 4 left
				my $newX = $robots->{$robot}{x};
				my $newY = $robots->{$robot}{y};

				$newY-- if $direction == 1;
				$newX++ if $direction == 2;
				$newY++ if $direction == 3;
				$newX-- if $direction == 4;

				next if ($newX < 1 || $newX > 121 || $newY < 1 || $newY > 121);
				next if ($grid->{$newX}{$newY} eq '#' || $grid->{$newX}{$newY} eq ' ');
				next if (defined $gridStats->{$newX}{$newY} && $gridStats->{$newX}{$newY} == 1);

				my @thisPortals = @portalsTaken;
				$robots->{$robotID} = {
					x => $newX,
					y => $newY,
					steps => $steps + 1,
					portals => \@thisPortals,
				};
				$gridStats->{$newX}{$newY} = 1;
				$robotID++;
			}

			delete $robots->{$robot};
		}

		# check each robot for goodies
		my @currentRobots = keys %$robots;
		foreach my $robot (@currentRobots) {
			my $x = $robots->{$robot}{x};
			my $y = $robots->{$robot}{y};

			my $currentTile = $grid->{$x}{$y};

			# push @{$robots->{$robot}{portals}}, $x;
			# push @{$robots->{$robot}{portals}}, $y;
			# push @{$robots->{$robot}{portals}}, ',';

			my $asciiTileCode = ord($currentTile);
			if ($grid->{$x}{$y} ne '.' && $grid->{$x}{$y} ne '#' && $grid->{$x}{$y} ne ' ') {
				# we have exited the maze into a portal
				my $portal = $portals->{$x}{$y};

				if ($portal eq 'AA') {
					delete $robots->{$robot};
					next;
				}

				if ($portal eq $target) {
					# we escaped!
					print "We escaped the maze at $x $y in $robots->{$robot}{steps}\n";
					print Dumper($robots->{$robot}{portals});
					return $robots->{$robot}{steps};
				}
				# find coordinates of other portal
				my $points = $portals->{$portal};
				if ($points->{p1}{x} == $x || $points->{p1}{y} == $y) {
					# we go to point 2
					$robots->{$robot}{x} = $points->{p2}{x};
					$robots->{$robot}{y} = $points->{p2}{y};
				} elsif ($points->{p2}{x} == $x || $points->{p2}{y} == $y) {
					# we go to point 1
					$robots->{$robot}{x} = $points->{p1}{x};
					$robots->{$robot}{y} = $points->{p1}{y};
				}

				push @{$robots->{$robot}{portals}}, '@';
				push @{$robots->{$robot}{portals}}, $portal;
				push @{$robots->{$robot}{portals}}, '@';
			}
		}

		last if (scalar keys %$robots == 0);
	}

	return -1;
}

sub findPortals {
	for (my $x = 1; $x < $width - 1; $x++) {
		for (my $y = 1; $y < $height - 1; $y++) {
			if ($grid->{$x}{$y} ne '.' && $grid->{$x}{$y} ne '#' && $grid->{$x}{$y} ne ' ') {
				my $portalString;
				my $portalX = $x;
				my $portalY = $y;
				# We're looking at some side of the portal, find which side
				if ($grid->{$x}{$y + 1} eq '.') {
					# The maze is beneath us
					$portalY++;
					$portalString = "$grid->{$x}{$y-1}$grid->{$x}{$y}";
				} elsif ($grid->{$x}{$y - 1} eq '.') {
					# The maze is above us
					$portalY--;
					$portalString = "$grid->{$x}{$y}$grid->{$x}{$y+1}";
				} elsif ($grid->{$x - 1}{$y} eq '.') {
					# The maze is left of us
					$portalX--;
					$portalString = "$grid->{$x}{$y}$grid->{$x+1}{$y}";
				} elsif ($grid->{$x + 1}{$y} eq '.') {
					# The maze is right of us
					$portalX++;
					$portalString = "$grid->{$x-1}{$y}$grid->{$x}{$y}";
				} else {
					# We're on the second part of the phrase
					next;
				}

				if (not defined $portals->{$portalString}) {
					$portals->{$portalString}{p1} = {
						x => $portalX,
						y => $portalY,
					};
				} else {
					$portals->{$portalString}{p2} = {
						x => $portalX,
						y => $portalY,
					};
				}
				$portals->{$x}{$y} = $portalString;
			}
		}
	}
}