package NewIntcode;

use strict;
use warnings;

use CL;

use Data::Dumper;

#########
# Helpers
#########

sub getValue {
	my $self = shift;
	my $ptr = shift;
	my $mode = shift;

	my $currentValue = $self->{intcodes}[$ptr];

	if ($mode == 0) {
		# Position mode - we want the value at $self->{intcodes}[$currentValue]
		# Might be undefined, set it to zero if it is
		$self->{intcodes}[$currentValue] = 0 if not defined $self->{intcodes}[$currentValue];

		return $self->{intcodes}[$currentValue];
	} elsif ($mode == 1) {
		# Immediate mode - we simply return the currentValue
		return $currentValue;
	} elsif ($mode == 2) {
		# Relative mode - like position mode, but we add the relative base to currentValue to get the new address
		my $newPtr = $currentValue + $self->{relativeBase};

		# Might be undefined, set it to zero if it is
		$self->{intcodes}[$newPtr] = 0 if not defined $self->{intcodes}[$newPtr];

		return $self->{intcodes}[$newPtr];
	} else {
		print STDOUT "Unexpected input - pointer $ptr, mode $mode\n";
	}
}

sub getWriteAddress {
	my $self = shift;
	my $ptr = shift;
	my $mode = shift;

	# print "In getWriteAddress with $ptr $mode and...\n";
	# print Dumper($self);

	my $currentValue = $self->{intcodes}[$ptr];

	if ($mode == 0) {
		# Position mode - The value at the current ptr is the address we want

		# Might be undefined, set it to zero if it is
		$self->{intcodes}[$currentValue] = 0 if not defined $self->{intcodes}[$currentValue];

		return $currentValue;
	} elsif ($mode == 2) {
		# Relative mode - like position mode, but we add the relative base to currentValue to get the new address
		my $newPtr = $currentValue + $self->{relativeBase};

		# Might be undefined, set it to zero if it is
		$self->{intcodes}[$newPtr] = 0 if not defined $self->{intcodes}[$newPtr];

		return $newPtr;
	} else {
		# Mode 1 is not supported
		print STDOUT "Unexpected input - pointer $ptr, mode $mode\n";
	}
}

############
# Operations
############

my $machine = {
	1 => \&add,
	2 => \&multiply,
	3 => \&setValue,
	4 => \&readValue,
	5 => \&jumpNotZero,
	6 => \&jumpEqualZero,
	7 => \&lessThan,
	8 => \&equals,
	9 => \&increaseRelativeBase,

	99 => \&done,
};

# 1 - add
sub add {
	my $self = shift;
	my $operation = shift;

	my $firstValue = getValue($self, $self->{ptr}+1, $operation->{parameterMode1});
	my $secondValue = getValue($self, $self->{ptr}+2, $operation->{parameterMode2});

	my $sum = $firstValue + $secondValue;

	my $writeAddress = getWriteAddress($self, $self->{ptr}+3, $operation->{parameterMode3});

	$self->{intcodes}[$writeAddress] = $sum;

	$self->{ptr} += 4;
}

# 2 - mult
sub multiply {
	my $self = shift;
	my $operation = shift;

	my $firstValue = getValue($self, $self->{ptr}+1, $operation->{parameterMode1});
	my $secondValue = getValue($self, $self->{ptr}+2, $operation->{parameterMode2});

	my $product = $firstValue * $secondValue;

	my $writeAddress = getWriteAddress($self, $self->{ptr}+3, $operation->{parameterMode3});

	$self->{intcodes}[$writeAddress] = $product;

	$self->{ptr} += 4;
}

# 3 - set
sub setValue {
	my $self = shift;
	my $operation = shift;

	my $newInput = shift @{$self->{inputs}};

	my $writeAddress = getWriteAddress($self, $self->{ptr}+1, $operation->{parameterMode1});

	$self->{intcodes}[$writeAddress] = $newInput;

	$self->{ptr} += 2;
}

# 4 - read
sub readValue {
	my $self = shift;
	my $operation = shift;

	$self->{output} = getValue($self, $self->{ptr}+1, $operation->{parameterMode1});

	$self->{ptr} += 2;
}

# 5 - jumpNotZero
sub jumpNotZero {
	my $self = shift;
	my $operation = shift;

	my $firstValue = getValue($self, $self->{ptr}+1, $operation->{parameterMode1});
	my $secondValue = getValue($self, $self->{ptr}+2, $operation->{parameterMode2});

	if ($firstValue != 0) {
		$self->{ptr} = $secondValue;
	} else {
		$self->{ptr} += 3;
	}
}

# 6 - jumpEqualZero
sub jumpEqualZero {
	my $self = shift;
	my $operation = shift;

	my $firstValue = getValue($self, $self->{ptr}+1, $operation->{parameterMode1});
	my $secondValue = getValue($self, $self->{ptr}+2, $operation->{parameterMode2});

	if ($firstValue == 0) {
		$self->{ptr} = $secondValue;
	} else {
		$self->{ptr} += 3;
	}
}

# 7 - lessThan
sub lessThan {
	my $self = shift;
	my $operation = shift;

	my $firstValue = getValue($self, $self->{ptr}+1, $operation->{parameterMode1});
	my $secondValue = getValue($self, $self->{ptr}+2, $operation->{parameterMode2});

	my $writeAddress = getWriteAddress($self, $self->{ptr}+3, $operation->{parameterMode3});

	if ($firstValue < $secondValue) {
		$self->{intcodes}[$writeAddress] = 1;
	} else {
		$self->{intcodes}[$writeAddress] = 0;
	}

	$self->{ptr} += 4;
}

# 8 - equals
sub equals {
	my $self = shift;
	my $operation = shift;

	my $firstValue = getValue($self, $self->{ptr}+1, $operation->{parameterMode1});
	my $secondValue = getValue($self, $self->{ptr}+2, $operation->{parameterMode2});

	my $writeAddress = getWriteAddress($self, $self->{ptr}+3, $operation->{parameterMode3});

	if ($firstValue == $secondValue) {
		$self->{intcodes}[$writeAddress] = 1;
	} else {
		$self->{intcodes}[$writeAddress] = 0;
	}

	$self->{ptr} += 4;
}

# 9 - relative base adjustment
sub increaseRelativeBase {
	my $self = shift;
	my $operation = shift;

	$self->{relativeBase} += getValue($self, $self->{ptr}+1, $operation->{parameterMode1});

	$self->{ptr} += 2;
}

# 99 - done
sub done {
	my $self = shift;

	$self->{ptr} = -1;
}

sub getOperationDetails {
	my $self = shift;

	my $int = $self->{intcodes}[$self->{ptr}];

	my $operation = {
		opCode => ($int % 100),
		parameterMode1 => int($int / 100) % 10,
		parameterMode2 => int($int / 1000) % 10,
		parameterMode3 => int($int / 10000) % 10,
		originalValue => $int,
	};

	return $operation;
}

sub execute {
	my $self = shift;

	while($self->{ptr} != -1) {
		my $operation = getOperationDetails($self);

		if (defined $machine->{$operation->{opCode}}) {
			# print STDOUT "Operation continues - $self->{ptr}\n";
			# print STDOUT "$operation->{originalValue}\n";
			# print STDOUT "+1 $self->{intcodes}[$self->{ptr}+1]\n" if defined $self->{intcodes}[$self->{ptr}+1];
			# print STDOUT "+2 $self->{intcodes}[$self->{ptr}+2]\n" if defined $self->{intcodes}[$self->{ptr}+2];
			# print STDOUT "+3 $self->{intcodes}[$self->{ptr}+3]\n" if defined $self->{intcodes}[$self->{ptr}+3];
			$machine->{$operation->{opCode}}($self, $operation);

			if ($operation->{opCode} == 4) {
				return $self->{output};
			}
		} else {
			print STDOUT "Operation continues - $self->{ptr}\n";
			print STDOUT "$operation->{originalValue}\n";
			print STDOUT "+1 $self->{intcodes}[$self->{ptr}+1]\n" if defined $self->{intcodes}[$self->{ptr}+1];
			print STDOUT "+2 $self->{intcodes}[$self->{ptr}+2]\n" if defined $self->{intcodes}[$self->{ptr}+2];
			print STDOUT "+3 $self->{intcodes}[$self->{ptr}+3]\n" if defined $self->{intcodes}[$self->{ptr}+3];
			print STDOUT "YIKES! something went wrong at $self->{ptr}\n";
			print STDOUT Dumper($operation);
			$self->{ptr} = -1;
		}
	}

	return $self->{output};
}

sub new {
	my $class = shift;
	my $line = shift;
	my $inputs = shift;

	my @intcodes = ();
	my @inputs = ();

	foreach my $intcode (@$line) {
		push @intcodes, int($intcode);
	}

	foreach my $input (@$inputs) {
		push @inputs, int($input);
	}

	my $self = {
		inputs => \@inputs,
		intcodes => \@intcodes,
		ptr => 0,
	};

	bless $self, $class;

	return $self;
}

sub addInput {
	my $self = shift;
	my $newInputs = shift;

	foreach my $newInput (@$newInputs) {
		push @{$self->{inputs}}, $newInput;
	}
}

1;