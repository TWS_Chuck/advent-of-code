use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day15-2.txt";

my $grid = CL::import2DGridFromFile($filename);
my $width = scalar (keys %$grid);
my $height = scalar (keys %{$grid->{0}});

# 1 North
# 2 South
# 3 West
# 4 East

my @pointsToExplore;

my $spreadingOxygen = {};
$spreadingOxygen->{"1,33"} = 1;

my $minutes = 0;
while(1) {
	# Oxygen spreads in each direction from wherever it is currently
	my @currentOxygen = keys %$spreadingOxygen;
	foreach my $oxygenLocation (@currentOxygen) {
		$oxygenLocation =~ /(\d+),(\d+)/;

		my $oxyX = $1;
		my $oxyY = $2;

		my $oxygenSpread = 0;
		foreach my $direction (1,2,3,4) {
			my $nextX = $oxyX;
			my $nextY = $oxyY;
			$nextY-- if $direction == 1;
			$nextY++ if $direction == 2;
			$nextX-- if $direction == 3;
			$nextX++ if $direction == 4;

			next if ($grid->{$nextX}{$nextY} ne '.');

			$grid->{$nextX}{$nextY} = 'O';
			$spreadingOxygen->{"$nextX,$nextY"} = 1;

			$oxygenSpread++;
		}

		delete $spreadingOxygen->{$oxygenLocation} if $oxygenSpread == 0;
	}

	$minutes++;
	printGrid() if ($minutes % 1000 == 0);

	last if (scalar (keys %$spreadingOxygen) == 0);
}

print "It took ".($minutes - 1)." minutes\n";