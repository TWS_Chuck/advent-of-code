use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day2.txt";

my @line = CL::importSingleLineFromFile(',', $filename);

$line[1] = 12;
$line[2] = 2;

my $machine = Intcode->new(\@line);

$machine->execute();

print "Index 0 is $machine->{intcodes}[0]\n";
