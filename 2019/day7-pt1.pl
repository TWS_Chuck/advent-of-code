use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day7.txt";

my @line = CL::importSingleLineFromFile(',', $filename);

my @words = (0,1,2,3,4);
my $iterator = CL::permute(\@words);

my $maxOutput = -9999;
my $bestPhaseSetting = "";
while (my @phases = $iterator->next) {
	my $lastOutput = 0;
	foreach my $phase (@phases) {
		my @inputs = (
			int($phase),
			int($lastOutput),
		);

		my @lineCopy = @line;
		my $machine = Intcode->new(\@lineCopy, \@inputs);
		$lastOutput = $machine->execute();
	}

	if ($lastOutput > $maxOutput) {
		$maxOutput = $lastOutput;
		$bestPhaseSetting = (join '', @phases);
	}
}

print "Phase $bestPhaseSetting - Output $maxOutput\n"
