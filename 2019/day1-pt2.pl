use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day1.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];

	my $mass = int($line);
	my $fuel = (int($mass / 3) - 2);

	$result += $fuel;
	my $moreFuel = $fuel;

	while ($moreFuel > 0) {
		$fuel = (int($moreFuel / 3) - 2);
		$fuel = 0 if ($fuel < 0);
		$result += $fuel;
		$moreFuel = $fuel;
	}
}

print("$result\n");
