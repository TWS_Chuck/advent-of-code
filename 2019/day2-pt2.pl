use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day2.txt";

my @line = CL::importSingleLineFromFile(',', $filename);

my $target = 19690720;

my @original = @line;
my @increaseFirstOverride = @line;
my @increaseSecondOverride = @line;

$original[1] = 12;
$original[2] = 2;

my $firstMachine = Intcode->new(\@original);
$firstMachine->execute();
my $firstValue = $firstMachine->{intcodes}[0];

$increaseFirstOverride[1] = 13;
$increaseFirstOverride[2] = 2;

my $secondMachine = Intcode->new(\@increaseFirstOverride);
$secondMachine->execute();
my $secondValue = $secondMachine->{intcodes}[0];

$increaseSecondOverride[1] = 12;
$increaseSecondOverride[2] = 3;

my $thirdMachine = Intcode->new(\@increaseSecondOverride);
$thirdMachine->execute();
my $thirdValue = $thirdMachine->{intcodes}[0];

my $firstDifference = $secondValue - $firstValue;
my $secondDifference = $thirdValue - $firstValue;

print ("Increasing parameter 1 by 1 adds $firstDifference\n");
print ("Increasing parameter 2 by 1 adds $secondDifference\n");

my $whatWeNeed = $target - $firstValue;

my $firstOverride = 12;
while ($whatWeNeed > $firstDifference) {
	$firstOverride++;
	$whatWeNeed -= $firstDifference;
}

my $secondOverride = 2;
while ($whatWeNeed >= $secondDifference) {
	$secondOverride++;
	$whatWeNeed -= $secondDifference;
}

my $result = 100 * $firstOverride + $secondOverride;

print ("$result\n");
