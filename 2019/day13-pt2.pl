use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day13.txt";

my @line = CL::importSingleLineFromFile(',', $filename);
$line[0] = 2;

my $result = 0;

my $machine = Intcode->new(\@line);
$machine->{logLevel} = Intcode::DEBUG;

my $grid = {};

my $minX =  9999;
my $minY =  9999;
my $maxX = -9999;
my $maxY = -9999;

my $score = 0;

my $currentBallX;
my $currentPaddleX;

while (1) {
    my $response = $machine->execute();

    if (not defined $response) {
        # waiting for input
        printGrid();
        
        my $nextInput = 0;
        $nextInput = 1 if $currentPaddleX < $currentBallX;
        $nextInput = -1 if $currentPaddleX > $currentBallX;

        my @nextInput = ($nextInput);
        $machine->addInput(\@nextInput);
        $response = $machine->execute();
        print ("Now response is $response\n");
    }
	my $x = $response;
	my $y = $machine->execute();
	my $tileID = $machine->execute();

    if ($x == -1 && $y == 0) {
        $score = $tileID;
        next;
    }

    if ($tileID == 3) {
        $currentPaddleX = $x;
    }
    if ($tileID == 4) {
        $currentBallX = $x;
    }

    $grid->{$x}{$y} = $tileID;

    $minX = $x if $x < $minX;
    $minY = $y if $y < $minY;
    $maxX = $x if $x > $maxX;
    $maxY = $y if $y > $maxY;

	last if $machine->{ptr} == -1;
}

print("$score\n");

sub printGrid {
    for (my $y = $minY; $y < $maxY; $y++) {
    for (my $x = $minX; $x < $maxX; $x++) {
        my $tile = $grid->{$x}{$y};
        if (not defined $tile) {
            print " ";
        } else {
            print " " if $tile == 0;
            print "W" if $tile == 1;
            print "B" if $tile == 2;
            print "P" if $tile == 3;
            print "O" if $tile == 4;
        }
    }
    print "\n";
}
}