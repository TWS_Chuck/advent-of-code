use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day24.txt";

my $grid = CL::import2DGridFromFile($filename);
my $width = scalar (keys %$grid);
my $height = scalar (keys %{$grid->{0}});

my $result = 0;

my $states;
while (1) {
	my $nextGrid = {};
	my $thisState = "";
	for (my $y = 0; $y < 5; $y++) {
		for (my $x = 0; $x < 5; $x++) {
			my $neighbours = 0;
			foreach my $direction (1,2,3,4) {
				my $nX = $x;
				my $nY = $y;
				$nX++ if $direction == 1;
				$nX-- if $direction == 2;
				$nY++ if $direction == 3;
				$nY-- if $direction == 4;

				$neighbours++ if (defined $grid->{$nX}{$nY} && $grid->{$nX}{$nY} eq '#');
			}
			
			if ($grid->{$x}{$y} eq '#') {
				$nextGrid->{$x}{$y} = '.';
				$nextGrid->{$x}{$y} = '#' if ($neighbours == 1);
			} elsif ($grid->{$x}{$y} eq '.') {
				$nextGrid->{$x}{$y} = '.';
				$nextGrid->{$x}{$y} = '#' if ($neighbours == 1 || $neighbours == 2);
			}

			$thisState .= $nextGrid->{$x}{$y};
		}
	}

	if (defined $states->{$thisState}) {
		my $result = calculateBiodiversity($nextGrid);
		print("We are done here $result\n");
		exit;
	}

	$states->{$thisState} = 1;

	$grid = $nextGrid;
}

sub calculateBiodiversity {
	my $nextGrid = shift;
	my $rating = 0;
	my $power = 0;
	for (my $y = 0; $y < $height; $y++) {
		for (my $x = 0; $x < $width; $x++) {
			if ($nextGrid->{$x}{$y} eq '#') {
				$rating += 2**$power;
			}
			$power++;
		}
	}
	return $rating;
}