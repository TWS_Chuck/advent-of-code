use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day11.txt";

my @line = CL::importSingleLineFromFile(',', $filename);

my $result = 0;

# 0 UP
# 1 RIGHT
# 2 DOWN
# 3 LEFT
my $direction => 0;

my $grid = {};

my $machine = Intcode->new(\@line);

my $painted = {};

my $currentX = 0;
my $currentY = 0;
while(1) {
	$grid->{$currentX}{$currentY} = 0 if (not defined $grid->{$currentX}{$currentY});

	my $currentTile = $grid->{$currentX}{$currentY};

	# print "Working with $currentX $currentY and the tile is $currentTile\n";

	my @newInput = ($currentTile);
	$machine->addInput(\@newInput);

	my $tileColour = $machine->execute();
	my $turnDirection = $machine->execute();

	# print "Got colour $tileColour and dir $turnDirection\n";

	$grid->{$currentX}{$currentY} = $tileColour;

	$painted->{"$currentX,$currentY"}++;

	if ($turnDirection == 1) {
		# turn right
		$direction++;
		$direction = 0 if $direction == 4;
	} elsif ($turnDirection == 0) {
		# turn left
		$direction--;
		$direction = 3 if $direction == -1;
	} else {
		print "AHHH bad turnDirection $turnDirection\n";
	}
	if ($direction == 0) {
		$currentY++;
	} elsif ($direction == 1) {
		$currentX++;
	} elsif ($direction == 2) {
		$currentY--;
	} elsif ($direction == 3) {
		$currentX--;
	} else {
		print "AHHHHHHH bad direction\n";
	}

	last if $machine->{ptr} == -1;
}

$result = scalar keys %$painted;

print("$result\n");
