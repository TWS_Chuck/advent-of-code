use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day5.txt";

my @line = CL::importSingleLineFromFile(',', $filename);

my @input = (5);

my $machine = Intcode->new(\@line, \@input);

my $output;
my $done = 0;
while (!$done) {
	$output = $machine->execute();
	$done = 1 if $machine->{ptr} == -1;
}

print "Output $output\n";
