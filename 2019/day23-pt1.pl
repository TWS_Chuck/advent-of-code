use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day23.txt";

my @line = CL::importSingleLineFromFile(',', $filename);

my $machines = {};
for (my $machineID = 0; $machineID < 50; $machineID++) {
	$machines->{$machineID} = Intcode->new(\@line);
	my @input = ($machineID);
	$machines->{$machineID}->addInput(\@input);
}

while (1) {
	my $deliveries;
	my $awaitingDelivery;
	for (my $machineID = 0; $machineID < 50; $machineID++) {
		my $firstOutput = $machines->{$machineID}->execute();
		if (not defined $firstOutput) {
			$awaitingDelivery->{$machineID} = 1;
			next;
		}

		my $address = $firstOutput;
		my $x = $machines->{$machineID}->execute();
		my $y = $machines->{$machineID}->execute();

		if ($address == 255) {
			print "First packet sent to 255, has y $y\n";
			exit;
		}

		push @{$deliveries->{$address}}, $x;
		push @{$deliveries->{$address}}, $y;
	}

	for (my $machineID = 0; $machineID < 50; $machineID++) {
		if (defined $deliveries->{$machineID}) {
			my @input;
			foreach my $input (@{$deliveries->{$machineID}}) {
				push @input, $input;
			}
			$machines->{$machineID}->addInput(\@input);
		} elsif (defined $awaitingDelivery->{$machineID}) {
			my @input = (-1);
			$machines->{$machineID}->addInput(\@input);
		}
	}
}