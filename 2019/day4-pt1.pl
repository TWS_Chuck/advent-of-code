use strict;
use warnings;

use CL;

use Data::Dumper;

my $result = 0;

for (my $i = 372037; $i < 905157; $i++) {
	my $iString = "$i";
	my @iStringParts = split //, $iString;

	my $doubleLetter = 0;
	my $increasing = 1;
	for (my $j = 0; $j < 5; $j++) {
		if ($iStringParts[$j] eq $iStringParts[$j+1]) {
			$doubleLetter = 1;
		}

		if (int($iStringParts[$j]) > $iStringParts[$j+1]) {
			$increasing = 0;
		}
	}

	$result++ if ($doubleLetter && $increasing);
}


print("$result\n");
