use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;
use POSIX;

my $filename = "day14.txt";

my $result = 0;

my @lines = CL::importLinesFromFile($filename);

my $recipes = {};
my $ingredientPresence = {};

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
	my @reactionComponents = split '=>', $line;

	my @ingredients = split ', ', $reactionComponents[0];

	$reactionComponents[1] =~ /(\d+) (.*)/;
	my $reactionCount = $1;
	my $reactionElement = $2;

	$recipes->{$reactionElement}{count} = $reactionCount;
	$ingredientPresence->{$reactionElement} += 0;

	foreach my $ingredient (@ingredients) {
		$ingredient =~ /(\d+) ([^ ]*)/;

		my $ingredientCount = $1;
		my $ingredientElement = $2;

		$recipes->{$reactionElement}{ingredients}{$ingredientElement} = $ingredientCount;
		$ingredientPresence->{$ingredientElement}++;
	}
}

print Dumper($recipes);
print Dumper($ingredientPresence);

my $neededElements = {
	"FUEL" => 1,
};
my $availableElements = {};

while (1) {
	print "We need the following elements, and in the appropriate quantities\n";
	print Dumper($neededElements);

	my @remainingElements = (keys %$neededElements);

	foreach my $neededElement (@remainingElements) {
		if (not defined $recipes->{$neededElement}) {
			print "Skipping $neededElement because there is no recipe for it\n";
			next;
		}

		my $scalar;
		if ($ingredientPresence->{$neededElement} == 0) {
			$scalar = ceil($neededElements->{$neededElement} / $recipes->{$neededElement}{count});
		} else {
			$scalar = floor($neededElements->{$neededElement} / $recipes->{$neededElement}{count});
		}
		print "Need $scalar executions of recipe for $neededElement (recipe produces $recipes->{$neededElement}{count}, we need $neededElements->{$neededElement})\n";

		foreach my $ingredient (keys %{$recipes->{$neededElement}{ingredients}}) {
			$neededElements->{$ingredient} += $scalar * $recipes->{$neededElement}{ingredients}{$ingredient};
		}

		$neededElements->{$neededElement} -= ($scalar * $recipes->{$neededElement}{count});

		if ($ingredientPresence->{$neededElement} == 0) {
			print "We are done with $neededElement\n";
			foreach my $ingredient (keys %{$recipes->{$neededElement}{ingredients}}) {
				$ingredientPresence->{$ingredient}--;
			}

			delete $recipes->{$neededElement};
		}

		delete $neededElements->{$neededElement} if ($neededElements->{$neededElement} <= 0);
	}

	last if (scalar (keys %$neededElements) == 1);

	print Dumper($recipes);
}

print ("We need $neededElements->{ORE} ore\n");
