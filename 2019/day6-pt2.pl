use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day6.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

my $orbits = {};

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];

	$line =~ /([^)]*)\)([^)]*)/;

	$orbits->{$2} = $1;
}

my @mePath;
my @destPath;

my $ptr = "YOU";
while(defined $orbits->{$ptr}) {
	push @mePath, $orbits->{$ptr};
	$ptr = $orbits->{$ptr};
}

$ptr = "SAN";
while(defined $orbits->{$ptr}) {
	push @destPath, $orbits->{$ptr};
	$ptr = $orbits->{$ptr};
}

print Dumper(@mePath);
print Dumper(@destPath);

print("$result\n");
