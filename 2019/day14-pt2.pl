use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;
use POSIX;

my $filename = "day14.txt";

my $result = 0;

my @lines = CL::importLinesFromFile($filename);

my $recipes = {};
my $ingredientPresence = {};

my $neededFuel = 1895000;

while (1) {
	for (my $i = 0; $i < scalar @lines; $i++) {
		my $line = $lines[$i];
		my @reactionComponents = split '=>', $line;

		my @ingredients = split ', ', $reactionComponents[0];

		$reactionComponents[1] =~ /(\d+) (.*)/;
		my $reactionCount = $1;
		my $reactionElement = $2;

		$recipes->{$reactionElement}{count} = $reactionCount;
		$ingredientPresence->{$reactionElement} += 0;

		foreach my $ingredient (@ingredients) {
			$ingredient =~ /(\d+) ([^ ]*)/;

			my $ingredientCount = $1;
			my $ingredientElement = $2;

			$recipes->{$reactionElement}{ingredients}{$ingredientElement} = $ingredientCount;
			$ingredientPresence->{$ingredientElement}++;
		}
	}

	my $neededElements = {
		"FUEL" => $neededFuel,
	};

	while (1) {
		my @remainingElements = (keys %$neededElements);

		foreach my $neededElement (@remainingElements) {
			next if (not defined $recipes->{$neededElement});

			my $scalar;
			if ($ingredientPresence->{$neededElement} == 0) {
				$scalar = ceil($neededElements->{$neededElement} / $recipes->{$neededElement}{count});
			} else {
				$scalar = floor($neededElements->{$neededElement} / $recipes->{$neededElement}{count});
			}

			foreach my $ingredient (keys %{$recipes->{$neededElement}{ingredients}}) {
				$neededElements->{$ingredient} += $scalar * $recipes->{$neededElement}{ingredients}{$ingredient};
			}

			$neededElements->{$neededElement} -= ($scalar * $recipes->{$neededElement}{count});

			if ($ingredientPresence->{$neededElement} == 0) {
				foreach my $ingredient (keys %{$recipes->{$neededElement}{ingredients}}) {
					$ingredientPresence->{$ingredient}--;
				}

				delete $recipes->{$neededElement};
			}

			delete $neededElements->{$neededElement} if ($neededElements->{$neededElement} <= 0);
		}

		last if (scalar (keys %$neededElements) == 1);
	}

	if ($neededElements->{ORE} > 1000000000000) {
		print ("The max fuel we can produce is ".($neededFuel-1)."\n");
		exit;
	} else {
		print "That time we got $neededFuel with $neededElements->{ORE} ore\n";
	}
	$neededFuel++;
}

