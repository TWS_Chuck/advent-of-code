use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day24.txt";

my $grid;
$grid->{0} = CL::import2DGridFromFile($filename);
my $width = scalar (keys %$grid);
my $height = scalar (keys %{$grid->{0}});

$grid->{0}{2}{2} = '?';

my $currentMinDepth = -1;
my $currentMaxDepth = 1;

for (my $y = 0; $y < 5; $y++) {
	for (my $x = 0; $x < 5; $x++) {
		$grid->{$currentMinDepth}{$x}{$y} = '.';
		$grid->{$currentMinDepth}{$x}{$y} = '?' if ($x == 2 && $y == 2);
		$grid->{$currentMaxDepth}{$x}{$y} = '.';
		$grid->{$currentMaxDepth}{$x}{$y} = '?' if ($x == 2 && $y == 2);
	}
}

for (my $minute = 0; $minute < 200; $minute++) {
	print "At minute $minute\n";
	my $nextGrid = determineNextGrid($grid);

	$currentMinDepth--;
	$currentMaxDepth++;
	for (my $y = 0; $y < 5; $y++) {
		for (my $x = 0; $x < 5; $x++) {
			$nextGrid->{$currentMinDepth}{$x}{$y} = '.';
			$nextGrid->{$currentMinDepth}{$x}{$y} = '?' if ($x == 2 && $y == 2);
			$nextGrid->{$currentMaxDepth}{$x}{$y} = '.';
			$nextGrid->{$currentMaxDepth}{$x}{$y} = '?' if ($x == 2 && $y == 2);
		}
	}

	$grid = $nextGrid;
}

my $result = bugsInGrid($grid);
print "A total of $result bugs\n";

sub printGrid {
	my $planet = shift;

	for (my $depth = $currentMinDepth; $depth <= $currentMaxDepth; $depth++) {
		print "$depth\n";
		for (my $y = 0; $y < 5; $y++) {
			for (my $x = 0; $x < 5; $x++) {
				print "$planet->{$depth}{$x}{$y}";
			}
			print "\n";
		}
		print "\n\n";
	}
}

sub determineNextGrid {
	my $planet = shift;

	my $newPlanet = {};

	for (my $depth = $currentMinDepth; $depth <= $currentMaxDepth; $depth++) {
		for (my $y = 0; $y < 5; $y++) {
			for (my $x = 0; $x < 5; $x++) {
				if (defined $planet->{$depth}{$x}{$y} && $planet->{$depth}{$x}{$y} eq '?') {
					$newPlanet->{$depth}{$x}{$y} = '?';
					next;
				}

				my $neighbours = 0;
				foreach my $direction (1,2,3,4) {
					my $nX = $x;
					my $nY = $y;
					$nX++ if $direction == 1;
					$nX-- if $direction == 2;
					$nY++ if $direction == 3;
					$nY-- if $direction == 4;

					if (defined $planet->{$depth}{$nX}{$nY} && $planet->{$depth}{$nX}{$nY} eq '#') {
						$neighbours++;
					} elsif (defined $planet->{$depth}{$nX}{$nY} && $planet->{$depth}{$nX}{$nY} eq '?') {
						$neighbours += determineNeighboursOnEdgeOfDepth($planet, $depth + 1, $direction);
					} elsif (not defined $planet->{$depth}{$nX}{$nY}) {
						$neighbours += determineIfBugOnLevelAbove($planet, $depth - 1, $direction);
					}
				}

				if ($planet->{$depth}{$x}{$y} eq '#') {
					$newPlanet->{$depth}{$x}{$y} = '.';
					$newPlanet->{$depth}{$x}{$y} = '#' if ($neighbours == 1);
				} elsif ($planet->{$depth}{$x}{$y} eq '.') {
					$newPlanet->{$depth}{$x}{$y} = '.';
					$newPlanet->{$depth}{$x}{$y} = '#' if ($neighbours == 1 || $neighbours == 2);
				}
			}
		}
	}

	return $newPlanet;
}

sub bugsInGrid {
	my $planet = shift;

	my $bugs = 0;

	for (my $depth = $currentMinDepth; $depth <= $currentMaxDepth; $depth++) {
		for (my $y = 0; $y < 5; $y++) {
			for (my $x = 0; $x < 5; $x++) {
				$bugs++ if $planet->{$depth}{$x}{$y} eq '#';
			}
		}
	}

	return $bugs;
}

sub determineNeighboursOnEdgeOfDepth {
	my $planet = shift;
	my $depth = shift;
	my $direction = shift;

	my $neighbours = 0;

	return 0 if (not defined $planet->{$depth});

	my $x = 0;
	my $y = 0;
	$x = 4 if $direction == 2;
	$y = 4 if $direction == 4;

	if ($direction == 1 || $direction == 2) {
		for (my $y = 0; $y < 5; $y++) {
			$neighbours++ if ($planet->{$depth}{$x}{$y} eq '#');
		}
	} elsif ($direction == 3 || $direction == 4) {
		for (my $x = 0; $x < 5; $x++) {
			$neighbours++ if ($planet->{$depth}{$x}{$y} eq '#');
		}
	}

	return $neighbours;
}

sub determineIfBugOnLevelAbove {
	my $planet = shift;
	my $depth = shift;
	my $direction = shift;

	return 0 if (not defined $planet->{$depth});

	my $x = 2;
	my $y = 2;
	$x++ if $direction == 1;
	$x-- if $direction == 2;
	$y++ if $direction == 3;
	$y-- if $direction == 4;

	if ($planet->{$depth}{$x}{$y} eq '#') {
		return 1;
	}

	return 0;
}