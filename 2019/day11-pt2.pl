use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day11.txt";

my @line = CL::importSingleLineFromFile(',', $filename);

my $result = 0;

# 0 UP
# 1 RIGHT
# 2 DOWN
# 3 LEFT
my $direction => 0;

my $grid = {};
$grid->{0}{0} = 1;

my $machine = Intcode->new(\@line);

my $painted = {};
my $otherCheck = 0;

my $currentX = 0;
my $currentY = 0;

my $minX = 9999;
my $minY = 9999;
my $maxX = -9999;
my $maxY = -9999;
while(1) {
	$grid->{$currentX}{$currentY} = 0 if (not defined $grid->{$currentX}{$currentY});

	my $currentTile = $grid->{$currentX}{$currentY};

	# print "Working with $currentX $currentY and the tile is $currentTile\n";

	my @newInput = ($currentTile);
	$machine->addInput(\@newInput);

	my $tileColour = $machine->execute();
	my $turnDirection = $machine->execute();

	# print "Got colour $tileColour and dir $turnDirection\n";

	$grid->{$currentX}{$currentY} = $tileColour;

	$otherCheck++ unless (defined $painted->{"$currentX,$currentY"});
	$painted->{"$currentX,$currentY"}++;

	if ($turnDirection == 1) {
		# turn right
		$direction++;
		$direction = 0 if $direction == 4;
	} elsif ($turnDirection == 0) {
		# turn left
		$direction--;
		$direction = 3 if $direction == -1;
	} else {
		print "AHHH bad turnDirection $turnDirection\n";
	}
	if ($direction == 0) {
		$currentY++;
	} elsif ($direction == 1) {
		$currentX++;
	} elsif ($direction == 2) {
		$currentY--;
	} elsif ($direction == 3) {
		$currentX--;
	} else {
		print "AHHHHHHH bad direction\n";
	}

    $minX = $currentX if $currentX < $minX;
    $minY = $currentY if $currentY < $minY;
    $maxX = $currentX if $currentX > $maxX;
    $maxY = $currentY if $currentY > $maxY;

	last if $machine->{ptr} == -1;
}

for (my $y = $maxY; $y >= $minY; $y--) {
    for (my $x = $minX; $x <= $maxX; $x++) {
        if (not defined $grid->{$x}{$y}) {
            print " ";
        } elsif (defined $grid->{$x}{$y} && $grid->{$x}{$y} == 0) {
            print " ";
        } elsif (defined $grid->{$x}{$y} && $grid->{$x}{$y} == 1) {
            print "X";
        } else {
            print "AHHHH $grid->{$x}{$y}\n";
        }
    }
    print "\n";
}
