use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day10.txt";

my $grid = CL::import2DGridFromFile($filename);

my $result = 0;

my $width = scalar (keys %$grid);
my $height = scalar (keys %{$grid->{0}});

my $maxAsteroidsSeen = -1;
my $xMax;
my $yMax;

for (my $x = 0; $x < $width; $x++) {
	for (my $y = 0; $y < $height; $y++) {
		next if ($grid->{$x}{$y} eq '.');
		my $currentAsteroidsSeen = 0;

		# Check in a circle what's around
		for (my $otherX = 0; $otherX < $width; $otherX++) {
			for (my $otherY = 0; $otherY < $height; $otherY++) {
				next if ($grid->{$otherX}{$otherY} eq '.');
				next if ($x == $otherX && $y == $otherY);

				$currentAsteroidsSeen++ if canSeeAsteroid($x, $y, $otherX, $otherY);
			}
		}

		if ($currentAsteroidsSeen > $maxAsteroidsSeen) {
			$maxAsteroidsSeen = $currentAsteroidsSeen;
			$xMax = $x;
			$yMax = $y;
		}
	}
	# print "\n";
}

print "Most asteroids seen $maxAsteroidsSeen at ($xMax, $yMax)\n";

sub canSeeAsteroid {
	my $x = shift;
	my $y = shift;
	my $otherX = shift;
	my $otherY = shift;

	my $desiredPoint; # = ($x == 1 && $y == 0) ? 1 : 0;

	print("Now checking if $x $y can see $otherX $otherY\n") if $desiredPoint;

	my $vx = $otherX - $x;
	my $vy = $otherY - $y;

	my $canSee = scanInDirection($x, $y, $vx, $vy, $otherX, $otherY);
	print "Can it see? - $canSee\n" if $desiredPoint;
	return $canSee;
}

sub scanInDirection {
	my $x = shift;
	my $y = shift;
	my $vx = shift;
	my $vy = shift;
	my $otherX = shift;
	my $otherY = shift;

	return if ($vx == 0 && $vy == 0);

	my $desiredPoint; # = ($x == 1 && $y == 0) ? 1 : 0;

	my ($deltaX, $deltaY) = getRateOfChange($vx, $vy);

	print("from $x $y, and $vx $vy, we get $deltaX $deltaY\n") if $desiredPoint;

	if (not defined $deltaX) {
		print STDOUT "In scanInDirection with $x $y; $vx $vy\n";
		print STDOUT "Just got undefined deltaX\n";
	}
	if (not defined $deltaY) {
		print STDOUT "In scanInDirection with $x $y; $vx $vy\n";
		print STDOUT "Just got undefined deltaY\n";
	}

	my $scanX = $x;
	my $scanY = $y;

	do {
		$scanX += $deltaX;
		$scanY += $deltaY;

		if ($scanX == $otherX && $scanY == $otherY) {
			print STDOUT "I GIVE UP $scanX $scanY\n" if $desiredPoint;
			return 1;
		}

		if ($grid->{$scanX}{$scanY} ne '.') {
			print STDOUT "-- From $x $y, I found an asteroid at $scanX $scanY\n" if $desiredPoint;
			return 0;
		}
	} while (1);

	print STDOUT "I guess we didn't find it $scanX $scanY\n" if $desiredPoint;

	return 0;
}

sub getRateOfChange {
	my $x = shift;
	my $y = shift;

	if ($x == 0) {
		# Straight up or down
		return (0,1) if $y > 0;
		return (0,-1) if $y < 0;
	} elsif ($y == 0) {
		# Straight left or right
		return (1,0) if $x > 0;
		return (-1,0) if $x < 0;
	} else {
		my $divisor = gcd($x, $y);
		$divisor *= -1 if ($divisor < 0);

		$x /= $divisor;
		$y /= $divisor;

		return ($x,$y);
	}
}

sub gcd {
  my ($a, $b) = @_;
  ($a,$b) = ($b,$a) if $a > $b;
  while ($a) {
    ($a, $b) = ($b % $a, $a);
  }
  return $b;
}
