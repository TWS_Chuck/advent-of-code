use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day6.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

my $orbits = {};

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];

	$line =~ /([^)]*)\)([^)]*)/;

	$orbits->{$2} = $1;
}

foreach my $orbit (keys %{$orbits}) {
	my $ptr = $orbit;

	while(defined $orbits->{$ptr}) {
		$result++;
		$ptr = $orbits->{$ptr};
	}
}

print("$result\n");
