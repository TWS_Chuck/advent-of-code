use strict;
use warnings;

use CL;

use Data::Dumper;
use Storable qw(dclone);

my $filename = "day18.txt";

my $grid = CL::import2DGridFromFile($filename);

my $startX = 40;
my $startY = 40;
# my $startX = 8;
# my $startY = 4;
# my $startX = 15;
# my $startY = 1;
print "Making sure: $grid->{$startX}{$startY}\n";

my $result = 0;

my $pathsFromXToY;
my $keysGrabbedSoFarMemo;

my $doors = {};
my $keys = {};

my $xBlocksY = {};
my $yIsBlockedByX = {};

print "Populating all the general stuff\n";
# First lets find all keys and all doors
populateGridStatsAndOtherSuchTrackables($startX, $startY);

# print Dumper($xBlocksY);

print "Trying to find the shortest path, fingers crossed\n";
# Next we are going to do some recursive tomfoolery to see if we can find the shortest path
$result = findShortestPathToGrabRemainingKeys($keys, $doors, $xBlocksY, $yIsBlockedByX, $startX, $startY, "", 0);

print("$result\n");

sub findShortestPathToGrabRemainingKeys {
	my $remainingKeys = shift;
	my $remainingDoors = shift;
	my $remainingXBlocksY = shift;
	my $remainingYIsBlockedByX = shift;
	my $currentX = shift;
	my $currentY = shift;
	my $keysGrabbed = shift;
	my $depth = shift;

	if (scalar keys %$remainingKeys == 0) {
		return 0;
	}

	my $shortestPathSoFar = 99999999;

	# Get list of things we can try to reach this step
	my @viableTargets;
	foreach my $key (keys %$remainingKeys) {
		next if (defined $remainingYIsBlockedByX->{$key});
		push @viableTargets, $key;
	}

	my $presentTile = $grid->{$currentX}{$currentY};
	# print "\n\nAt this iteration, we are $depth deep, we just grabbed $presentTile, and we have the following viable targets:\n";
	# print Dumper(@viableTargets);

	foreach my $target (@viableTargets) {
		print "$depth - Currently looking at $target, have already grabbed $keysGrabbed\n" if $depth <= 10;
		my $nextKeys = dclone($remainingKeys);
		my $nextDoors = dclone($remainingDoors);
		my $nextXBlocksY = dclone($remainingXBlocksY);
		my $nextYIsBlockedByX = dclone($remainingYIsBlockedByX);
		my $nextX;
		my $nextY;

		# target can only be key
		$nextX = $remainingKeys->{$target}{x};
		$nextY = $remainingKeys->{$target}{y};
		delete $nextKeys->{$target};

		# if this key was before some other stuff, unblock those
		foreach my $blocked (keys %{$remainingXBlocksY->{$target}}) {
			delete $nextYIsBlockedByX->{$blocked}{$target};
			delete $nextYIsBlockedByX->{$blocked} if (scalar keys %{$nextYIsBlockedByX->{$blocked}} == 0);
		}

		# also delete the associated door material
		my $door = uc($target);
		delete $nextDoors->{$door};

		delete $nextXBlocksY->{$door};
		foreach my $blocked (keys %{$remainingXBlocksY->{$door}}) {
			delete $nextYIsBlockedByX->{$blocked}{$door};
			delete $nextYIsBlockedByX->{$blocked} if (scalar keys %{$nextYIsBlockedByX->{$blocked}} == 0);
		}

		# Find steps to next target
		my $stepsRequired;
		my $currentTile = $grid->{$currentX}{$currentY};
		if (defined $pathsFromXToY->{$currentTile}{$target}) {
			$stepsRequired = searchForTarget($currentX, $currentY, $target, $nextDoors);
		} else {
			$stepsRequired = searchForTarget($currentX, $currentY, $target, $nextDoors);
			$pathsFromXToY->{$currentTile}{$target} = $stepsRequired;
		}

		next if $stepsRequired == -1;

		# Find shortest path to get remaining keys
		my $shortestRemainingPath;

		my @keysSoFar = split //, $keysGrabbed;
		my @sortedKeys = sort {$a cmp $b} @keysSoFar;
		push @sortedKeys, $target;
		my $newKeysGrabbed = join('', @sortedKeys);
		if (defined $keysGrabbedSoFarMemo->{$newKeysGrabbed}) {
			$shortestRemainingPath = $keysGrabbedSoFarMemo->{$newKeysGrabbed};
		} else {
			$shortestRemainingPath = findShortestPathToGrabRemainingKeys($nextKeys, $nextDoors, $nextXBlocksY, $nextYIsBlockedByX, $nextX, $nextY, $newKeysGrabbed, $depth + 1);
			$keysGrabbedSoFarMemo->{$newKeysGrabbed} = $shortestRemainingPath;
		}
		# print "$currentX, $currentY, and $newKeysGrabbed -> $keysGrabbedSoFarMemo->{$newKeysGrabbed}\n";

		my $thisPath = $stepsRequired + $shortestRemainingPath;

		$shortestPathSoFar = $thisPath if $thisPath < $shortestPathSoFar;
	}

	return $shortestPathSoFar;
}

# will look for target character (either a key or door)
# any search path will die if it hits a door (unless we are looking for that door)
sub searchForTarget {
	my $startingX = shift;
	my $startingY = shift;
	my $target = shift;
	my $closedDoors = shift;

	# print "Looking for $target from pos $startingX, $startingY\n";
	# print Dumper($closedDoors);

	my $robots = {
		0 => {
			x => $startingX,
			y => $startingY,
			steps => 0,
		},
	};
	my $robotID = 1;

	my $gridStats = {};

	while(1) {
		# step in every direction
		my @lastRobots = keys %$robots;
		foreach my $robot (@lastRobots) {
			my $steps = $robots->{$robot}{steps};

			foreach my $direction (1,2,3,4) {
				# 1 up 2 right 3 down 4 left
				my $newX = $robots->{$robot}{x};
				my $newY = $robots->{$robot}{y};

				$newY-- if $direction == 1;
				$newX++ if $direction == 2;
				$newY++ if $direction == 3;
				$newX-- if $direction == 4;

				next if $grid->{$newX}{$newY} eq '#';
				next if (defined $gridStats->{$newX}{$newY} && $gridStats->{$newX}{$newY} == 1);

				$robots->{$robotID} = {
					x => $newX,
					y => $newY,
					steps => $steps + 1,
				};
				$gridStats->{$newX}{$newY} = 1;
				$robotID++;
			}

			delete $robots->{$robot};
		}

		# check each robot for goodies
		my @currentRobots = keys %$robots;
		foreach my $robot (@currentRobots) {
			my $x = $robots->{$robot}{x};
			my $y = $robots->{$robot}{y};

			my $currentTile = $grid->{$x}{$y};

			# Return the steps taken if this is the tile we were looking for
			return $robots->{$robot}{steps} if ($currentTile eq $target);

			my $asciiTileCode = ord($currentTile);
			if ($asciiTileCode >= 65 && $asciiTileCode <= 90) {
				# ignore this space if the door is 'opened', otherwise delete this robot
				if (defined $closedDoors->{$currentTile}) {
					# print "Deleting robot because it ended up at this spot: $currentTile\n";
					delete $robots->{$robot};
				} else {
					# print "This door is already open: $currentTile\n";
				}
			}
		}

		last if (scalar keys %$robots == 0);
	}

	return -1;
}

sub populateGridStatsAndOtherSuchTrackables {
	my $startingX = shift;
	my $startingY = shift;

	my $robots = {
		0 => {
			x => $startingX,
			y => $startingY,
			steps => 0,
			things => {},
		},
	};
	my $robotID = 1;

	my $gridStats = {};

	while(1) {
		# step in every direction
		my @lastRobots = keys %$robots;
		foreach my $robot (@lastRobots) {
			my $steps = $robots->{$robot}{steps};

			foreach my $direction (1,2,3,4) {
				# 1 up 2 right 3 down 4 left
				my $newX = $robots->{$robot}{x};
				my $newY = $robots->{$robot}{y};
				my $things = dclone($robots->{$robot}{things});

				$newY-- if $direction == 1;
				$newX++ if $direction == 2;
				$newY++ if $direction == 3;
				$newX-- if $direction == 4;

				next if $grid->{$newX}{$newY} eq '#';
				next if (defined $gridStats->{$newX}{$newY} && $gridStats->{$newX}{$newY} == 1);

				$robots->{$robotID} = {
					x => $newX,
					y => $newY,
					steps => $steps + 1,
					things => $things,
				};
				$gridStats->{$newX}{$newY} = 1;
				$robotID++;
			}

			delete $robots->{$robot};
		}

		# check each robot for goodies
		my @currentRobots = keys %$robots;
		foreach my $robot (@currentRobots) {
			my $x = $robots->{$robot}{x};
			my $y = $robots->{$robot}{y};

			my $currentTile = $grid->{$x}{$y};

			next if $currentTile eq '.';
			next if $currentTile eq '@';

			foreach my $thing (keys %{$robots->{$robot}{things}}) {
				$xBlocksY->{$thing}{$currentTile} = 1;
				$yIsBlockedByX->{$currentTile}{$thing} = 1;
			}

			my $asciiTileCode = ord($currentTile);
			if ($asciiTileCode >= 65 && $asciiTileCode <= 90) {
				# Found something interesting!
				# print "Got a door here: $grid->{$x}{$y} and it took $robots->{$robot}{steps} steps\n";
				$robots->{$robot}{things}{$currentTile} = 1;
				$doors->{$currentTile} = {
					x => $x,
					y => $y,
				};
			} elsif ($asciiTileCode >= 97 && $asciiTileCode <= 122) {
				# print "Got a key here: $grid->{$x}{$y} and it took $robots->{$robot}{steps} steps\n";
				$robots->{$robot}{things}{$currentTile} = 1;
				$keys->{$currentTile} = {
					x => $x,
					y => $y,
				};
			}
		}

		last if (scalar keys %$robots == 0);
	}
}