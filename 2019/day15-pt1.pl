use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day15.txt";

my @line = CL::importSingleLineFromFile(',', $filename);
my $originalMachine = Intcode->new(\@line);

my $grid = {};

# 1 North
# 2 South
# 3 West
# 4 East

my $steps = 0;

my $minX = 0;
my $minY = 0;
my $maxX = 0;
my $maxY = 0;

my @pointsToExplore;

my $robotsSearching;

$robotsSearching->{0} = $originalMachine;
$robotsSearching->{0}{ptr} = 0;
$robotsSearching->{0}{relativeBase} = 0;
$robotsSearching->{0}{steps} = 0;
$robotsSearching->{0}{x} = 0;
$robotsSearching->{0}{y} = 0;
$robotsSearching->{0}{direction} = 0;
my $robotID = 1;

my $iterations = 0;
while(1) {
	my @currentRobots = (keys %$robotsSearching);

	# create new robots
	foreach my $robot (@currentRobots) {
		next if not defined $robotsSearching->{$robot};

		my $originalRobot = $robotsSearching->{$robot};

		my $currentRobotDirection = $originalRobot->{direction} // 0;

		my $robotX = $originalRobot->{x};
		my $robotY = $originalRobot->{y};

		my $originalStepsTaken = $originalRobot->{steps};

		my $desiredPosition = ($robotX == -12 && $robotY == 2) ? 1 : 0;

		# spawn a bunch of robots to explore
		foreach my $direction (1,2,3,4) {
			next if ($direction == $originalRobot->{direction});

			my $nextX = $robotX;
			my $nextY = $robotY;
			$nextY-- if $direction == 1;
			$nextY++ if $direction == 2;
			$nextX-- if $direction == 3;
			$nextX++ if $direction == 4;

			# we have already been in this direction, this robot doesn't need to exist
			next if (defined $grid->{$nextX}{$nextY});

			$robotsSearching->{$robotID} = $originalRobot->clone();

			$robotsSearching->{$robotID}{steps} = $originalStepsTaken;
			$robotsSearching->{$robotID}{x} = $robotX;
			$robotsSearching->{$robotID}{y} = $robotY;
			$robotsSearching->{$robotID}{direction} = $direction;

			$robotID++;
		}

		if ($currentRobotDirection == 0) {
			# destroy this robot and continue
			delete $robotsSearching->{0};
			next;
		}
	}

	@currentRobots = (keys %$robotsSearching);
	foreach my $robot (@currentRobots) {
		next if not defined $robotsSearching->{$robot};

		my $currentRobotDirection = $robotsSearching->{$robot}{direction} // 0;

		my $robotX = $robotsSearching->{$robot}{x};
		my $robotY = $robotsSearching->{$robot}{y};

		# move this robot in the specified direction
		my @chosenDirection = (int($currentRobotDirection));
		my $machine = $robotsSearching->{$robot};
		$machine->addInput(\@chosenDirection);

		my $result = $machine->execute();

		if ($result == 0) {
			# we bumped into a wall, so we haven't moved, time to choose new direction
			my $attemptedX = $robotX;
			my $attemptedY = $robotY;

			$attemptedY-- if $currentRobotDirection == 1;
			$attemptedY++ if $currentRobotDirection == 2;
			$attemptedX-- if $currentRobotDirection == 3;
			$attemptedX++ if $currentRobotDirection == 4;

			$grid->{$attemptedX}{$attemptedY} = '#';

			$minX = $attemptedX if $attemptedX < $minX;
			$minY = $attemptedY if $attemptedY < $minY;
			$maxX = $attemptedX if $attemptedX > $maxX;
			$maxY = $attemptedY if $attemptedY > $maxY;

			delete $robotsSearching->{$robot};
			next;
		} elsif ($result == 1) {
			# We moved, but found nothing
			$robotY-- if $currentRobotDirection == 1;
			$robotY++ if $currentRobotDirection == 2;
			$robotX-- if $currentRobotDirection == 3;
			$robotX++ if $currentRobotDirection == 4;

			$grid->{$robotX}{$robotY} = '.';

			$robotsSearching->{$robot}{steps}++;
		} elsif ($result == 2) {
			# we moved and found the oxygen!
			$robotY-- if $currentRobotDirection == 1;
			$robotY++ if $currentRobotDirection == 2;
			$robotX-- if $currentRobotDirection == 3;
			$robotX++ if $currentRobotDirection == 4;

			$grid->{$robotX}{$robotY} = 'W';

			$robotsSearching->{$robot}{steps}++;

			print "We found the oxygen at $robotX $robotY !\n It took $robotsSearching->{$robot}{steps} steps\n";

			exit;
		} else {
			print "YIKES we got something unexpected!\n";
			print Dumper($result);
			exit;
		}

		$robotsSearching->{$robot}{x} = $robotX;
		$robotsSearching->{$robot}{y} = $robotY;

		$minX = $robotX if $robotX < $minX;
		$minY = $robotY if $robotY < $minY;
		$maxX = $robotX if $robotX > $maxX;
		$maxY = $robotY if $robotY > $maxY;
	}

	$iterations++;
	printGrid();# if ($iterations % 1000 == 0);

	last if (scalar @currentRobots == 0);
}

sub printGrid {
	print "\n\n\n";
	for (my $y = $minY; $y <= $maxY; $y++) {
		for (my $x = $minX; $x <= $maxX; $x++) {
			if ($x == 0 && $y == 0) {
				print "0";
				next;
			} 
			print " " if (not defined $grid->{$x}{$y});
			print $grid->{$x}{$y} if (defined $grid->{$x}{$y});
		}
		print "\n";
	}
	print "\n\n\n";
}
