use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $result = 0;
my $filename = "day17.txt";

my @line = CL::importSingleLineFromFile(',', $filename);

my @input;
my @inputStrings = ("A,B,A,B,C,C,B,A,B,C","L,12,L,10,R,8,L,12","R,8,R,10,R,12","L,10,R,12,R,8","n");
# create machine input
foreach my $input (@inputStrings) {
	foreach my $char (split //, $input) {
		push @input, ord($char);
	}
	push @input, 10;
}

my $machine = Intcode->new(\@line, \@input);
# $machine->{logLevel} = Intcode::VERBOSE;
$machine->dump();

my $grid = {};
my $x = 0;
my $y = 0;

my $maxX = 0;
my $maxY = 0;

my $robotX;
my $robotY;

my $output;
while(1) {
	$output = $machine->execute();
	print "$output\n";

	if ($output == 10) {
		$y++;
		$x = 0;
	} else {
		$grid->{$x}{$y} = chr($output);
		$x++;
	}

	$maxX = $x if $x > $maxX;
	$maxY = $y if $y > $maxY;
}

printGrid(0,0);
print("$output\n");

sub printGrid {
	my $minX = shift;
	my $minY = shift;

	print "\n\n\n";
	for (my $y = $minY; $y <= $maxY; $y++) {
		for (my $x = $minX; $x <= $maxX; $x++) {
			print " " if (not defined $grid->{$x}{$y});
			print $grid->{$x}{$y} if (defined $grid->{$x}{$y});
		}
		print "\n";
	}
	print "\n\n\n";
}
