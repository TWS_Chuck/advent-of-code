use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;
use POSIX;

my $filename = "day16.txt";

my $origLine = "59750939545604170490448806904053996019334767199634549908834775721405739596861952646254979483184471162036292390420794027064363954885147560867913605882489622487048479055396272724159301464058399346811328233322326527416513041769256881220146486963575598109803656565965629866620042497176335792972212552985666620566167342140228123108131419565738662203188342087202064894410035696740418174710212851654722274533332525489527010152875822730659946962403568074408253218880547715921491803133272403027533886903982268040703808320401476923037465500423410637688454817997420944672193747192363987753459196311580461975618629750912028908140713295213305315022251918307904937";
# my $line = "03036732577212944063491565474664";
my $adjustedLine = $origLine x 10000;

my $offset = 5975093; # from day16.txt, first 7 digits
# my $offset = 303673;

my $zero = "0";
my $zeroLine = $zero x 6500000;
my @zeroLine = split //, $zeroLine;

my @line = split //, $adjustedLine;

print "Length of line is ".(scalar @line)."\n";
for (my $i = 0; $i < 8; $i++) {
	my $value = $line[$offset + $i];
	print "$value";
}
print "\n";

my @basePattern = (0,1,0,-1);

my $phases = 0;
while (1) {
	my @newLine = @zeroLine;

	my $ongoingSum = 0;
	for (my $digit = scalar @line - 1; $digit >= $offset; $digit--) {
		$ongoingSum += $line[$digit];

		my $newValue = abs($ongoingSum) % 10;
		$newLine[$digit] = $newValue;
	}

	@line = @newLine;

	$phases++;
	if ($phases % 1 == 0) {
		print "Just finished a phase, now at $phases\n";
		for (my $i = 0; $i < 8; $i++) {
			my $value = $line[$offset + $i];
			print "$value";
		}
		print "\n";
	}
	if ($phases == 100) {
		print "We have done 100 phases!\nThis is the result:\n";
		for (my $i = 0; $i < 8; $i++) {
			my $value = $line[$offset + $i];
			print "$value";
		}
		print "\n";
		exit;
	}
}