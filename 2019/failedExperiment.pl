use strict;
use warnings;

use CL;

use Data::Dumper;

my $input = 7139;

my $grid = {};


my $total = 0;
for (my $i = 0; $i < 300; $i++) {
	for (my $j = 0; $j < 300; $j++) {
		my $rackID = $i + 10;
		$grid->{$i}{$j} = $rackID * $j;
		$grid->{$i}{$j} += $input;
		$grid->{$i}{$j} *= $rackID;
		$grid->{$i}{$j} = (($grid->{$i}{$j} / 100) % 10);
		$grid->{$i}{$j} -= 5;
		$total += $grid->{$i}{$j};
	}
}

my $maxPowerLevel = $total;
my $xCoord = 0;
my $yCoord = 0;
my $largestSize = 300;

my ($minX, $minY, $maxX, $maxY) = (0, 0, 299, 299);

print "Currently on 300\nLargest so far is $maxPowerLevel: $xCoord,$yCoord,$largestSize\n";

for (my $size = 300; $size > 0; $size--) {

	my $topRow = 0;
	for (my $x = $minX; $x <= $maxX; $x++) {
		$topRow += $grid->{$x}{$minY};
	}

	my $bottomRow = 0;
	for (my $x = $minX; $x <= $maxX; $x++) {
		$bottomRow += $grid->{$x}{$maxY};
	}

	my $leftColum = 0;
	for (my $y = $minY; $y <= $maxY; $y++) {
		$leftColum += $grid->{$minX}{$y};
	}

	my $rightColumn = 0;
	for (my $y = $minY; $y <= $maxY; $y++) {
		$rightColumn += $grid->{$maxX}{$y};
	}

	my $yToFix;
	if ($topRow < $bottomRow) {
		$total -= $topRow;
		$yToFix = $minY;
		$minY++;
	} else {
		$total -= $bottomRow;
		$yToFix = $maxY;
		$maxY--;
	}
	
	my $xToFix;
	if ($leftColum < $rightColumn) {
		$total -= $leftColum;
		$xToFix = $minX;
		$minX++;
	} else {
		$total -= $rightColumn;
		$xToFix = $maxX;
		$maxX--;
	}

	$total += $grid->{$xToFix}{$yToFix};

	if ($total > $maxPowerLevel) {
		$maxPowerLevel = $total;
		$xCoord = $minX;
		$yCoord = $minY;
		$largestSize = $size;
	}
	print "At size $size, our total is now $total\n";

	print "Currently on $size\nLargest so far is $maxPowerLevel: $xCoord,$yCoord,$largestSize\n";
}
