use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day22.txt";

my @lines = CL::importLinesFromFile($filename);

my @deck = (0..10006);

my $result = 0;

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];

	if ($line =~ /deal with increment (\d+)/) {
		# deal out the cards
		my @newDeck;
		my $currentIndex = 0;
		my $increment = $1;
		for (my $j = 0; $j < 10007; $j++) {
			$newDeck[$currentIndex] = shift @deck;
			$currentIndex += $increment;
			$currentIndex -= 10007 if $currentIndex >= 10007;
		}
		@deck = @newDeck;
	} elsif ($line =~ /cut (\d+)/) {
		# cut cards
		for (my $j = 0; $j < $1; $j++) {
			my $newValue = shift @deck;
			push @deck, $newValue;
		}
	} elsif ($line =~ /cut -(\d+)/) {
		# cut negative N cards
		for (my $j = 0; $j < $1; $j++) {
			my $newValue = pop @deck;
			unshift @deck, $newValue;
		}
	} elsif ($line =~ /deal into new stack/) {
		# deal into new stack
		my @newDeck = reverse(@deck);
		@deck = @newDeck;
	} else {
		print "We got $line instead\n";
	}
}
for (my $i = 0; $i < 10007; $i++) {
	if ($deck[$i] == 2019) {
		print "It's at $i!\n";
	}
}

