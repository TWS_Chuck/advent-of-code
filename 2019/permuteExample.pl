use strict;
use warnings;

use CL;

use Data::Dumper;

my @words = (0,1,2,3,4);

my $iterator = CL::permute(\@words);

my $i = 0;
while (my @perm = $iterator->next) {
   print "$i - next permutation: (@perm)\n";
   $i++;
}