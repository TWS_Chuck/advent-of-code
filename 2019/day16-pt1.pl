use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;
use POSIX;

my $filename = "day16.txt";

my @line = CL::importSingleLineFromFile('', $filename);

my @basePattern = (0,1,0,-1);

my $phases = 0;
while (1) {
	my @newLine;

	for (my $digit = 1; $digit <= scalar @line; $digit++) {
		my $newValue = 0;
		
		for (my $i = 0; $i < scalar @line; $i++) {
			my $scalar = scalarToApply($digit, $i);
			# print "We are multiplying the current digit $line[$i] by $scalar\n";
			$newValue += ($line[$i] * $scalar);
		}

		# print "Before fixing up that value, we get $newValue\n";
		$newValue = abs($newValue) % 10;
		# print "And after we have $newValue\n";
		push @newLine, $newValue;
	}

	@line = @newLine;

	$phases++;
	# if ($phases % 1 == 0) {
	# 	print "Status update after $phases phases\n";
	# 	print (join('', @line));
	# 	print "\n";
	# }
	if ($phases == 100) {
		print "We have done 100 phases!\nThis is the result:\n";
		print (join('', @line));
		print "\n";
		exit;
	}
}

sub scalarToApply {
	my $digit = shift;
	my $i = shift;

	my $index = floor(($i + 1) / $digit) % 4;

	# print "For $digit, and $i...\n" if $digit == 3;
	# print "we return $basePattern[$index]\n" if $digit == 3;

	return $basePattern[$index];
}