use strict;
use warnings;

use CL;

use Data::Dumper;
use Storable qw(dclone);

my $filename = "day18-2.txt";

my $grid = CL::import2DGridFromFile($filename);

my $doors = {};
my $keys = {};

my $xBlocksY = {};
my $yIsBlockedByX = {};

my $machines = {
	0 => {
		x => 39,
		y => 39,
		keysGrabbed => "",
	},
	1 => {
		x => 41,
		y => 39,
		keysGrabbed => "",
	},
	2 => {
		x => 39,
		y => 41,
		keysGrabbed => "",
	},
	3 => {
		x => 41,
		y => 41,
		keysGrabbed => "",
	},
};

print "Populating all the general stuff\n";
# First lets find all keys and all doors
for (my $i = 0; $i < 4; $i++) {
	populateGridStatsAndOtherSuchTrackables($machines->{$i});
}

print "Trying to find the shortest path, fingers crossed\n";
# Next we are going to do some recursive tomfoolery to see if we can find the shortest path
my $pathsFromXToY;
my $keysGrabbedSoFarMemo;
my $result = findShortestPathToGrabRemainingKeys($machines, 0);

print("$result\n");

sub findShortestPathToGrabRemainingKeys {
	my $machines = shift;
	my $depth = shift;

	my $shortestPathSoFar = 99999999;

	# Get list of things we can try to reach this step
	my $viableTargets;
	foreach my $machine (keys %$machines) {
		foreach my $key (keys %{$machines->{$machine}{keys}}) {
			next if (defined $machines->{$machine}{yIsBlockedByX}{$key});
			$viableTargets->{$key} = $machine;
		}
	}

	if (scalar keys %$viableTargets == 0) {
		return 0;
	}

	foreach my $target (keys %$viableTargets) {
		my $nextMachines = dclone($machines);
		my $nextMachineMoved = $viableTargets->{$target};

		my $nextX;
		my $nextY;

		# target can only be key
		$nextX = $nextMachines->{$nextMachineMoved}{keys}{$target}{x};
		$nextY = $nextMachines->{$nextMachineMoved}{keys}{$target}{y};
		delete $nextMachines->{$nextMachineMoved}{keys}{$target};

		# if this key was before some other stuff, unblock those
		foreach my $blocked (keys %{$nextMachines->{$nextMachineMoved}{xBlocksY}{$target}}) {
			delete $nextMachines->{$nextMachineMoved}{yIsBlockedByX}{$blocked}{$target};
			delete $nextMachines->{$nextMachineMoved}{yIsBlockedByX}{$blocked} if (scalar keys %{$nextMachines->{$nextMachineMoved}{yIsBlockedByX}{$blocked}} == 0);
		}

		# also delete the associated door material
		my $door = uc($target);
		for (my $i = 0; $i < 4; $i++) {
			next if (not defined $nextMachines->{$i}{doors}{$door});
			delete $nextMachines->{$i}{doors}{$door};

			foreach my $blocked (keys %{$nextMachines->{$i}{xBlocksY}{$door}}) {
				delete $nextMachines->{$i}{yIsBlockedByX}{$blocked}{$door};
				delete $nextMachines->{$i}{yIsBlockedByX}{$blocked} if (scalar keys %{$nextMachines->{$i}{yIsBlockedByX}{$blocked}} == 0);
			}

			delete $nextMachines->{$i}{xBlocksY}{$door};
		}

		# Find steps to next target
		my $stepsRequired;

		my $currentX = $machines->{$nextMachineMoved}{x};
		my $currentY = $machines->{$nextMachineMoved}{y};
		my $machineDoors = $nextMachines->{$nextMachineMoved}{doors};

		my $currentTile = $grid->{$currentX}{$currentY};
		if (defined $pathsFromXToY->{$currentTile}{$target}) {
			$stepsRequired = $pathsFromXToY->{$currentTile}{$target};
		} else {
			$stepsRequired = searchForTarget($currentX, $currentY, $target, $machineDoors);
			$pathsFromXToY->{$currentTile}{$target} = $stepsRequired;
		}

		# Now that we have moved to target, update machine location
		$nextMachines->{$nextMachineMoved}{x} = $nextX;
		$nextMachines->{$nextMachineMoved}{y} = $nextY;

		next if $stepsRequired == -1;

		# Find shortest path to get remaining keys
		my $shortestRemainingPath;

		my $totalKeyStateString;
		for (my $i = 0; $i < 4; $i++) {
			my $keysGrabbed = $nextMachines->{$i}{keysGrabbed};
			if ($i != $nextMachineMoved) {
				$totalKeyStateString .= "$keysGrabbed,";
			} else {
				my @keysSoFar = split //, $keysGrabbed;
				my @sortedKeys = sort {$a cmp $b} @keysSoFar;
				push @sortedKeys, $target;
				my $newKeysGrabbed = join('', @sortedKeys);
				$nextMachines->{$i}{keysGrabbed} = $newKeysGrabbed;
				$totalKeyStateString .= "$newKeysGrabbed,";
			}
		}

		if (defined $keysGrabbedSoFarMemo->{$totalKeyStateString}) {
			$shortestRemainingPath = $keysGrabbedSoFarMemo->{$totalKeyStateString};
		} else {
			$shortestRemainingPath = findShortestPathToGrabRemainingKeys($nextMachines, $depth + 1);
			$keysGrabbedSoFarMemo->{$totalKeyStateString} = $shortestRemainingPath;
		}

		my $thisPath = $stepsRequired + $shortestRemainingPath;

		$shortestPathSoFar = $thisPath if $thisPath < $shortestPathSoFar;
	}

	return $shortestPathSoFar;
}

# will look for target character (either a key or door)
# any search path will die if it hits a door (unless we are looking for that door)
sub searchForTarget {
	my $startingX = shift;
	my $startingY = shift;
	my $target = shift;
	my $closedDoors = shift;

	# print "Looking for $target from pos $startingX, $startingY\n";
	# print Dumper($closedDoors);

	my $robots = {
		0 => {
			x => $startingX,
			y => $startingY,
			steps => 0,
		},
	};
	my $robotID = 1;

	my $gridStats = {};

	while(1) {
		# step in every direction
		my @lastRobots = keys %$robots;
		foreach my $robot (@lastRobots) {
			my $steps = $robots->{$robot}{steps};

			foreach my $direction (1,2,3,4) {
				# 1 up 2 right 3 down 4 left
				my $newX = $robots->{$robot}{x};
				my $newY = $robots->{$robot}{y};

				$newY-- if $direction == 1;
				$newX++ if $direction == 2;
				$newY++ if $direction == 3;
				$newX-- if $direction == 4;

				next if $grid->{$newX}{$newY} eq '#';
				next if (defined $gridStats->{$newX}{$newY} && $gridStats->{$newX}{$newY} == 1);

				$robots->{$robotID} = {
					x => $newX,
					y => $newY,
					steps => $steps + 1,
				};
				$gridStats->{$newX}{$newY} = 1;
				$robotID++;
			}

			delete $robots->{$robot};
		}

		# check each robot for goodies
		my @currentRobots = keys %$robots;
		foreach my $robot (@currentRobots) {
			my $x = $robots->{$robot}{x};
			my $y = $robots->{$robot}{y};

			my $currentTile = $grid->{$x}{$y};

			# Return the steps taken if this is the tile we were looking for
			return $robots->{$robot}{steps} if ($currentTile eq $target);

			my $asciiTileCode = ord($currentTile);
			if ($asciiTileCode >= 65 && $asciiTileCode <= 90) {
				# ignore this space if the door is 'opened', otherwise delete this robot
				if (defined $closedDoors->{$currentTile}) {
					# print "Deleting robot because it ended up at this spot: $currentTile\n";
					delete $robots->{$robot};
				} else {
					# print "This door is already open: $currentTile\n";
				}
			}
		}

		last if (scalar keys %$robots == 0);
	}

	return -1;
}

sub populateGridStatsAndOtherSuchTrackables {
	my $machine = shift;

	my $startingX = $machine->{x};
	my $startingY = $machine->{y};
	print "Making sure: $grid->{$startingX}{$startingY}\n";

	my $robots = {
		0 => {
			x => $startingX,
			y => $startingY,
			steps => 0,
			things => {},
		},
	};
	my $robotID = 1;

	my $gridStats = {};

	while(1) {
		# step in every direction
		my @lastRobots = keys %$robots;
		foreach my $robot (@lastRobots) {
			my $steps = $robots->{$robot}{steps};

			foreach my $direction (1,2,3,4) {
				# 1 up 2 right 3 down 4 left
				my $newX = $robots->{$robot}{x};
				my $newY = $robots->{$robot}{y};
				my $things = dclone($robots->{$robot}{things});

				$newY-- if $direction == 1;
				$newX++ if $direction == 2;
				$newY++ if $direction == 3;
				$newX-- if $direction == 4;

				next if $grid->{$newX}{$newY} eq '#';
				next if (defined $gridStats->{$newX}{$newY} && $gridStats->{$newX}{$newY} == 1);

				$robots->{$robotID} = {
					x => $newX,
					y => $newY,
					steps => $steps + 1,
					things => $things,
				};
				$gridStats->{$newX}{$newY} = 1;
				$robotID++;
			}

			delete $robots->{$robot};
		}

		# check each robot for goodies
		my @currentRobots = keys %$robots;
		foreach my $robot (@currentRobots) {
			my $x = $robots->{$robot}{x};
			my $y = $robots->{$robot}{y};

			my $currentTile = $grid->{$x}{$y};

			next if $currentTile eq '.';
			next if $currentTile eq '@';

			foreach my $thing (keys %{$robots->{$robot}{things}}) {
				$machine->{xBlocksY}{$thing}{$currentTile} = 1;
				$machine->{yIsBlockedByX}{$currentTile}{$thing} = 1;
			}

			my $asciiTileCode = ord($currentTile);
			if ($asciiTileCode >= 65 && $asciiTileCode <= 90) {
				# Found something interesting!
				# print "Got a door here: $grid->{$x}{$y} and it took $robots->{$robot}{steps} steps\n";
				$robots->{$robot}{things}{$currentTile} = 1;
				$machine->{doors}{$currentTile} = {
					x => $x,
					y => $y,
				};
			} elsif ($asciiTileCode >= 97 && $asciiTileCode <= 122) {
				# print "Got a key here: $grid->{$x}{$y} and it took $robots->{$robot}{steps} steps\n";
				$robots->{$robot}{things}{$currentTile} = 1;
				$machine->{keys}{$currentTile} = {
					x => $x,
					y => $y,
				};
			}
		}

		last if (scalar keys %$robots == 0);
	}
}