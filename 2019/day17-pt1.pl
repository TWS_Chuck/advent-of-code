use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $result = 0;
my $filename = "day17.txt";

my @line = CL::importSingleLineFromFile(',', $filename);

my $machine = Intcode->new(\@line);

my $grid = {};
my $x = 0;
my $y = 0;

my $maxX = 0;
my $maxY = 0;

while(1) {
	my $output = $machine->execute();
	last if ($x == 0 && $output == 10);

	if ($output == 10) {
		$y++;
		$x = 0;
	} else {
		$grid->{$x}{$y} = chr($output);
		$x++;
	}

	$maxX = $x if $x > $maxX;
	$maxY = $y if $y > $maxY;
}

print "Finished with output\n";
printGrid(0,0);

for (my $i = 0; $i <= $maxX; $i++) {
	for (my $j = 0; $j <= $maxY; $j++) {
		my $above = (defined $grid->{$i}{$j-1} && $grid->{$i}{$j-1} eq '#') ? 1 : 0;
		my $below = (defined $grid->{$i}{$j+1} && $grid->{$i}{$j+1} eq '#') ? 1 : 0;
		my $left = (defined $grid->{$i-1}{$j} && $grid->{$i-1}{$j} eq '#') ? 1 : 0;
		my $right = (defined $grid->{$i+1}{$j} && $grid->{$i+1}{$j} eq '#') ? 1 : 0;
		my $center = (defined $grid->{$i}{$j} && $grid->{$i}{$j} eq '#') ? 1 : 0;
		if ($above && $below && $left && $right && $center) {
			$result += ($i * $j);
		}
	}
}

print("$result\n");

sub printGrid {
	my $minX = shift;
	my $minY = shift;

	print "\n\n\n";
	for (my $y = $minY; $y <= $maxY; $y++) {
		for (my $x = $minX; $x <= $maxX; $x++) {
			if ($x == 0 && $y == 0) {
				print "0";
				next;
			} 
			print " " if (not defined $grid->{$x}{$y});
			print $grid->{$x}{$y} if (defined $grid->{$x}{$y});
		}
		print "\n";
	}
	print "\n\n\n";
}
