use strict;
use warnings;

use CL;

use Data::Dumper;

my $moons = {
	0 => {
		x => 8,
		y => 0,
		z => 8,
		vel => {
			x => 0,
			y => 0,
			z => 0,
		}
	},
	1 => {
		x => 0,
		y => -5,
		z => -10,
		vel => {
			x => 0,
			y => 0,
			z => 0,
		}
	},
	2 => {
		x => 16,
		y => 10,
		z => -5,
		vel => {
			x => 0,
			y => 0,
			z => 0,
		}
	},
	3 => {
		x => 19,
		y => -10,
		z => -7,
		vel => {
			x => 0,
			y => 0,
			z => 0,
		}
	},
};

my $moonHistory = {
	0 => {
		x => 8,
		y => 0,
		z => 8,
		vel => {
			x => 0,
			y => 0,
			z => 0,
		}
	},
	1 => {
		x => 0,
		y => -5,
		z => -10,
		vel => {
			x => 0,
			y => 0,
			z => 0,
		}
	},
	2 => {
		x => 16,
		y => 10,
		z => -5,
		vel => {
			x => 0,
			y => 0,
			z => 0,
		}
	},
	3 => {
		x => 19,
		y => -10,
		z => -7,
		vel => {
			x => 0,
			y => 0,
			z => 0,
		}
	},
};

my $steps = 0;
while(1) {
	# apply gravity
	for (my $j = 0; $j < 4; $j++) {
		for (my $k = $j + 1; $k < 4; $k++) {
			foreach my $thing (qw/x y z/) {
				if ($moons->{$j}{$thing} > $moons->{$k}{$thing}) {
					$moons->{$j}{vel}{$thing}--;
					$moons->{$k}{vel}{$thing}++;
				} elsif ($moons->{$j}{$thing} < $moons->{$k}{$thing}) {
					$moons->{$j}{vel}{$thing}++;
					$moons->{$k}{vel}{$thing}--;
				}
			}
		}
	}

	# apply velocity
	for (my $j = 0; $j < 4; $j++) {
		foreach my $thing (qw/x y z/) {
			$moons->{$j}{$thing} += $moons->{$j}{vel}{$thing};
		}
	}

    $steps++;

	foreach my $thing (qw/x y z/) {
        my $stateString = "";
        for (my $j = 0; $j < 4; $j++) {
            $stateString .= $moons->{$j}{$thing}.",".$moons->{$j}{vel}{$thing}.",";
		}
        if (defined $moonHistory->{$thing}{$stateString}) {
            print "$steps - We again saw these moons in this state for $thing: $stateString\nThis gives us an orbit time of: ".($steps - $moonHistory->{$thing}{$stateString})."\n";
        }
        $moonHistory->{$thing}{$stateString} = $steps;
	}
}
