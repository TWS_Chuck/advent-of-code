use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day8.txt";

my @line = CL::importSingleLineFromFile('', $filename);

my $result = 0;

my $width = 25;
my $height = 6;

my $pixelsPerLayer = $width * $height;

my $layers = {};
my $currentLayer = -1;

my $fewestZeroes = 99999;
my $fewestZeroLayer = -1;

for (my $i = 0; $i < scalar @line; $i++) {
	my $currentValue = int($line[$i]);
	# print "$i $line[$i]\n";
	# push @{$layers->{$currentLayer}{pixels}}, $currentValue;

	$layers->{$currentLayer}{0}++ if $currentValue == 0;
	$layers->{$currentLayer}{1}++ if $currentValue == 1;
	$layers->{$currentLayer}{2}++ if $currentValue == 2;


	if (($i + 1) % $pixelsPerLayer == 0) {
		if (defined $layers->{$currentLayer}{0} && $fewestZeroes > $layers->{$currentLayer}{0}) {
			$fewestZeroes = $layers->{$currentLayer}{0};
			$fewestZeroLayer = $currentLayer;
		}
		$currentLayer++;
	}
}

print Dumper($layers);

$result = $layers->{$fewestZeroLayer}{1} * $layers->{$fewestZeroLayer}{2};

print("$result\n");
