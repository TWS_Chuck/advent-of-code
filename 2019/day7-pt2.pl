use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day7.txt";

my @line = CL::importSingleLineFromFile(',', $filename);

my @words = (5,6,7,8,9);
my $iterator = CL::permute(\@words);

my $maxOutput = -9999;
my $bestPhaseSetting = "";
while (my @phases = $iterator->next) {
	my $machines = {};
	foreach my $phase (@phases) {
		my @lineCopy = @line;
		my @input = (int($phase));
		$machines->{$phase} = Intcode->new(\@lineCopy, \@input);
	}

	my $done = 0;
	my $lastOutput = 0;
	while (!$done) {
		foreach my $phase (@phases) {
			my @newInput = ($lastOutput);
			$machines->{$phase}->addInput(\@newInput);

			my $output = $machines->{$phase}->execute();

			$lastOutput = $output;

			$done = 1 if ($machines->{$phase}{ptr} == -1);
		}
	}

	if ($lastOutput > $maxOutput) {
		$maxOutput = $lastOutput;
		$bestPhaseSetting = (join '', @phases);
	}
}

print "Phase $bestPhaseSetting - Output $maxOutput\n"
