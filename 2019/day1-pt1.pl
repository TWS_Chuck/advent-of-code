use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day1.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];

	my $mass = int($line);
	$result += (int($mass / 3) - 2);
}

print("$result\n");
