use strict;
use warnings;

use CL;
use Intcode;

use bignum;
use Data::Dumper;

my $filename = "day22.txt";

my @lines = CL::importLinesFromFile($filename);

my $deckSize = 119315717514047;
my $shuffleTimes = 101741582076661;
my $deckSize = 10007;
my $partOneResult = whereDidCardGo(2019);
print "Card 2019 from a deck of 10007 goes to $partOneResult\n";

my $memo = {};

my $indexWeAreTracking = 2020;
for (my $loop = 0; $loop < 101741582076661; $loop++) {
    my $newIndex = whereDidCardComeFrom($indexWeAreTracking);
    print "We moved ".($newIndex - $indexWeAreTracking).".We started at $indexWeAreTracking, and now we're at $newIndex\n";
    $indexWeAreTracking = $newIndex;
}

# print "Finished building deckMovement objects\n";

print "The index we were tracking ended up at $indexWeAreTracking\n";

sub calculateLinearCongruence {
    my $a = 1;
    my $b = 0;

    for (my $i = 0; $i < scalar @lines; $i--) {
        my $line = $lines[$i];
        if ($line =~ /deal with increment (\d+)/) {
            # reverse deal out the cards
            my $k = $1;
            $a *= $k;
            $b *= $k;
        } elsif ($line =~ /cut (\d+)/) {
            # cut cards
            my $k = $1;
            $b -= $k;
        } elsif ($line =~ /cut -(\d+)/) {
            # cut negative N cards
            my $k = $1;
            $b += $k;
        } elsif ($line =~ /deal into new stack/) {
            # deal into new stack
            $a *= -1;
            $b = -$b - 1;
        } else {
            print "We got $line instead\n";
        }
    }

    $a = $a % $deckSize;
    $b = $b % $deckSize;

    $inv_a = ($a**MOD-2) % $deckSize;
    $inv_b = ((-$b) * $inv_a) % $deckSize;

    my @matrix = (($inv_a, $inv_b), (0, 1));

    my $Mexp = mat_pow(\@matrix)
}


# reversing card operations
sub whereDidCardComeFrom {
    my $index = shift;

    if ($memo->{$index}) {
        print "HEY! We've been here before!\n";
        return $memo->{$index};
    }
    my $originalIndex = $index;

    for (my $i = ((scalar @lines) - 1); $i >= 0; $i--) {
        my $line = $lines[$i];
        my $lastIndex = $index;

        if ($line =~ /deal with increment (\d+)/) {
            # reverse deal out the cards
            while($index % $1 != 0) {
                $index += $deckSize;
            }
            $index = $index / $1;
        } elsif ($line =~ /cut (\d+)/) {
            # cut cards
            if ($deckSize - 1 - $index >= $1) {
                $index += $1;
            } else {
                $index = $index - ($deckSize - $1);
            }
        } elsif ($line =~ /cut -(\d+)/) {
            # cut negative N cards
            if ($index >= $1) {
                $index -= $1;
            } else {
                $index = $deckSize - $1 + $index;
            }
        } elsif ($line =~ /deal into new stack/) {
            # deal into new stack
            $index = $deckSize - $index - 1;
        } else {
            print "We got $line instead\n";
        }
    }

    $memo->{$originalIndex} = $index;

    return $index;
}

sub whereDidCardGo {
    my $index = shift;

    for (my $i = 0; $i < scalar @lines; $i++) {
        my $line = $lines[$i];
        my $lastIndex = $index;

        if ($line =~ /deal with increment (\d+)/) {
            # deal out the cards
            my $newIndex = $index * $1;
            $index = $newIndex % $deckSize;
        } elsif ($line =~ /cut (\d+)/) {
            # cut cards
            if ($index >= $1) {
                $index -= $1;
            } else {
                $index = $deckSize - $1 + $index;
            }
        } elsif ($line =~ /cut -(\d+)/) {
            # cut negative N cards
            if ($deckSize - 1 - $index >= $1) {
                $index += $1;
            } else {
                $index = $index - ($deckSize - $1);
            }
        } elsif ($line =~ /deal into new stack/) {
            # deal into new stack
            $index = $deckSize - $index - 1;
        } else {
            print "We got $line instead\n";
        }
    }

    return $index;
}