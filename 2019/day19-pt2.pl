use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day19.txt";

my @line = CL::importSingleLineFromFile(',', $filename);

my $grid = {};

my $currentX = 0;
my $currentY = 4;

while(1) {
	# x and y 1 find lower bounds
	my $machine = Intcode->new(\@line);
	my @newInput = ($currentX, $currentY);
	$machine->addInput(\@newInput);
	my $originalStatus = $machine->execute();

	if ($originalStatus == 0) {
		$currentX++;
		next;
	}
	if ($currentY - 100 < 0) {
		$currentY++;
		next;
	}

	$machine = Intcode->new(\@line);
	@newInput = ($currentX, $currentY - 99);
	$machine->addInput(\@newInput);
	my $upStatus = $machine->execute();

	if ($upStatus == 0) {
		$currentY++;
		next;
	} else {
		print "So far so interesting, $currentX $currentY, 0, up 100 have been good so far\n";
	}

	$machine = Intcode->new(\@line);
	@newInput = ($currentX + 99, $currentY - 99);
	$machine->addInput(\@newInput);
	my $upRightStatus = $machine->execute();

	if ($upRightStatus == 1) {
		print "Try $currentX, ".($currentY-99)."\n";
		exit;
	} else {
		print "but not 100 to the right\n";
		$currentY++;
	}
}
