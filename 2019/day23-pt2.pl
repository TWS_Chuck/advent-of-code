use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day23.txt";

my @line = CL::importSingleLineFromFile(',', $filename);

my $machines = {};
for (my $machineID = 0; $machineID < 50; $machineID++) {
	$machines->{$machineID} = Intcode->new(\@line);
	my @input = ($machineID);
	$machines->{$machineID}->addInput(\@input);
}

my $natPacket;
my $natYDelivered;

my $loop = -1;
while (1) {
	$loop++;
	my $deliveries;
	my $awaitingDelivery;
	my $idleCount = 0;
	for (my $machineID = 0; $machineID < 50; $machineID++) {
		my $firstOutput = $machines->{$machineID}->execute();
		if (not defined $firstOutput) {
			$awaitingDelivery->{$machineID} = 1;
			$idleCount++;
			next;
		}

		my $address = $firstOutput;
		my $x = $machines->{$machineID}->execute();
		my $y = $machines->{$machineID}->execute();

		if ($address == 255) {
			$natPacket->{x} = $x;
			$natPacket->{y} = $y;
		}

		push @{$deliveries->{$address}}, $x;
		push @{$deliveries->{$address}}, $y;
	}

	for (my $machineID = 0; $machineID < 50; $machineID++) {
		if (defined $deliveries->{$machineID}) {
			my @input;
			foreach my $input (@{$deliveries->{$machineID}}) {
				push @input, $input;
			}
			$machines->{$machineID}->addInput(\@input);
		} elsif (defined $awaitingDelivery->{$machineID}) {
			my @input = (-1);
			$machines->{$machineID}->addInput(\@input);
		}
	}

	if ($idleCount == 50) {
		next if $loop == 0;
		if (defined $natYDelivered->{$natPacket->{y}}) {
			print "Nat packet delivered for a second time $natPacket->{y}\n";
			exit;
		}
		print "sending $natPacket->{y}\n";
		my @input = ($natPacket->{x}, $natPacket->{y});
		$machines->{0}->addInput(\@input);
		$natYDelivered->{$natPacket->{y}} = 1;
		# undef $natPacket;
	}
}