use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day19.txt";

my @line = CL::importSingleLineFromFile(',', $filename);

my $result = 0;
my $grid = {};

for (my $x = 0; $x < 50; $x++) {
	for (my $y = 0; $y < 50; $y++) {
		print "Trying $x $y\n";
		my @newInput = ($x, $y);
		my $machine = Intcode->new(\@line);
		$machine->addInput(\@newInput);
		my $status = $machine->execute();
		$result++ if $status == 1;

		$grid->{$x}{$y} = '#' if $status == 1;
		$grid->{$x}{$y} = '.' if $status == 0;

		print "Now status is $status\n";
	}
}

printGrid();

print("$result\n");

sub printGrid {
	my $minX = 0;
	my $minY = 0;
	my $maxX = 50;
	my $maxY = 50;

	print "\n\n\n";
	for (my $y = $minY; $y <= $maxY; $y++) {
		for (my $x = $minX; $x <= $maxX; $x++) {
			print " " if (not defined $grid->{$x}{$y});
			print $grid->{$x}{$y} if (defined $grid->{$x}{$y});
		}
		print "\n";
	}
	print "\n\n\n";
}