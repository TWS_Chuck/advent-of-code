package Intcode;

use strict;
use warnings;

use CL;

use Data::Dumper;

use constant WARN => -1;
use constant INFO => 0;
use constant OUTPUT => 1;
use constant DEBUG => 2;
use constant VERBOSE => 3;

#########
# Helpers
#########

sub getValue {
	my $self = shift;
	my $ptr = shift;
	my $mode = shift;

	my $currentValue = $self->{intcodes}[$ptr];

	if ($mode == 0) {
		# Position mode - we want the value at $self->{intcodes}[$currentValue]
		# Might be undefined, set it to zero if it is
		$self->{intcodes}[$currentValue] = 0 if not defined $self->{intcodes}[$currentValue];

		return $self->{intcodes}[$currentValue];
	} elsif ($mode == 1) {
		# Immediate mode - we simply return the currentValue
		return $currentValue;
	} elsif ($mode == 2) {
		# Relative mode - like position mode, but we add the relative base to currentValue to get the new address
		my $newPtr = $currentValue + $self->{relativeBase};

		# Might be undefined, set it to zero if it is
		$self->{intcodes}[$newPtr] = 0 if not defined $self->{intcodes}[$newPtr];

		return $self->{intcodes}[$newPtr];
	} else {
		$self->log(WARN, "Unexpected input - pointer $ptr, mode $mode");
	}
}

sub getWriteAddress {
	my $self = shift;
	my $ptr = shift;
	my $mode = shift;

	my $currentValue = $self->{intcodes}[$ptr];

	if ($mode == 0) {
		# Position mode - The value at the current ptr is the address we want

		# Might be undefined, set it to zero if it is
		$self->{intcodes}[$currentValue] = 0 if not defined $self->{intcodes}[$currentValue];

		return $currentValue;
	} elsif ($mode == 2) {
		# Relative mode - like position mode, but we add the relative base to currentValue to get the new address
		my $newPtr = $currentValue + $self->{relativeBase};

		# Might be undefined, set it to zero if it is
		$self->{intcodes}[$newPtr] = 0 if not defined $self->{intcodes}[$newPtr];

		return $newPtr;
	} else {
		# Mode 1 is not supported
		$self->log(WARN, "Unexpected input - pointer $ptr, mode $mode");
	}
}

############
# Operations
############

my $machine = {
	1 => \&add,
	2 => \&multiply,
	3 => \&setValue,
	4 => \&readValue,
	5 => \&jumpNotZero,
	6 => \&jumpEqualZero,
	7 => \&lessThan,
	8 => \&equals,
	9 => \&increaseRelativeBase,

	99 => \&done,
};

# 1 - add
sub add {
	my $self = shift;
	my $operation = shift;

	my $firstValue = getValue($self, $self->{ptr}+1, $operation->{parameterMode1});
	my $secondValue = getValue($self, $self->{ptr}+2, $operation->{parameterMode2});

	my $sum = $firstValue + $secondValue;

	my $writeAddress = getWriteAddress($self, $self->{ptr}+3, $operation->{parameterMode3});

	$self->{intcodes}[$writeAddress] = $sum;

	$self->{ptr} += 4;
}

# 2 - mult
sub multiply {
	my $self = shift;
	my $operation = shift;

	my $firstValue = getValue($self, $self->{ptr}+1, $operation->{parameterMode1});
	my $secondValue = getValue($self, $self->{ptr}+2, $operation->{parameterMode2});

	my $product = $firstValue * $secondValue;

	my $writeAddress = getWriteAddress($self, $self->{ptr}+3, $operation->{parameterMode3});

	$self->{intcodes}[$writeAddress] = $product;

	$self->{ptr} += 4;
}

# 3 - set
sub setValue {
	my $self = shift;
	my $operation = shift;

	$self->log(DEBUG, "Trying to read inputs, apparently");

	$self->log(DEBUG, "We have no inputs, but I must screen") if (scalar @{$self->{inputs}} == 0);

	my $newInput = shift @{$self->{inputs}};

	$self->log(DEBUG, "Received input of $newInput");

	my $writeAddress = getWriteAddress($self, $self->{ptr}+1, $operation->{parameterMode1});

	$self->{intcodes}[$writeAddress] = $newInput;

	$self->{ptr} += 2;
}

# 4 - read
sub readValue {
	my $self = shift;
	my $operation = shift;

	$self->{output} = getValue($self, $self->{ptr}+1, $operation->{parameterMode1});
	$self->log(OUTPUT, "Output: $self->{output}");

	$self->{ptr} += 2;
}

# 5 - jumpNotZero
sub jumpNotZero {
	my $self = shift;
	my $operation = shift;

	my $firstValue = getValue($self, $self->{ptr}+1, $operation->{parameterMode1});
	my $secondValue = getValue($self, $self->{ptr}+2, $operation->{parameterMode2});

	if ($firstValue != 0) {
		$self->{ptr} = $secondValue;
	} else {
		$self->{ptr} += 3;
	}
}

# 6 - jumpEqualZero
sub jumpEqualZero {
	my $self = shift;
	my $operation = shift;

	my $firstValue = getValue($self, $self->{ptr}+1, $operation->{parameterMode1});
	my $secondValue = getValue($self, $self->{ptr}+2, $operation->{parameterMode2});

	if ($firstValue == 0) {
		$self->{ptr} = $secondValue;
	} else {
		$self->{ptr} += 3;
	}
}

# 7 - lessThan
sub lessThan {
	my $self = shift;
	my $operation = shift;

	my $firstValue = getValue($self, $self->{ptr}+1, $operation->{parameterMode1});
	my $secondValue = getValue($self, $self->{ptr}+2, $operation->{parameterMode2});

	my $writeAddress = getWriteAddress($self, $self->{ptr}+3, $operation->{parameterMode3});

	if ($firstValue < $secondValue) {
		$self->{intcodes}[$writeAddress] = 1;
	} else {
		$self->{intcodes}[$writeAddress] = 0;
	}

	$self->{ptr} += 4;
}

# 8 - equals
sub equals {
	my $self = shift;
	my $operation = shift;

	my $firstValue = getValue($self, $self->{ptr}+1, $operation->{parameterMode1});
	my $secondValue = getValue($self, $self->{ptr}+2, $operation->{parameterMode2});

	my $writeAddress = getWriteAddress($self, $self->{ptr}+3, $operation->{parameterMode3});

	if ($firstValue == $secondValue) {
		$self->{intcodes}[$writeAddress] = 1;
	} else {
		$self->{intcodes}[$writeAddress] = 0;
	}

	$self->{ptr} += 4;
}

# 9 - relative base adjustment
sub increaseRelativeBase {
	my $self = shift;
	my $operation = shift;

	$self->{relativeBase} += getValue($self, $self->{ptr}+1, $operation->{parameterMode1});

	$self->{ptr} += 2;
}

# 99 - done
sub done {
	my $self = shift;

	$self->{ptr} = -1;
}

sub getOperationDetails {
	my $self = shift;

	my $int = $self->{intcodes}[$self->{ptr}];

	my $operation = {
		opCode => ($int % 100),
		parameterMode1 => int($int / 100) % 10,
		parameterMode2 => int($int / 1000) % 10,
		parameterMode3 => int($int / 10000) % 10,
		originalValue => $int,
	};

	return $operation;
}

sub execute {
	my $self = shift;

	while($self->{ptr} != -1) {
		my $operation = getOperationDetails($self);
		$self->log(VERBOSE, "Operation continues - $self->{ptr}");
		$self->log(VERBOSE, "$operation->{originalValue}");
		$self->log(VERBOSE, "+1 $self->{intcodes}[$self->{ptr}+1]") if defined $self->{intcodes}[$self->{ptr}+1];
		$self->log(VERBOSE, "+2 $self->{intcodes}[$self->{ptr}+2]") if defined $self->{intcodes}[$self->{ptr}+2];
		$self->log(VERBOSE, "+3 $self->{intcodes}[$self->{ptr}+3]") if defined $self->{intcodes}[$self->{ptr}+3];

		if (defined $machine->{$operation->{opCode}}) {
			if ($operation->{opCode} == 3 && scalar @{$self->{inputs}} == 0) {
				$self->log(DEBUG, "Opcode is 3, but we have no inputs");
				return;
			}

			my $result = $machine->{$operation->{opCode}}($self, $operation);

			if ($operation->{opCode} == 4) {
				return $self->{output};
			}
		} else {
			$self->log(WARN, "YIKES! something went wrong at $self->{ptr}");
			$self->log(WARN, Dumper($operation));
			$self->{ptr} = -1;
		}
	}

	return $self->{output};
}

sub log {
	my $self = shift;
	my $level = shift;
	my $msg = shift;

	my $currentLevel = $self->{logLevel};

	if ($currentLevel >= $level) {
		print STDOUT "$msg\n";
	}
}

sub new {
	my $class = shift;
	my $line = shift;
	my $inputs = shift;

	my @intcodes = ();
	my @inputs = ();

	foreach my $intcode (@$line) {
		push @intcodes, int($intcode);
	}

	foreach my $input (@$inputs) {
		push @inputs, int($input);
	}

	my $self = {
		inputs => \@inputs,
		intcodes => \@intcodes,
		ptr => 0,
		logLevel => 0,
	};

	bless $self, $class;

	return $self;
}

#Work in progress
sub clone {
	my $originalIntcodeMachine = shift;

	my $newIntcodeMachine;

	my $originalLineRef = $originalIntcodeMachine->{intcodes};
	my $originalInputRef = $originalIntcodeMachine->{inputs};
	my $originalPtr = $originalIntcodeMachine->{ptr};
	my $originalLogLevel = $originalIntcodeMachine->{logLevel};
	my $originalRelativeBase = $originalIntcodeMachine->{relativeBase};

	my @intcodes;
	foreach my $intcode (@$originalLineRef) {
		push @intcodes, int($intcode);
	}

	my @inputs;
	foreach my $input (@$originalInputRef) {
		push @inputs, $input;
	}

	$newIntcodeMachine->{intcodes} = \@intcodes;
	$newIntcodeMachine->{inputs} = \@inputs;
	$newIntcodeMachine->{ptr} = $originalPtr;
	$newIntcodeMachine->{logLevel} = $originalLogLevel;
	$newIntcodeMachine->{relativeBase} = $originalRelativeBase;

	bless $newIntcodeMachine, 'Intcode';

	return $newIntcodeMachine;
}

sub addInput {
	my $self = shift;
	my $newInputs = shift;

	foreach my $newInput (@$newInputs) {
		push @{$self->{inputs}}, $newInput;
	}
}

sub inputCount {
	my $self = shift;

	return scalar @{$self->{inputs}};
}

sub dump {
	my $self = shift;

	$self->log(INFO, "inputs: ".Dumper($self->{inputs}));
	$self->log(INFO, "ptr: ".Dumper($self->{ptr}));
	$self->log(INFO, "output: ".Dumper($self->{output}));
}

1;