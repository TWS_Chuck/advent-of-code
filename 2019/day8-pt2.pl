use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day8.txt";

my @line = CL::importSingleLineFromFile('', $filename);

my $result = 0;

my $width = 25;
my $height = 6;

my $pixelsPerLayer = $width * $height;

my $layers = {};
my $currentLayer = -1;

my $fewestZeroes = 99999;
my $fewestZeroLayer = -1;

for (my $i = 0; $i < scalar @line; $i++) {
	my $currentValue = int($line[$i]);

	push @{$layers->{$currentLayer}{pixels}}, $currentValue;

	if (($i + 1) % $pixelsPerLayer == 0) {
		if (defined $layers->{$currentLayer}{0} && $fewestZeroes > $layers->{$currentLayer}{0}) {
			$fewestZeroes = $layers->{$currentLayer}{0};
			$fewestZeroLayer = $currentLayer;
		}
		$currentLayer++;
	}
}

my @image = ();

for (my $i = 0; $i < $pixelsPerLayer; $i++) {
    for (my $j = 0; $j < $currentLayer; $j++) {
        if ($layers->{$j}{pixels}[$i] != 2) {
            push @image, $layers->{$j}{pixels}[$i];
            if ($layers->{$j}{pixels}[$i] == 1) {
                print "X";
            } else {
                print " ";
            }
            last;
        }
    }
    if (($i + 1) % $width == 0) {
        print "\n";
    }
}
