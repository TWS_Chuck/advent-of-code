use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day20.txt";

my $grid = CL::import2DGridFromFile($filename);
my $width = scalar (keys %$grid);
my $height = scalar (keys %{$grid->{0}});

my $gridInnerMin = 33;
my $gridInnerXMax = 89;
my $gridInnerYMax = 91;

my $portals = {};

# turns out each portal can only reach so many other portals
# my $outerPortals = {};
# my $innerPortals = {};
# findPortals();
# findPortalPaths();

my $outerPortals = {
	'AA' =>  {
		'portals' => {
			'TR' => {
				steps => 149,
				inner => 1,
				depth => 3,
			},
		},
	},
	'TR' =>  {
		'portals' => {
			'VO' => {
				steps => 5,
				inner => 0,
				depth => -1,
			},
			'UY' => {
				steps => 57,
				inner => 1,
				depth => 1,
			},
			'NB' => {
				steps => 55,
				inner => 1,
				depth => 1,
			},
		},
	},
	'VO' =>  {
		'portals' => {
			'TR' => {
				steps => 5,
				inner => 0,
				depth => -1,
			},
			'UY' => {
				steps => 59,
				inner => 1,
				depth => 1,
			},
			'NB' => {
				steps => 57,
				inner => 1,
				depth => 1,
			},
		},
	},
	'NB' =>  {
		'portals' => {
			'VO' => {
				steps => 1100,
				inner => 1,
				depth => 18,
			},
		},
	},
	'UY' =>  {
		'portals' => {
			'TR' => {
				steps => 427,
				inner => 1,
				depth => 7,
			},
		},
	},
};
my $innerPortals = {
	'TR' => {
		'portals' => {
			'UY' => {
				steps => 427,
				inner => 0,
				depth => -7,
			},
		},
	},
	'VO' => {
		'portals' => {
			'EV' => {
				steps => 663,
				inner => 0,
				depth => -11,
			},
		},
	},
	'EV' => {
		'portals' => {
			'ZZ' => {
				steps => 73,
				inner => 0,
				depth => 0,
			},
			'NB' => {
				steps => 437,
				inner => 0,
				depth => -7,
			}
		},
	},
	'NB' => {
		'portals' => {
			'UY' => {
				steps => 5,
				inner => 1,
				depth => 1,
			},
			'TR' => {
				steps => 55,
				inner => 0,
				depth => -1,
			},
			'VO' => {
				steps => 57,
				inner => 0,
				depth => -1,
			},
		},
	},
	'UY' => {
		'portals' => {
			'NB' => {
				steps => 5,
				inner => 1,
				depth => 1,
			},
			'TR' => {
				steps => 57,
				inner => 0,
				depth => -1,
			},
			'VO' => {
				steps => 59,
				inner => 0,
				depth => -1,
			},
		},
	},
};
# print "Outer:\n";
# print Dumper($outerPortals);
# print "Inner:\n";
# print Dumper($innerPortals);

# print "\n\n";
# print "Found paths:\n";
# print Dumper($outerPortals);

# my $result = searchForTarget($startX, $startY, 'ZZ');
my $result = betterSearchForTargetWithDepth('AA', 'ZZ');
print "Restult is $result\n";
# this is one way
# my @connectingPortals = qw/AA WV EL PL XP UY VO OG LV UN OD PM BR SL OA WW XC EV ZZ/; # results in depth -16 and steps 1076
# my @connectingPortals = qw/AA PI/; # start to end up at PI outer is 1 depth 45 steps
# let's find some loops
# PATTERN 1 (PI) XL TR UY XP PL EL WV PI
# PATTERN 2 JH CK FZ JG TH YP NB VO OG LV UN OD PM BR SL OA WW XC EV (ZZ)
#                                      11111111111111111111111                                     22222222222222222222222222222222222222222222222222222222
# my @connectingPortals = qw/AA PI XL TR UY XP PL EL WV PI XL TR UY XP PL EL WV PI XL TR VO OG LV UN OD PM BR SL OA WW XC EV JH CK FZ JG TH YP NB VO OG LV UN OD PM BR SL OA WW XC EV ZZ/;
# my @connectingPortals = qw/AA PI XL TR UY XP PL EL WV PI XL TR UY XP PL EL WV PI XL TR UY XP PL EL WV PI XL TR UY XP PL EL WV PI XL TR UY XP PL EL WV PI XL TR UY XP PL EL WV PI XL TR UY XP PL EL WV PI XL TR UY XP PL EL WV PI XL TR UY XP PL EL WV PI XL TR UY XP PL EL WV PI XL TR UY XP PL EL WV PI XL TR UY XP PL EL WV PI XL TR UY XP PL EL WV PI XL TR VO OG LV UN OD PM BR SL OA WW XC EV JH CK FZ JG TH YP NB VO OG LV UN OD PM BR SL OA WW XC EV JH CK FZ JG TH YP NB VO OG LV UN OD PM BR SL OA WW XC EV JH CK FZ JG TH YP NB VO OG LV UN OD PM BR SL OA WW XC EV JH CK FZ JG TH YP NB VO OG LV UN OD PM BR SL OA WW XC EV JH CK FZ JG TH YP NB VO OG LV UN OD PM BR SL OA WW XC EV ZZ/;
# my @connectingPortals = qw/AA WV EL PL XP UY VO OG LV UN OD PM BR SL OA WW XC EV ZZ/;
# my @connectingPortals = qw/PI XL TR UY XP PL EL WV PI/; # loop with PI outer gets us 8 depth and 484 steps
# my @connectingPortals = qw/AA PI XL TR UY XP PL EL WV PI XL TR VO OG LV UN OD PM BR SL OA WW XC EV ZZ/; #
# my @connectingPortals = qw/AA PI XL TR NB YP TH JG FZ CK JH ZZ/;

# loops
# my @connectingPortals = qw/TR NB YP TH JG FZ CK JH EV XC WW OA SL BR PM OD UN LV OG VO UY XP PL EL WV PI XL TR/; # loop of 27, 1641
# my @connectingPortals = qw/TR UY XP PL EL WV PI XL TR/; # loop of 8, 484
# my @connectingPortals = qw/TR VO OG LV UN OD PM BR SL OA WW XC EV JH CK FZ JG TH YP NB VO/; # loop of 8, 484
# my @connectingPortals = qw/VO OG LV UN OD PM BR SL OA WW XC EV JH CK FZ JG TH YP NB VO/; # INNER 1, loop of -19, 1157
# my @connectingPortals = qw/VO NB YP TH JG FZ CK JH EV XC WW OA SL BR PM OD UN LV OG VO/; # loop of 19, 1157

# my @connectingPortals = qw/PI XL TR/; # ONE SHOT
# my @connectingPortals = qw/UY XP PL EL WV/; # ONE SHOT
# my @connectingPortals = qw/UY XP PL EL WV PI XL TR/; # ONE SHOT
# my @connectingPortals = qw/NB YP TH JG FZ CK JH/; # ONE SHOT
# my @connectingPortals = qw/EV XC WW OA SL BR PM OD UN LV OG VO/; # ONE SHOT

# my @connectingPortals = qw/VO OG LV UN OD PM BR SL OA WW XC EV ZZ/; # INNER 1
# my @connectingPortals = qw/AA PI XL TR NB YP TH JG FZ CK JH EV XC WW OA SL BR PM OD UN LV OG VO UY XP PL EL WV PI XL TR VO OG LV UN OD PM BR SL OA WW XC EV JH CK FZ JG TH YP NB VO OG LV UN OD PM BR SL OA WW XC EV ZZ/; # -1 3688
# my @connectingPortals = qw/AA TR NB VO UY TR VO EV NB VO EV ZZ/; # -1 3688

# my @connectingPortals = qw/EV JH CK FZ JG TH YP NB VO OG LV UN OD PM BR SL OA WW XC EV/; # INNER 1, -19 LOOP
# my @connectingPortals = qw/TR NB YP TH JG FZ CK JH EV XC WW OA SL BR PM OD UN LV OG VO UY XP PL EL WV PI XL TR/; #
# my @connectingPortals = qw/AA TR VO EV ZZ/;
# my @connectingPortals = qw/AA PI XL TR VO OG LV UN OD PM BR SL OA WW XC EV ZZ/;

# my $currentPortal = $connectingPortals[0];
# my $inner = 0;
# my $result = 0;
# my $depth = 0;
# for (my $i = 1; $i < scalar @connectingPortals; $i++) {
# 	my $nextPortal;

# 	print "We are at $currentPortal and depth $depth and steps $result. Before us we have:\n";

# 	if ($inner) {
# 		print Dumper($innerPortals->{$currentPortal}{portals});
# 		$nextPortal = $innerPortals->{$currentPortal}{portals}{$connectingPortals[$i]};
# 	} else {
# 		print Dumper($outerPortals->{$currentPortal}{portals});
# 		$nextPortal = $outerPortals->{$currentPortal}{portals}{$connectingPortals[$i]};
# 	}

# 	print Dumper($nextPortal);
# 	exit if not defined $nextPortal;

# 	$result += $nextPortal->{steps};
# 	$depth += $nextPortal->{depth};

# 	if ($connectingPortals[$i] eq 'ZZ') {
# 		print STDOUT "We are at $connectingPortals[$i] and depth $depth and steps $result\n";
# 		exit;
# 	}

# 	if ($nextPortal->{inner} == 0) {
# 		# $depth--;
# 		$inner = 1;
# 	} else {
# 		# $depth++;
# 		$inner = 0;
# 	}
# 	$currentPortal = $connectingPortals[$i];
# }
# print STDOUT "We are at $currentPortal and depth $depth and steps $result\n";

sub betterSearchForTargetWithDepth {
	my $startingPortal = shift;
	my $targetPortal = shift;

	my $robots = {
		0 => {
			currentPortal => {
				portal => $startingPortal,
				inner => 0,
			},
			steps => 0,
			depth => 0,
		},
	};
	my $robotID = 1;

	my $shortestPathSoFar = 9999999;

	my $gridStats = {};

	while (1) {
		# produce new robots to search
		my @lastRobots = keys %$robots;
		# print "Currently dealing with this many robots\n";
		# print Dumper($robots);
		foreach my $robot (@lastRobots) {
			my $steps = $robots->{$robot}{steps};
			my $currentPortal = $robots->{$robot}{currentPortal};
			my $depth = $robots->{$robot}{depth};

			my $pathRef = $robots->{$robot}{path};
			my @path;
			foreach my $pointTaken (@$pathRef) {
				push @path, $pointTaken;
			}

			my $connectingPortals;
			if ($currentPortal->{inner}) {
				# this is an inner portal
				$connectingPortals = $innerPortals->{$currentPortal->{portal}}{portals};
			} else {
				# this is an outer portal
				$connectingPortals = $outerPortals->{$currentPortal->{portal}}{portals};
			}

			# print "This is what our robots will be able to move into:\n";
			# print Dumper($connectingPortals);

			foreach my $connectingPortal (keys %$connectingPortals) {
				my $stepsToReachPortal = $connectingPortals->{$connectingPortal}{steps};
				my $portalIsInner = $connectingPortals->{$connectingPortal}{inner};

				my $nextDepth = $depth + $connectingPortals->{$connectingPortal}{depth};
				my $totalSteps = $steps + $stepsToReachPortal;

				next if $nextDepth < 0;
				next if $totalSteps > $shortestPathSoFar;

				next if ($connectingPortal eq $startingPortal);
				next if ($depth != 0 && $connectingPortal eq $targetPortal);
				next if (defined $gridStats->{visitedPortals}{$depth}{$connectingPortal}{$portalIsInner} && $gridStats->{visitedPortals}{$depth}{$connectingPortal}{$portalIsInner} == 1);

				my @pathCopy = @path;

				push @pathCopy, $currentPortal;
				push @pathCopy, "d:$depth";
				push @pathCopy, "s:$steps";
				push @pathCopy, "@@@";

				if ($depth == 0 && $connectingPortal eq $targetPortal) {
					push @pathCopy, $connectingPortal;
					push @pathCopy, "!!!!";
					print Dumper(@pathCopy);
					$shortestPathSoFar = $totalSteps if $totalSteps < $shortestPathSoFar;
					delete $robots->{$robot};
				}

				my $nextPortalInner = 0;
				$nextPortalInner = 1 if $portalIsInner == 0;

				$robots->{$robotID} = {
					currentPortal => {
						portal => $connectingPortal,
						inner => $nextPortalInner,
					},
					steps => $totalSteps,
					depth => $nextDepth,
					path => \@pathCopy,
				};
				# print "Creating another robot:\n";
				# print Dumper($robots->{$robotID});

				$gridStats->{visitedPortals}{$depth}{$connectingPortal}{$portalIsInner} = 1;
				$robotID++;
			}

			delete $robots->{$robot};
		}

		last if (scalar keys %$robots == 0);
	}
	return $shortestPathSoFar;
}

sub betterSearchForTarget {
	my $startingPortal = shift;
	my $targetPortal = shift;

	my $robots = {
		0 => {
			currentPortal => {
				portal => $startingPortal,
				inner => 0,
			},
			steps => 0,
		},
	};
	my $robotID = 1;

	my $gridStats = {};

	while (1) {
		# produce new robots to search
		my @lastRobots = keys %$robots;
		print "Currently dealing with these robots\n";
		print Dumper($robots);
		foreach my $robot (@lastRobots) {
			my $steps = $robots->{$robot}{steps};
			my $currentPortal = $robots->{$robot}{currentPortal};

			my $connectingPortals;
			if ($currentPortal->{inner}) {
				# this is an inner portal
				$connectingPortals = $innerPortals->{$currentPortal->{portal}}{portals};
			} else {
				# this is an outer portal
				$connectingPortals = $outerPortals->{$currentPortal->{portal}}{portals};
			}

			print Dumper($connectingPortals);

			foreach my $connectingPortal (keys %$connectingPortals) {
				my $stepsToReachPortal = $connectingPortals->{$connectingPortal}{steps};
				my $portalIsInner = $connectingPortals->{$connectingPortal}{inner};

				next if (defined $gridStats->{visitedPortals}{$connectingPortal}{$portalIsInner} && $gridStats->{visitedPortals}{$connectingPortal}{$portalIsInner} == 1);

				my $totalSteps = $steps + $stepsToReachPortal;

				if ($connectingPortal eq $targetPortal) {
					return $totalSteps;
				}

				my $nextPortalInner = 0;
				$nextPortalInner = 1 if $portalIsInner == 0;

				$robots->{$robotID} = {
					currentPortal => {
						portal => $connectingPortal,
						inner => $nextPortalInner,
					},
					steps => $totalSteps,
				};
				print "Creating another robot:\n";
				print Dumper($robots->{$robotID});

				$gridStats->{visitedPortals}{$connectingPortal}{$portalIsInner} = 1;
				$robotID++;
			}

			delete $robots->{$robot};
		}

		last if (scalar keys %$robots == 0);
	}
}

sub searchForTarget {
	my $startingX = shift;
	my $startingY = shift;
	my $target = shift;

	# print "Looking for $target from pos $startingX, $startingY\n";
	# print Dumper($closedDoors);

	my $robots = {
		0 => {
			x => $startingX,
			y => $startingY,
			steps => 0,
			depth => 0,
		},
	};
	my $robotID = 1;

	my $gridStats = {};

	while(1) {
		# step in every direction
		my @lastRobots = keys %$robots;
		my $leastDepth = 9999999999;
		my $deepestDepth = 0;
		foreach my $robot (@lastRobots) {
			my $steps = $robots->{$robot}{steps};
			my $depth = $robots->{$robot}{depth};
			# my $portalsRef = $robots->{$robot}{portals};
			# my @portalsTaken;
			# foreach my $portalTaken (@$portalsRef) {
			# 	push @portalsTaken, $portalTaken;
			# }
			$leastDepth = $depth if ($leastDepth > $depth);
			$deepestDepth = $depth if ($deepestDepth < $depth);

			if (not defined $depth) {
				print "We have the following robot giving us trouble\n";
				print Dumper($robots->{$robot});
			}

			foreach my $direction (1,2,3,4) {
				# 1 up 2 right 3 down 4 left
				my $newX = $robots->{$robot}{x};
				my $newY = $robots->{$robot}{y};

				$newY-- if $direction == 1;
				$newX++ if $direction == 2;
				$newY++ if $direction == 3;
				$newX-- if $direction == 4;

				next if ($grid->{$newX}{$newY} eq '#' || $grid->{$newX}{$newY} eq ' ');
				next if (defined $gridStats->{$depth}{$newX}{$newY} && $gridStats->{$depth}{$newX}{$newY} == 1);

				# my @thisPortals = @portalsTaken;
				$robots->{$robotID} = {
					x => $newX,
					y => $newY,
					steps => $steps + 1,
					depth => $depth,
					# portals => \@thisPortals
				};
				$gridStats->{$depth}{$newX}{$newY} = 1;
				$robotID++;
			}

			delete $robots->{$robot};
		}


		# check each robot for goodies
		my @currentRobots = keys %$robots;
		print "We are currently looking between $leastDepth and $deepestDepth and we currently have ".(scalar @currentRobots)."\n;";
		foreach my $robot (@currentRobots) {
			my $x = $robots->{$robot}{x};
			my $y = $robots->{$robot}{y};
			my $depth = $robots->{$robot}{depth};

			my $currentTile = $grid->{$x}{$y};

			# push @{$robots->{$robot}{portals}}, $x;
			# push @{$robots->{$robot}{portals}}, $y;
			# push @{$robots->{$robot}{portals}}, ',';

			my $asciiTileCode = ord($currentTile);
			if ($grid->{$x}{$y} ne '.' && $grid->{$x}{$y} ne '#' && $grid->{$x}{$y} ne ' ') {
				# we have exited the maze into a portal
				my $portal = $portals->{$x}{$y};

				if ($portal eq 'AA') {
					delete $robots->{$robot};
					next;
				}

				if ($portal eq 'ZZ' && $depth != 0) {
					# this is a wall right now
					delete $robots->{$robot};
					next;
				}

				if ($portal eq $target && $depth == 0) {
					# we escaped!
					print "We escaped the maze at $x $y in $robots->{$robot}{steps}\n";
					# print Dumper($robots->{$robot}{portals});
					return $robots->{$robot}{steps};
				}

				# Find out if this portal is an innie or an outie
				my $innerPortal = 0;
				$innerPortal = 1 if ($x >= $gridInnerMin && $x <= $gridInnerXMax && $y >= $gridInnerMin && $y <= $gridInnerYMax);

				if ($innerPortal == 1) {
					# We are descending
					# print "This robot is descending $x $y $depth $portal!\n";
					$robots->{$robot}{depth} = $depth + 1;
				} else {
					if ($depth == 0) {
						# we hit an outer wall
						delete $robots->{$robot};
						next;
					} else {
						$robots->{$robot}{depth} = $depth - 1;
					}
				}

				# find coordinates of other portal
				my $points = $portals->{$portal};
				if ($points->{p1}{x} == $x || $points->{p1}{y} == $y) {
					# we go to point 2
					$robots->{$robot}{x} = $points->{p2}{x};
					$robots->{$robot}{y} = $points->{p2}{y};
				} elsif ($points->{p2}{x} == $x || $points->{p2}{y} == $y) {
					# we go to point 1
					$robots->{$robot}{x} = $points->{p1}{x};
					$robots->{$robot}{y} = $points->{p1}{y};
				}

				# push @{$robots->{$robot}{portals}}, $portal;
				# push @{$robots->{$robot}{portals}}, $robots->{$robot}{depth};
				# push @{$robots->{$robot}{portals}}, '@';
			}
		}

		last if (scalar keys %$robots == 0);
	}

	return -1;
}

sub findPortals {
	for (my $x = 1; $x < $width - 1; $x++) {
		for (my $y = 1; $y < $height - 1; $y++) {
			if ($grid->{$x}{$y} ne '.' && $grid->{$x}{$y} ne '#' && $grid->{$x}{$y} ne ' ') {
				my $portalString;
				my $portalX = $x;
				my $portalY = $y;
				# We're looking at some side of the portal, find which side
				if ($grid->{$x}{$y + 1} eq '.') {
					# The maze is beneath us
					$portalY++;
					$portalString = "$grid->{$x}{$y-1}$grid->{$x}{$y}";
				} elsif ($grid->{$x}{$y - 1} eq '.') {
					# The maze is above us
					$portalY--;
					$portalString = "$grid->{$x}{$y}$grid->{$x}{$y+1}";
				} elsif ($grid->{$x - 1}{$y} eq '.') {
					# The maze is left of us
					$portalX--;
					$portalString = "$grid->{$x}{$y}$grid->{$x+1}{$y}";
				} elsif ($grid->{$x + 1}{$y} eq '.') {
					# The maze is right of us
					$portalX++;
					$portalString = "$grid->{$x-1}{$y}$grid->{$x}{$y}";
				} else {
					# We're on the second part of the phrase
					next;
				}

				if (not defined $portals->{$portalString}) {
					$portals->{$portalString}{p1} = {
						x => $portalX,
						y => $portalY,
					};
				} else {
					$portals->{$portalString}{p2} = {
						x => $portalX,
						y => $portalY,
					};
				}
				$portals->{$x}{$y} = $portalString;

				if ($x >= $gridInnerMin && $x <= $gridInnerXMax && $y >= $gridInnerMin && $y <= $gridInnerYMax) {
					# This portal is on the inside
					$innerPortals->{$portalString}{p} = {
						x => $portalX,
						y => $portalY,
					};
				} else {
					# This portal is on the outside
					$outerPortals->{$portalString}{p} = {
						x => $portalX,
						y => $portalY,
					};
				}
			}
		}
	}
}

sub findPortalPaths {
	foreach my $innerPortal (keys %$innerPortals) {
		# print "Currently working on $innerPortal\n";
		my $startingX = $innerPortals->{$innerPortal}{p}{x};
		my $startingY = $innerPortals->{$innerPortal}{p}{y};

		my $robots = {
			0 => {
				x => $startingX,
				y => $startingY,
				steps => 0,
			},
		};
		my $robotID = 1;

		my $gridStats = {};

		while(1) {
			# step in every direction
			my @lastRobots = keys %$robots;
			foreach my $robot (@lastRobots) {
				my $steps = $robots->{$robot}{steps};

				foreach my $direction (1,2,3,4) {
					# 1 up 2 right 3 down 4 left
					my $newX = $robots->{$robot}{x};
					my $newY = $robots->{$robot}{y};

					$newY-- if $direction == 1;
					$newX++ if $direction == 2;
					$newY++ if $direction == 3;
					$newX-- if $direction == 4;

					next if ($grid->{$newX}{$newY} eq '#' || $grid->{$newX}{$newY} eq ' ');
					next if (defined $gridStats->{$newX}{$newY} && $gridStats->{$newX}{$newY} == 1);

					$robots->{$robotID} = {
						x => $newX,
						y => $newY,
						steps => $steps + 1,
					};
					$gridStats->{$newX}{$newY} = 1;
					$robotID++;
				}

				delete $robots->{$robot};
			}

			# check each robot for goodies
			my @currentRobots = keys %$robots;
			foreach my $robot (@currentRobots) {
				my $x = $robots->{$robot}{x};
				my $y = $robots->{$robot}{y};

				my $currentTile = $grid->{$x}{$y};

				my $asciiTileCode = ord($currentTile);
				if ($grid->{$x}{$y} ne '.' && $grid->{$x}{$y} ne '#' && $grid->{$x}{$y} ne ' ') {
					# print "We have $grid->{$x}{$y} at point $x $y it took us $robots->{$robot}{steps} to get here\n";
					# we have found a portal
					my $portal = $portals->{$x}{$y};
					if (defined $portal && $portal ne $innerPortal) {
						my $inner = 0;
						$inner = 1 if ($x >= $gridInnerMin && $x <= $gridInnerXMax && $y >= $gridInnerMin && $y <= $gridInnerYMax);
						$innerPortals->{$innerPortal}{portals}{$portal}{steps} = $robots->{$robot}{steps};
						$innerPortals->{$innerPortal}{portals}{$portal}{inner} = $inner;
					}
					delete $robots->{$robot};
					next;
				}
			}

			last if (scalar keys %$robots == 0);
		}
	}

	foreach my $outerPortal (keys %$outerPortals) {
		# print "Currently working on $outerPortal\n";
		my $startingX = $outerPortals->{$outerPortal}{p}{x};
		my $startingY = $outerPortals->{$outerPortal}{p}{y};

		my $robots = {
			0 => {
				x => $startingX,
				y => $startingY,
				steps => 0,
			},
		};
		my $robotID = 1;

		my $gridStats = {};

		while(1) {
			# step in every direction
			my @lastRobots = keys %$robots;
			foreach my $robot (@lastRobots) {
				my $steps = $robots->{$robot}{steps};

				foreach my $direction (1,2,3,4) {
					# 1 up 2 right 3 down 4 left
					my $newX = $robots->{$robot}{x};
					my $newY = $robots->{$robot}{y};

					$newY-- if $direction == 1;
					$newX++ if $direction == 2;
					$newY++ if $direction == 3;
					$newX-- if $direction == 4;

					next if ($grid->{$newX}{$newY} eq '#' || $grid->{$newX}{$newY} eq ' ');
					next if (defined $gridStats->{$newX}{$newY} && $gridStats->{$newX}{$newY} == 1);

					$robots->{$robotID} = {
						x => $newX,
						y => $newY,
						steps => $steps + 1,
					};
					$gridStats->{$newX}{$newY} = 1;
					$robotID++;
				}

				delete $robots->{$robot};
			}

			# check each robot for goodies
			my @currentRobots = keys %$robots;
			foreach my $robot (@currentRobots) {
				my $x = $robots->{$robot}{x};
				my $y = $robots->{$robot}{y};

				my $currentTile = $grid->{$x}{$y};

				my $asciiTileCode = ord($currentTile);
				if ($grid->{$x}{$y} ne '.' && $grid->{$x}{$y} ne '#' && $grid->{$x}{$y} ne ' ') {
					# print "We have $grid->{$x}{$y} at point $x $y it took us $robots->{$robot}{steps} to get here\n";
					# we have found a portal
					my $portal = $portals->{$x}{$y};
					if (defined $portal && $portal ne $outerPortal) {
						my $inner = 0;
						$inner = 1 if ($x >= $gridInnerMin && $x <= $gridInnerXMax && $y >= $gridInnerMin && $y <= $gridInnerYMax);
						$outerPortals->{$outerPortal}{portals}{$portal}{steps} = $robots->{$robot}{steps};
						$outerPortals->{$outerPortal}{portals}{$portal}{inner} = $inner;
					}
					delete $robots->{$robot};
					next;
				}
			}

			last if (scalar keys %$robots == 0);
		}
	}
}
