use strict;
use warnings;

use CL;
use Intcode;

use Data::Dumper;

my $filename = "day9.txt";

my @line = CL::importSingleLineFromFile(',', $filename);

my $result = 0;

my @inputs = (1);

my $machine = Intcode->new(\@line, \@inputs);
my $output = $machine->execute();

print("$output\n");
