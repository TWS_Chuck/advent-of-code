use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day16-2.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

my $opCodes = {
	addr => 8,
	addi => 12,
	mulr => 0,
 	muli => 5,
	banr => 9,
	bani => 7,
 	borr => 6,
	bori => 15,
	setr => 2,
	seti => 14,
	gtir => 11,
	gtri => 13,
	gtrr => 4,
	eqir => 10,
	eqri => 1,
	eqrr => 3,
};

my $registers = {
	0 => 0,
	1 => 0,
	2 => 0,
	3 => 0,
};

for (my $i = 0; $i < scalar @lines; $i++) {
	$lines[$i] =~ /(\d+) (\d+) (\d+) (\d+)/;
	my $op = $1;
	my $instruction = {
		a => int($2),
		b => int($3),
		c => int($4),
	};

	# Addition
	# addr
	if ($opCodes->{"addr"} eq $op) {
		$registers->{$instruction->{c}} = $registers->{$instruction->{a}} + $registers->{$instruction->{b}};
	}

	# addi
	if ($opCodes->{"addi"} eq $op) {
		$registers->{$instruction->{c}} = $registers->{$instruction->{a}} + $instruction->{b};
	}

	# Mult
	# mulr
	if ($opCodes->{"mulr"} eq $op) {
		$registers->{$instruction->{c}} = $registers->{$instruction->{a}} * $registers->{$instruction->{b}};
	}

	# muli
	if ($opCodes->{"muli"} eq $op) {
		$registers->{$instruction->{c}} = $registers->{$instruction->{a}} * $instruction->{b};
	}

	# Bitwise AND
	# banr
	if ($opCodes->{"banr"} eq $op) {
		$registers->{$instruction->{c}} = $registers->{$instruction->{a}} & $registers->{$instruction->{b}};
	}

	# bani
	if ($opCodes->{"bani"} eq $op) {
		$registers->{$instruction->{c}} = $registers->{$instruction->{a}} & $instruction->{b};
	}

	# Bitwise OR
	# borr
	if ($opCodes->{"borr"} eq $op) {
		$registers->{$instruction->{c}} = $registers->{$instruction->{a}} | $registers->{$instruction->{b}};
	}

	# bori
	if ($opCodes->{"bori"} eq $op) {
		$registers->{$instruction->{c}} = $registers->{$instruction->{a}} | $instruction->{b};
	}

	# Assignment
	# setr
	if ($opCodes->{"setr"} eq $op) {
		$registers->{$instruction->{c}} = $registers->{$instruction->{a}};
	}

	# seti
	if ($opCodes->{"seti"} eq $op) {
		$registers->{$instruction->{c}} = $instruction->{a};
	}

	# Greater than
	# gtir
	if ($opCodes->{"gtir"} eq $op) {
		$registers->{$instruction->{c}} = ($instruction->{a} > $registers->{$instruction->{b}}) ? 1 : 0;
	}

	# gtri
	if ($opCodes->{"gtri"} eq $op) {
		$registers->{$instruction->{c}} = ($registers->{$instruction->{a}} > $instruction->{b}) ? 1 : 0;
	}

	# gtrr
	if ($opCodes->{"gtrr"} eq $op) {
		$registers->{$instruction->{c}} = ($registers->{$instruction->{a}} > $registers->{$instruction->{b}}) ? 1 : 0;
	}

	# Equality
	# eqir
	if ($opCodes->{"eqir"} eq $op) {
		$registers->{$instruction->{c}} = ($instruction->{a} == $registers->{$instruction->{b}}) ? 1 : 0;
	}

	# eqri
	if ($opCodes->{"eqri"} eq $op) {
		$registers->{$instruction->{c}} = ($registers->{$instruction->{a}} == $instruction->{b}) ? 1 : 0;
	}

	# eqrr
	if ($opCodes->{"eqrr"} eq $op) {
		$registers->{$instruction->{c}} = ($registers->{$instruction->{a}} == $registers->{$instruction->{b}}) ? 1 : 0;
	}
}

print Dumper $registers;
