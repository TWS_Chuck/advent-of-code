use strict;
use warnings;

use CL;

use Data::Dumper;
use Storable qw(dclone);

my $depth = 3066;
my $targetX = 13;
my $targetY = 726;

my $endX = $targetX + 100;
my $endY = $targetY + 100;

my $grid = {};
$grid->{0}{0} = {
	geo => 0,
	ero => ($depth) % 20183,
};

$grid->{0}{0}{mod} = $grid->{0}{0}{ero} % 3;

for (my $x = 1; $x <= $endX; $x++) {
	$grid->{0}{$x}{geo} = 16807 * $x;
	$grid->{0}{$x}{ero} = ($grid->{0}{$x}{geo} + $depth) % 20183;
	$grid->{0}{$x}{mod} = $grid->{0}{$x}{ero} % 3;
}

for (my $y = 1; $y <= $endY; $y++) {
	$grid->{$y}{0}{geo} = 48271 * $y;
	$grid->{$y}{0}{ero} = ($grid->{$y}{0}{geo} + $depth) % 20183;
	$grid->{$y}{0}{mod} = $grid->{$y}{0}{ero} % 3;
}

for (my $y = 1; $y <= $endY; $y++) {
	for (my $x = 1; $x <= $endX; $x++) {
		next if defined $grid->{$y}{$x};

		if ($y == $targetY && $x == $targetX) {
			$grid->{$y}{$x}{geo} = 0;
			$grid->{$y}{$x}{ero} = ($grid->{$y}{$x}{geo} + $depth) % 20183;
			$grid->{$y}{$x}{mod} = $grid->{$y}{$x}{ero} % 3;
		} else {
			$grid->{$y}{$x}{geo} = $grid->{$y-1}{$x}{ero} * $grid->{$y}{$x-1}{ero};
			$grid->{$y}{$x}{ero} = ($grid->{$y}{$x}{geo} + $depth) % 20183;
			$grid->{$y}{$x}{mod} = $grid->{$y}{$x}{ero} % 3;
		}
	}
}

calculateMinutes();

# 0 Torch, 1 Climb, 2 Neither
sub calculateMinutes {
	my $startX = 0;
	my $startY = 0;

	my $searched = {
		$startY => {
			$startX => {
				0 => 0,
				1 => 7,
				2 => 999999,
			},
		},
	};

	my $queue = {
		$startY + 1 => {
			$startX => 1,
		},
		$startY => {
			$startX + 1 => 1,
		},
	};

	while (1) {
		my $nextQueue = {};

		foreach my $y (sort { $a <=> $b } (keys $queue)) {
			foreach my $x (sort { $a <=> $b } (keys $queue->{$y})) {
				my $oldTorch = -1;
				my $oldClimb = -1;
				my $oldNeither = -1;

				if (defined $searched->{$y} && defined $searched->{$y}{$x}) {
					$oldTorch = $searched->{$y}{$x}{0};
					$oldClimb = $searched->{$y}{$x}{1};
					$oldNeither = $searched->{$y}{$x}{2};
				}

				my $minTorch = 999999;
				my $minClimb = 999999;
				my $minNeither = 999999;
				foreach my $offset (@CL::ManhattanNeighbours) {
					my $xO = $offset->{x};
					my $yO = $offset->{y};

					next if ($x + $xO < 0) || ($x + $xO > $endX) || ($y + $yO < 0) || ($y + $yO > $endY);
					next if (not (defined $searched->{$y+$yO} && defined $searched->{$y+$yO}{$x+$xO}));

					$minTorch = $searched->{$y+$yO}{$x+$xO}{0} if $searched->{$y+$yO}{$x+$xO}{0} < $minTorch;
					$minClimb = $searched->{$y+$yO}{$x+$xO}{1} if $searched->{$y+$yO}{$x+$xO}{1} < $minClimb;
					$minNeither = $searched->{$y+$yO}{$x+$xO}{2} if $searched->{$y+$yO}{$x+$xO}{2} < $minNeither;
				}

				my $terrain = $grid->{$y}{$x}{mod};
				my $torch = $minTorch + 1;
				my $climb = $minClimb + 1;
				my $neither = $minNeither + 1;

				if ($terrain == 0) {
					$torch = ($torch < $climb + 7) ? $torch : $climb + 7;
					$climb = ($climb < $torch + 7) ? $climb : $torch + 7;
					$neither = 999999;
				} elsif ($terrain == 1) {
					$torch = 999999;
					$climb = ($climb < $neither + 7) ? $climb : $neither + 7;
					$neither = ($neither < $climb + 7) ? $neither : $climb + 7;
				} elsif ($terrain == 2) {
					$torch = ($torch < $neither + 7) ? $torch : $neither + 7;
					$climb = 999999;
					$neither = ($neither < $torch + 7) ? $neither : $torch + 7;
				}

				# Either new point (oldTorch is still -1) or this is a previous point, and we found a cheaper path here
				if ($oldTorch == -1 || ( $oldTorch != -1 && ($oldTorch > $torch || $oldClimb > $climb || $oldNeither > $neither) ) ) {
					#print "Updating!\n";
					$searched->{$y}{$x} = {
						0 => $torch,
						1 => $climb,
						2 => $neither,
					};
					# We had to update, check neighbours
					foreach my $offset (@CL::ManhattanNeighbours) {
						my $xO = $offset->{x};
						my $yO = $offset->{y};
						next if ($x + $xO < 0) || ($x + $xO > $endX) || ($y + $yO < 0) || ($y + $yO > $endY);
						$nextQueue->{$y+$yO}{$x+$xO} = 1;
					}
				}
			}
		}

		if (defined $searched->{$targetY} && defined $searched->{$targetY}{$targetX}) {
			print Dumper $searched->{$targetY}{$targetX}->{0};
		}

		$queue = $nextQueue;
	}
}