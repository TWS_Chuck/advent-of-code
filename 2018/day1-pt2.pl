use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day1.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

my $previousResults = {};

while (1) {
	for (my $i = 0; $i < scalar @lines; $i++) {
		my $line = $lines[$i];
		$line =~ /([+-])(\d+)/;
		if ($1 eq "-") {
			$result -= int($2);
		} else {
			$result += int($2);
		}

		#print("This time result $result\n");
		if (defined $previousResults->{$result}) {
			print("We found it! $result\n");
			exit;
		} else {
			$previousResults->{$result} = 1;
		}
	}
}