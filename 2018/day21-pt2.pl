use strict;
use warnings;

use CL;

use Data::Dumper;

my $result = 0;

my $results = {};

my $fifth = 1024276;

my $previous = 0;

while (1) {
	my $third = $fifth | 65536;
	$fifth = 521363;
	while ($third > 0) {
		$fifth = ((((($third & 255) + $fifth) & 16777215) * 65899) & 16777215);
		$third = int($third / 256);
	}
	if (defined $results->{$fifth}) {
		last;
	}
	$results->{$fifth} = 1;
	$previous = $fifth;
}

print("$previous\n");
