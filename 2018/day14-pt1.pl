use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day14.txt";

my $input = 990941;

my $result = 0;

my $recipes = {
	0 => 3,
	1 => 7,
};

my $numRecipes = 2;

my $firstElf = 0;
my $secondElf = 1;

while ($numRecipes < 10 + $input) {
	my $sum = $recipes->{$firstElf} + $recipes->{$secondElf};
	my $tens = int($sum / 10);
	my $ones = $sum % 10;

	if ($tens != 0) {
		$recipes->{$numRecipes} = $tens;
		$numRecipes++;
	}
	$recipes->{$numRecipes} = $ones;
	$numRecipes++;

	$firstElf = ($firstElf + 1 + $recipes->{$firstElf}) % $numRecipes;
	$secondElf = ($secondElf + 1 + $recipes->{$secondElf}) % $numRecipes;
}

for (my $i = $input; $i < $input + 10; $i++) {
	print $recipes->{$i};
}
print "\n";