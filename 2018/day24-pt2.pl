use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day24.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

my $groups = {};

my $groupID = 0;

my $immuneMode = 0;
my $infectionMode = 0;

my $boost = 38;

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];

	if ($line =~ /Immune System/) {
		$immuneMode = 1;
		$infectionMode = 0;
		next;
	} elsif ($line =~ /Infection/) {
		$immuneMode = 0;
		$infectionMode = 1;
		next;
	}

	$line =~ /(\d+) units each with (\d+) hit points/;
	my $numUnits = $1;
	my $hitPointsPerUnit = $2;

	$line =~ /(\d+) (\w+) damage at initiative (\d+)/;
	my $damage = int($1);
	my $damageType = $2;
	my $initiative = $3;

	$damage += $boost if ($immuneMode == 1);

	$groups->{$groupID} = {
		team => $infectionMode, # 0 when immune, 1 when infection
		num => $numUnits,
		hp => $hitPointsPerUnit,
		damage => $damage,
		damageType => $damageType,
		initiative => $initiative,
	};

	if ($line =~ /immune to (\w+), (\w+)/) {
		$groups->{$groupID}{immun}{$1} = 1;
		$groups->{$groupID}{immun}{$2} = 1;
	} elsif ($line =~ /immune to (\w+)[);]/) {
		$groups->{$groupID}{immun}{$1} = 1;
	}

	if ($line =~ /weak to (\w+), (\w+)/) {
		$groups->{$groupID}{weak}{$1} = 1;
		$groups->{$groupID}{weak}{$2} = 1;
	} elsif ($line =~ /weak to (\w+)[);]/) {
		$groups->{$groupID}{weak}{$1} = 1;
	}

	$groupID++;
}

while (1) {
	# Get list of groups, and in what order they act
	my $groupsByEffectivePower = {};
	foreach my $groupID (keys $groups) {
		my $effectivePower = $groups->{$groupID}{damage} * $groups->{$groupID}{num};
		$groupsByEffectivePower->{$effectivePower} = $groupID;
	}

	my $targets = {};
	my $targeted = {};
	foreach my $ep (sort { $b <=> $a } (keys $groupsByEffectivePower)) {
		# print "The first group has EP $ep. Group: $groupsByEffectivePower->{$ep}\n";
		my $groupID = $groupsByEffectivePower->{$ep};
		my $currentGroup = $groups->{$groupID};
		my $currentTeam = $currentGroup->{team};

		my $targetedGroup = -1;
		my $mostDamagePossible = 0;
		my $targetedGroupEP = 0;
		my $targetedGroupInitiative = -1;
		foreach my $otherGroupID (keys $groups) {
			next if $targeted->{$otherGroupID}; # Skip if this group already targeted
			my $otherGroup = $groups->{$otherGroupID};
			next if $otherGroup->{team} == $currentTeam;

			my $damageGroupCouldDeal = $ep;
			if (defined $otherGroup->{immun} && defined $otherGroup->{immun}{$currentGroup->{damageType}}) {
				$damageGroupCouldDeal = 0;
			} elsif (defined $otherGroup->{weak} && defined $otherGroup->{weak}{$currentGroup->{damageType}}) {
				$damageGroupCouldDeal *= 2;
			}

			my $otherGroupEP = $groups->{$otherGroupID}{damage} * $groups->{$otherGroupID}{num};

			# print "damage is greater\n" if ($damageGroupCouldDeal > $mostDamagePossible);
			# print "damage is same, but ep is higher\n" if ($damageGroupCouldDeal == $mostDamagePossible && $targetedGroupEP < $otherGroupEP);
			# print "damage and ep are same, but init is higher\n" if ($damageGroupCouldDeal == $mostDamagePossible && $targetedGroupEP == $otherGroupEP && $targetedGroupInitiative < $groups->{$otherGroupID}{initiative});

			if ($damageGroupCouldDeal > $mostDamagePossible ||
			($damageGroupCouldDeal == $mostDamagePossible && $targetedGroupEP < $otherGroupEP) ||
			($damageGroupCouldDeal == $mostDamagePossible && $targetedGroupEP == $otherGroupEP && $targetedGroupInitiative < $groups->{$otherGroupID}{initiative})) {
				$targetedGroup = $otherGroupID;
				$mostDamagePossible = $damageGroupCouldDeal;
				$targetedGroupEP = $otherGroupEP;
				$targetedGroupInitiative = $groups->{$otherGroupID}{initiative};
			}
		}

		if ($mostDamagePossible > 0) {
			$targets->{$groupID} = $targetedGroup;
			$targeted->{$targetedGroup} = 1;
			# print Dumper $targets->{$groupID};
		} else {
			# print "$groupID has no valid targets\n";
		}
	}

	if (scalar (keys $targets) == 0) {
		print "No more fight!\n";
		last;
	}

	# Get order of attackers
	my $groupsByInitiative = {};
	foreach my $groupID (keys $groups) {
		$groupsByInitiative->{$groups->{$groupID}{initiative}} = $groupID;
	}

	foreach my $initiative (sort { $b <=> $a } (keys $groupsByInitiative)) {
		my $attackingGroupID = $groupsByInitiative->{$initiative};
		my $currentAttackingGroup = $groups->{$attackingGroupID};

		next if (not defined $targets->{$attackingGroupID}); # This group not attacking anyone

		my $defenderGroupID = $targets->{$attackingGroupID};
		my $currentDefendingGroup = $groups->{$defenderGroupID};

		next if $currentAttackingGroup->{num} < 0; # The attacking group was already killed
		next if $currentDefendingGroup->{num} < 0; # The defending group was already killed
		
		my $attackDamageType = $currentAttackingGroup->{damageType};

		my $effectivePowerAgainstDefenders = $currentAttackingGroup->{damage} * $currentAttackingGroup->{num};
		if (defined $currentDefendingGroup->{immun} && $currentDefendingGroup->{immun}{$attackDamageType}) {
			$effectivePowerAgainstDefenders = 0;
		} elsif (defined $currentDefendingGroup->{weak} && $currentDefendingGroup->{weak}{$attackDamageType}) {
			$effectivePowerAgainstDefenders *= 2;
		}

		# Handle damage
		my $numUnitsDestroyed = int($effectivePowerAgainstDefenders / $currentDefendingGroup->{hp});
		$currentDefendingGroup->{num} -= $numUnitsDestroyed;
		# print "$attackingGroupID attacked $defenderGroupID, dealing $effectivePowerAgainstDefenders and killing $numUnitsDestroyed\n";
	}

	# print Dumper $groups;

	# Remove dead groups
	foreach my $groupID (keys $groups) {
		if ($groups->{$groupID}{num} <= 0) {
			# print "Group $groupID died\n";
			delete $groups->{$groupID};
		}
	}
	# print Dumper $groups;
	# print "\n";
}

print Dumper $groups;

foreach my $groupID (keys $groups) {
	$result += $groups->{$groupID}{num};
}

print("$result\n");
