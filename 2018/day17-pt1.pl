use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day17.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

my $grid = {};

my $minX = 999999;
my $maxX = 0;
my $minY = 999999;
my $maxY = 0;

# Fill grid
for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];

	if ($line =~ /y=(\d+), x=(\d+)..(\d+)/) {
		for (my $x = int($2); $x <= int($3); $x++) {
			$grid->{int($1)}{$x} = '#';
		}
		$minX = int($2) if int($2) < $minX;
		$maxX = int($3) if int($3) > $maxX;
		$minY = int($1) if int($1) < $minY;
		$maxY = int($1) if int($1) > $maxY;
	} elsif ($line =~ /x=(\d+), y=(\d+)..(\d+)/) {
		for (my $y = int($2); $y <= int($3); $y++) {
			$grid->{$y}{int($1)} = '#';
		}
		$minX = int($1) if int($1) < $minX;
		$maxX = int($1) if int($1) > $maxX;
		$minY = int($2) if int($2) < $minY;
		$maxY = int($3) if int($3) > $maxY;
	}
}

# Fill rest of grid with emptiness
for (my $y = $minY; $y <= $maxY; $y++) {
	for (my $x = $minX - 1; $x <= $maxX + 1; $x++) {
		$grid->{$y}{$x} = '.' if not defined $grid->{$y}{$x};
	}
}

my $startingX = 500;
my $startingY = 0;

fillBelowFromSource($startingX, $startingY, $minY, $maxY);

$result = findAllWater();

print("$result\n");

sub fillBelowFromSource {
	my $sourceX = shift;
	my $sourceY = shift;

	my $x = $sourceX;
	my $y = $sourceY + 1;

	printGrid($x,$y);

	# Descend until we hit the bottom or something fillable
	while ($y <= $maxY) {
		if (not (defined $grid->{$y}) && not (defined $grid->{$y}{$x})) {
			$grid->{$y}{$x} = '.';
		}
		if ($grid->{$y}{$x} eq '#' || $grid->{$y}{$x} eq '~') {
			# Found our bottom
			$y--;
			last;
		}
		if ($grid->{$y}{$x} eq '|') {
			# Feeding into something that's already wet af
			return;
		}
		$grid->{$y}{$x} = '|';

		$y++;
	}
	if ($y > $maxY) {
		$y--;
	}

	printGrid($x, $y);

	# We have found something, if bottom of world return...
	return if $y >= $maxY;

	my $leftWallFound = 1;
	my $rightWallFound = 1;
	while ($leftWallFound && $rightWallFound) {
		# Perhaps the next row would be ground...
		if ($y >= $maxY || $y == $sourceY) {
			return;
		}

		# Go left
		my $leftOffset = 1;
		while ($grid->{$y}{$x-$leftOffset} eq '.') {
			$grid->{$y}{$x-$leftOffset} = '|';

			# If we pour off this platform...
			if ($grid->{$y+1}{$x-$leftOffset} eq '.') {
				fillBelowFromSource($x-$leftOffset, $y, $maxY);
				if (not $grid->{$y+1}{$x-$leftOffset} eq '~') {
					$leftWallFound = 0;
					last;
				}
			}

			$leftOffset++;
			if (not defined $grid->{$y}{$x-$leftOffset}) {
				exit;
			}
		}

		printGrid($x, $y);

		# Go right
		my $rightOffset = 1;
		my $rightWall = 1;
		while ($grid->{$y}{$x+$rightOffset} eq '.') {
			$grid->{$y}{$x+$rightOffset} = '|';

			# If we pour off this platform...
			if ($grid->{$y+1}{$x+$rightOffset} eq '.') {
				fillBelowFromSource($x+$rightOffset, $y, $maxY);
				if (not $grid->{$y+1}{$x+$rightOffset} eq '~') {
					$rightWallFound = 0;
					last;
				}
			}

			$rightOffset++;
			if (not defined $grid->{$y}{$x+$rightOffset}) {
				exit;
			}
		}
		printGrid($x, $y);

		if ($leftWallFound && $rightWallFound) {
			for (my $i = $x - ($leftOffset - 1); $i < $x + $rightOffset; $i++) {
				$grid->{$y}{$i} = '~';
			}
			$y = $y - 1;
		}
	}

	return;
}

sub printGrid {
	my $givenX = shift;
	my $givenY = shift;

	return 1;

	my $leftX = $minX;
	my $rightX = $maxX;
	my $upY = $minY;
	my $botY = $maxY;
	if (defined $givenX && defined $givenY) {
		$leftX = $givenX - 40;
		$rightX = $givenX + 40;
		$upY = $givenY - 20;
		$botY = $givenY + 20;
	}

	$givenX = -1000;
	$givenY = -1000;

	print "\n";
	for (my $y = $upY; $y <= $botY; $y++) {
		for (my $x = $leftX; $x <= $rightX; $x++) {
			$grid->{$y}{$x} = '.' if not defined $grid->{$y}{$x};
			print $grid->{$y}{$x} if ($x != $givenX || $y != $givenY);
			print 'X' if ($x == $givenX && $y == $givenY);
		}
		print "\n";
	}
	print "\n ------- \n";
}

sub findAllWater {
	my $water = 0;
	for (my $y = $minY; $y <= $maxY; $y++) {
		for (my $x = $minX - 1; $x <= $maxX + 1; $x++) {
			$water++ if ($grid->{$y}{$x} eq '~' || $grid->{$y}{$x} eq '|');
		}
	}
	return $water;
}