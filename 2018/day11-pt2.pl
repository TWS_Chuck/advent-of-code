use strict;
use warnings;

use CL;

use Data::Dumper;

my $input = 7139;

my $grid = {};

for (my $i = 0; $i < 300; $i++) {
	for (my $j = 0; $j < 300; $j++) {
		my $rackID = $i + 10;
		$grid->{$i}{$j} = $rackID * $j;
		$grid->{$i}{$j} += $input;
		$grid->{$i}{$j} *= $rackID;
		$grid->{$i}{$j} = (($grid->{$i}{$j} / 100) % 10);
		$grid->{$i}{$j} -= 5;
	}
}

my $maxPowerLevel = -99999;
my $xCoord = -1;
my $yCoord = -1;
my $largestSize = -1;

for (my $size = 300; $size <= 300; $size++) {
	my ($x, $y, $result) = CL::getLargestSectionOfMultiArray($size, $size, $grid);

	if ($result > $maxPowerLevel) {
		$maxPowerLevel = $result;
		$xCoord = $x - $size + 1;
		$yCoord = $y - $size + 1;
		$largestSize = $size;
	}

	print "Currently on $size\nLargest so far is $maxPowerLevel: $xCoord,$yCoord,$largestSize\n";
}

print("$xCoord,$yCoord,$largestSize\n");
