use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day5.txt";
my @line = CL::importSingleLineFromFile('', $filename);
my $shortestLength = 999999999;

for (my $j = 65; $j <= 90; $j++) {
	my $reducedLine = "";
	for (my $i = 0; $i < scalar @line; $i++) {
		my $char = $line[$i];
		unless ($char eq chr($j) || $char eq chr($j + 32)) {
			$reducedLine .= $char;
		}
	}

	my $reactions = 0;
	my $continue = 1;
	my @newLine = split //, $reducedLine;
	my $nextLine = "";
	while ($continue) {
		$reactions = 0;
		for (my $i = 0; $i < scalar @newLine; $i++) {
			my $char = $newLine[$i];
			my $ord = ord($char);
			unless ($i + 1 < scalar @newLine && ($newLine[$i + 1] eq chr($ord + 32) || $newLine[$i + 1] eq chr($ord - 32))) {
				$nextLine .= $char;
			} else {
				$reactions = 1;
				$i++
			}
		}
		unless ($reactions) {
			$continue = 0;
		} else {
			@newLine = split //, $nextLine;
			$nextLine = "";
		}
	}

	my $length = split //, $nextLine;
	if ($length < $shortestLength) {
		$shortestLength = $length;
	}
}
print "$shortestLength\n";