use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day5.txt";

my @line = CL::importSingleLineFromFile('', $filename);

my $result = 0;

my $nextLine = "";

my $reactions = 0;

while (1) {
	$reactions = 0;
	for (my $i = 0; $i < scalar @line; $i++) {
		my $char = $line[$i];
		my $ord = ord($char);
		unless ($i + 1 < scalar @line && ($line[$i + 1] eq chr($ord + 32) || $line[$i + 1] eq chr($ord - 32))) {
			$nextLine .= $char;
		} else {
			$reactions = 1;
			$i++
		}
	}

	unless ($reactions) {
		print "We have it!\n".scalar (split //, $nextLine)."\n";
		exit;
	} else {
		@line = split //, $nextLine;
		$nextLine = "";
	}
}
