use strict;
use warnings;

use CL;

use Data::Dumper;

my $depth = 3066;
my $targetX = 13;
my $targetY = 726;

my $grid = {};
$grid->{0}{0} = {
	geo => 0,
	ero => ($depth) % 20183,
};

$grid->{0}{0}{mod} = $grid->{0}{0}{ero} % 3;

my $result = $grid->{0}{0}{mod};

for (my $x = 1; $x <= $targetX; $x++) {
	$grid->{0}{$x}{geo} = 16807 * $x;
	$grid->{0}{$x}{ero} = ($grid->{0}{$x}{geo} + $depth) % 20183;
	$grid->{0}{$x}{mod} = $grid->{0}{$x}{ero} % 3;
	$result += $grid->{0}{$x}{mod};
}

for (my $y = 1; $y <= $targetY; $y++) {
	$grid->{$y}{0}{geo} = 48271 * $y;
	$grid->{$y}{0}{ero} = ($grid->{$y}{0}{geo} + $depth) % 20183;
	$grid->{$y}{0}{mod} = $grid->{$y}{0}{ero} % 3;
	$result += $grid->{$y}{0}{mod};
}

for (my $y = 1; $y <= $targetY; $y++) {
	for (my $x = 1; $x <= $targetX; $x++) {
		next if defined $grid->{$y}{$x};

		if ($y == $targetY && $x == $targetX) {
			$grid->{$y}{$x}{geo} = 0;
			$grid->{$y}{$x}{ero} = ($grid->{$y}{$x}{geo} + $depth) % 20183;
			$grid->{$y}{$x}{mod} = $grid->{$y}{$x}{ero} % 3;
			$result += $grid->{$y}{$x}{mod};
		} else {
			$grid->{$y}{$x}{geo} = $grid->{$y-1}{$x}{ero} * $grid->{$y}{$x-1}{ero};
			$grid->{$y}{$x}{ero} = ($grid->{$y}{$x}{geo} + $depth) % 20183;
			$grid->{$y}{$x}{mod} = $grid->{$y}{$x}{ero} % 3;
			$result += $grid->{$y}{$x}{mod};
		}
	}
}

print("$result\n");
