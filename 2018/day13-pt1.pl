use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day13.txt";

my @multiArray = CL::importMultiArrayFromFile($filename);

my $result = 0;

my $agentsAndDirections = {
	'^' => 0,
	'>' => 1,
	'v' => 2,
	'<' => 3,
};

my $agentDefaults = {
	'inter' => -1,
};

my $characterDirectionMapping = {
	'/' => {
		0 => sub {
			my $cart = shift;
			$cart->{dir} = 1;
		},
		1 => sub {
			my $cart = shift;
			$cart->{dir} = 0;
		},
		2 => sub {
			my $cart = shift;
			$cart->{dir} = 3;
		},
		3 => sub {
			my $cart = shift;
			$cart->{dir} = 2;
		},
	},
	'G' => {
		0 => sub {
			my $cart = shift;
			$cart->{dir} = 3;
		},
		1 => sub {
			my $cart = shift;
			$cart->{dir} = 2;
		},
		2 => sub {
			my $cart = shift;
			$cart->{dir} = 1;
		},
		3 => sub {
			my $cart = shift;
			$cart->{dir} = 0;
		},
	},
	'+' => {
		default => sub {
			my $cart = shift;

			$cart->{dir} += $cart->{inter};
			if ($cart->{dir} == -1) {
				$cart->{dir} = 3;
			} elsif ($cart->{dir} == 4) {
				$cart->{dir} = 0;
			}

			$cart->{inter}++;
			if ($cart->{inter} == 2) {
				$cart->{inter} = -1;
			}
		},
	},
};

my ($carts, $currentLocations) = CL::GetAgentsAndLocations(\@multiArray, $agentsAndDirections, $agentDefaults);

my $currentIteration = 0;
while (1) {
	my $largestRow = 0;
	my $nextLocations = {};
	foreach my $row (sort { $a <=> $b } (keys $currentLocations)) {
		my $largestCol = 0;
		foreach my $column (sort { $a <=> $b } (keys $currentLocations->{$row})) {
			my $cart = $currentLocations->{$row}{$column};

			delete $currentLocations->{$row}{$column};
			if (scalar keys $currentLocations->{$row} == 0) {
				delete $currentLocations->{$row};
			}

			my ($x, $y) = CL::MoveAgentAndUpdateDirection(\@multiArray, $carts->{$cart}, $characterDirectionMapping);

			if ((defined $currentLocations->{$y} && defined $currentLocations->{$y}{$x}) || (defined $nextLocations->{$y} && defined $nextLocations->{$y}{$x})) {
				# CRASH
				my $otherCar = $currentLocations->{$y}{$x};
				print ("Crash happened after $currentIteration ticks at $x,$y with carts $cart and $otherCar\n");
				exit;
			} else {
				$nextLocations->{$y}{$x} = $cart;
			}

			if (not defined $multiArray[$y][$x]) {
				print "UH OH\n$cart @ $x, $y\n";
				print Dumper $carts->{$cart};
				exit;
			}
		}
	}
	$currentLocations = $nextLocations;
	$currentIteration++;
}
