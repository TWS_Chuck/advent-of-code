use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day10.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

my $points = {};

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
	$line =~ /position=<([ -]?)(\d+), ([ -]?)(\d+)> velocity=<([ -]?)(\d+), ([ -]?)(\d+)>/;
	my ($posX, $posY, $velX, $velY) = (int($2), int($4), int($6), int($8));
	$posX *= -1 if $1 eq "-";
	$posY *= -1 if $3 eq "-";
	$velX *= -1 if $5 eq "-";
	$velY *= -1 if $7 eq "-";

	#print "$posX, $posY, $velX, $velY\n";
	$points->{$i} = {
		pos => {
			x => $posX,
			y => $posY,
		},
		vel => {
			x => $velX,
			y => $velY,
		},
	};
}

my $seconds = 0;
while (1) {
	$seconds++;
	my $graph = {};
	my $minX = 99999999;
	my $minY = 99999999;
	my $maxX = -99999999;
	my $maxY = -99999999;

	foreach my $point (keys $points) {
		$points->{$point}{pos}{x} = $points->{$point}{pos}{x} + $points->{$point}{vel}{x};
		$points->{$point}{pos}{y} = $points->{$point}{pos}{y} + $points->{$point}{vel}{y};

		$minX = $points->{$point}{pos}{x} if $points->{$point}{pos}{x} < $minX;
		$minY = $points->{$point}{pos}{y} if $points->{$point}{pos}{y} < $minY;
		$maxX = $points->{$point}{pos}{x} if $points->{$point}{pos}{x} > $maxX;
		$maxY = $points->{$point}{pos}{y} if $points->{$point}{pos}{y} > $maxY;

		$graph->{$points->{$point}{pos}{x}}{$points->{$point}{pos}{y}} = '#';
	}

	if ( $maxY - $minY > 12 ) {
		next;
	}

	print "$seconds\n";

	for (my $j = $minY; $j <= $maxY; $j++) {
		my $line = "";
		for (my $i = $minX; $i <= $maxX; $i++) {
			my $char = (defined $graph->{$i}{$j} ? $graph->{$i}{$j} : '.');
			$line .= "$char ";
		}
		print "$line\n";
	}
}

print("$result\n");
