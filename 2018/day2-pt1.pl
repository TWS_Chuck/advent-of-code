use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day2.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

my $twoCount = 0;
my $threeCount = 0;

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
	my @characters = split //, $line;
	my $charactersSoFar = {};
	foreach my $char (@characters) {
		$charactersSoFar->{$char}++;
	}

	my $twoCounted = 0;
	my $threeCounted = 0;
	foreach my $char (@characters) {
		if ($charactersSoFar->{$char} == 2 && $twoCounted == 0) {
			$twoCounted = 1;
			$twoCount++;
		} elsif ($charactersSoFar->{$char} == 3 && $threeCounted == 0) {
			$threeCounted = 1;
			$threeCount++;
		}
	}
}

$result = $twoCount * $threeCount;

print("$result\n");
