use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day8.txt";

my @nums = CL::importSingleLineFromFile(' ', $filename);

my $processResult = process(0);
my $result = $processResult->{result};

print("$result\n");

sub process {
	my $start = shift;
	my $result = 0;

	my $nodes = $nums[$start];
	my $metadata = $nums[$start+1];

	my $nextIndex = $start+2;

	if ($nodes == 0) {
		for (my $i = 0; $i < $metadata; $i++) {
			$result += $nums[$nextIndex+$i];
		}

		return { result => $result, nextIndex => $nextIndex + $metadata };
	}

	my $values = {};

	for (my $i = 0; $i < $nodes; $i++) {
		my $processResult = process($nextIndex);
		$nextIndex = $processResult->{nextIndex};
		$values->{$i+1} = $processResult->{result};
	}

	for (my $i = 0; $i < $metadata; $i++) {
		my $childIndex = $nums[$nextIndex+$i];
		$result += $values->{$childIndex} if (defined $values->{$childIndex});
	}

	return { result => $result, nextIndex => $nextIndex + $metadata };
}