use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day6.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

my $points = {};
my $grid = {};

my $smallestX = 99999;
my $smallestY = 99999;
my $largestX = 0;
my $largestY = 0;

my $currentPoint = 0;
for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
	$line =~ /(\d+), (\d+)/;
	my ($x, $y) = (int($1), int($2));

	$largestX = $x if $x > $largestX;
	$largestY = $y if $y > $largestY;
	$smallestX = $x if $x < $smallestX;
	$smallestY = $y if $y < $smallestY;

	$points->{$currentPoint}{'x'} = $x;
	$points->{$currentPoint}{'y'} = $y;
	$grid->{$x}{$y}{'point'} = $currentPoint;
	$grid->{$x}{$y}{'dist'} = 0;

	$currentPoint++;
}

#print "Calculating distances...\n";
for (my $x = $smallestX; $x <= $largestX; $x++) {
	for (my $y = $smallestY; $y <= $largestY; $y++) {
		if (defined $grid->{$x}{$y} and $grid->{$x}{$y}{'dist'} == 0) {
			next;
		}
		#print "Current x and y: $x $y\n";
		foreach my $point (keys $points) {
			#print "Trying point $point\n";
			my $distance = abs($x - $points->{$point}{'x'}) + abs($y - $points->{$point}{'y'});
			if (not defined $grid->{$x}{$y} or $grid->{$x}{$y}{'dist'} >= $distance) {
				#print "Writing new distance!\n";
				if (defined $grid->{$x}{$y} and $grid->{$x}{$y}{'dist'} == $distance) {
					$grid->{$x}{$y}{'point'} = ".";
				} else {
					$grid->{$x}{$y}{'point'} = $point;
				}
				$grid->{$x}{$y}{'dist'} = $distance;
			}
		}
	}
}
#print Dumper $grid;

#print "Removing infinte areas from contention\n";
for (my $y = $smallestY; $y < $largestY; $y++) {
	my $point = $grid->{$smallestX}{$y}{'point'};
	delete $points->{$point};
	$point = $grid->{$largestX}{$y}{'point'};
	delete $points->{$point};
}

for (my $x = $smallestX; $x < $largestX; $x++) {
	my $point = $grid->{$x}{$smallestY}{'point'};
	delete $points->{$point};
	$point = $grid->{$x}{$largestY}{'point'};
	delete $points->{$point};
}

#print "Calculating areas...\n";
my $counts = {};
for (my $x = $smallestX; $x <= $largestX; $x++) {
	for (my $y = $smallestY; $y <= $largestY; $y++) {
		my $point = $grid->{$x}{$y}{'point'};
		$counts->{$point}++;
	}
}

#print Dumper $counts;

my $largestArea = 0;
foreach my $possiblePoint (keys $points) {
	$largestArea = $counts->{$possiblePoint} if $largestArea < $counts->{$possiblePoint};
}

print("$largestArea\n");
