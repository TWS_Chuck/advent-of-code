use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day12.txt";

my @lines = CL::importLinesFromFile($filename);

my @plants = split //, "..........#...#..###.#.###.####.####.#..#.##..#..##..#.....#.#.#.##.#...###.#..##..#.##..###..#..##.#..##..........................";
my $offset = 10;

my $result = 0;

for (my $i = 0; $i < scalar @plants; $i++) {
	print $plants[$i];
}
print " - 0\n";

for (my $gen = 1; $gen <= 20; $gen++) {
	my @nextPlants = ('.','.');
	for (my $i = 2; $i < scalar @plants - 2; $i++) {
		my $replaced = 0;
		$nextPlants[$i] = $plants[$i];
		foreach my $line (@lines) {
			$line =~ /([.#]{5}) => ([.#])/;
			my @plantsToSearch = split //, $1;
			my $replace = $2;

			if ($plantsToSearch[2] eq $plants[$i]) {
				my $leftmost = ($plants[$i-2] eq $plantsToSearch[0]) ? 1 : 0;
				my $cLeft = ($plants[$i-1] eq $plantsToSearch[1]) ? 1 : 0;
				my $cRight = ($plants[$i+1] eq $plantsToSearch[3]) ? 1 : 0;
				my $rightmost = ($plants[$i+2] eq $plantsToSearch[4]) ? 1 : 0;
				if ($leftmost && $cLeft && $cRight && $rightmost) {
					$nextPlants[$i] = $replace;
					$replaced = 1;
				}
			}
			last if $replaced;
		}
		if (not $replaced) {
			$nextPlants[$i] = '.';
		}
	}
	push @nextPlants, '.','.';
	@plants = @nextPlants;

	for (my $i = 0; $i < scalar @plants; $i++) {
		print $plants[$i];
	}
	print " - $gen\n";
}

for (my $i = 0; $i < scalar @plants; $i++) {
	if ($plants[$i] eq "#") {
		$result += $i - $offset;
		print "$i ";
	}
}
print "\n";

print("$result\n");
