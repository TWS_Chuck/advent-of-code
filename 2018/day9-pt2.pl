use strict;
use warnings;

use CL;

use Data::Dumper;

my $players = 430;
my $lastMarble = 7158800;

my $marbleCircle = {};
$marbleCircle->{0}{next} = 0;
$marbleCircle->{0}{prev} = 0;

my $currentMarble = 0;

my $results = {};
my $currentElf = 0;

for (my $i = 1; $i <= $lastMarble; $i++) {
	if ($i % 23 == 0) {
		$results->{$currentElf} += $i;

		my $prevMarble = $currentMarble;
		for (my $j = 0; $j < 7; $j++) {
			$prevMarble = $marbleCircle->{$prevMarble}{prev};
		}

		$results->{$currentElf} += $prevMarble;

		my $nextMarble = $marbleCircle->{$prevMarble}{next};
		my $prevPrevMarble = $marbleCircle->{$prevMarble}{prev};

		$marbleCircle->{$nextMarble}{prev} = $prevPrevMarble;
		$marbleCircle->{$prevPrevMarble}{next} = $nextMarble;

		$currentMarble = $nextMarble;
	} else {
		# something else
		my $nextMarble = $marbleCircle->{$currentMarble}{next};
		my $nextNextMarble = $marbleCircle->{$nextMarble}{next};

		$marbleCircle->{$i}{prev} = $nextMarble;
		$marbleCircle->{$i}{next} = $nextNextMarble;

		$marbleCircle->{$nextMarble}{next} = $i;
		$marbleCircle->{$nextNextMarble}{prev} = $i;

		$currentMarble = $i;
	}
	$currentElf = ($currentElf + 1) % $players;
}

my $result = 0;
for (my $i = 0; $i < $players; $i++) {
	$result = $results->{$i} if (defined $results->{$i} && $result < $results->{$i});
}

print("$result\n");
