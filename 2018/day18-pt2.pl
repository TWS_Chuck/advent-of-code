use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day18.txt";

my @multiArray = CL::importMultiArrayFromFile($filename);

my $result = 0;

my $previousResults = {};

my $scooted = 0;

my $minute = 0;
while (1) {
	my @nextGrid;
	my $totalLumberyards = 0;
	my $totalTrees = 0;
	for (my $j = 0; $j < scalar @multiArray; $j++) {
		for (my $i = 0; $i < scalar @{$multiArray[$j]}; $i++) {
			if ($multiArray[$j][$i] eq '.') {
				# Currently open...
				my $treeNeighbours = 0;
				foreach my $offsets (@CL::EightDirectionNeighbours) {
					unless ($j + $offsets->{y} < 0 || $j + $offsets->{y} >= 50 || $i + $offsets->{x} < 0 || $i + $offsets->{x} >= 50) {
						$treeNeighbours++ if ($multiArray[$j+$offsets->{y}][$i+$offsets->{x}] eq '|');
					}
				}
				if ($treeNeighbours >= 3) {
					$nextGrid[$j][$i] = '|';
					$totalTrees++;
				} else {
					$nextGrid[$j][$i] = '.';
				}
			} elsif ($multiArray[$j][$i] eq '|') {
				# Currently trees...
				my $lumberyardNeighbours = 0;
				foreach my $offsets (@CL::EightDirectionNeighbours) {
					unless ($j + $offsets->{y} < 0 || $j + $offsets->{y} >= 50 || $i + $offsets->{x} < 0 || $i + $offsets->{x} >= 50) {
						$lumberyardNeighbours++ if ($multiArray[$j+$offsets->{y}][$i+$offsets->{x}] eq '#');
					}
				}
				if ($lumberyardNeighbours >= 3) {
					$nextGrid[$j][$i] = '#';
					$totalLumberyards++;
				} else {
					$nextGrid[$j][$i] = '|';
					$totalTrees++;
				}
			} elsif ($multiArray[$j][$i] eq '#') {
				# Currently lumberyard...
				my $lumberyardNeighbours = 0;
				my $treeNeighbours = 0;
				foreach my $offsets (@CL::EightDirectionNeighbours) {
					unless ($j + $offsets->{y} < 0 || $j + $offsets->{y} >= 50 || $i + $offsets->{x} < 0 || $i + $offsets->{x} >= 50) {
						$treeNeighbours++ if ($multiArray[$j+$offsets->{y}][$i+$offsets->{x}] eq '|');
						$lumberyardNeighbours++ if ($multiArray[$j+$offsets->{y}][$i+$offsets->{x}] eq '#');
					}
				}
				if ($treeNeighbours >= 1 && $lumberyardNeighbours >= 1) {
					$nextGrid[$j][$i] = '#';
					$totalLumberyards++;
				} else {
					$nextGrid[$j][$i] = '.';
				}
			} else {
				print "WE HAVE A PROBLEM\n";
				exit;
			}
		}
	}
	$minute++;
	@multiArray = @nextGrid;
	$result = $totalLumberyards * $totalTrees;
	if (defined $previousResults->{$result} && $result == 241780 && $scooted == 0) {
		print "$minute - We last saw 241780 before at $previousResults->{$result}\n";
		$minute += int((1000000000 - $minute) / 28) * 28;
		$scooted = 1;
		print "Scooted ahead, now at $minute\n";
	}
	$previousResults->{$result} = $minute;
	# if ($minute % 28 == 0) {
	# 	print "\nAt minute $minute\n";
	# 	for (my $j = 0; $j < scalar @multiArray; $j++) {
	# 		for (my $i = 0; $i < scalar @{$multiArray[$j]}; $i++) {
	# 			print "$multiArray[$j][$i]";
	# 		}
	# 		print "\n";
	# 	}
	# 	print "\n";
	# }

	if ($minute >= 1000000000) {
		print "Stopping at $minute\n";
		$result = $totalLumberyards * $totalTrees;
		last;
	}
}

print("$result\n");
