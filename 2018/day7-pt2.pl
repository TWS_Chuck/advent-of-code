use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day7.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

my $precedence = {};

my $dependencies = {};

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
	$line =~ /Step (\w) must be finished before step (\w) can begin./;
	if (not defined $precedence->{$1}) {
		$precedence->{$1} = [];
	}
	push $precedence->{$1}, $2;
	if (not defined ($dependencies->{$1})) {
		$dependencies->{$1} = 0;
	}
	$dependencies->{$2}++;
}

my $numWorkers = 5;
my $stepsInProgress = {};
my $i = 0;
while (1) {
	if (scalar (keys $precedence) == 0 && scalar (keys $dependencies) == 0 && scalar (keys $stepsInProgress) == 0) {
		print "Done! $result\n";
		print Dumper $dependencies;
		print Dumper $precedence;
		print Dumper $stepsInProgress;
		exit;
	}

	my @possibleSteps;
	foreach my $step (keys $dependencies) {
		if ($dependencies->{$step} == 0) {
			push @possibleSteps, $step;
		}
	}

	print "$i - possible steps ".Dumper(@possibleSteps)."\n";

	my @sortedSteps = sort @possibleSteps;

	if ($numWorkers == 0 || scalar @possibleSteps == 0) {
		print "$i - Just do work\n";
		my $minTime = 9999999999;
		foreach my $step (keys $stepsInProgress) {
			$minTime = $stepsInProgress->{$step} if $minTime > $stepsInProgress->{$step};
		}
		if ($minTime == 9999999999) {
			print "FUCK\n";
			print Dumper $dependencies;
			print Dumper $stepsInProgress;
			print Dumper $precedence;
		}
		print "$i - minTime $minTime\n";
		foreach my $step (keys $stepsInProgress) {
			 $stepsInProgress->{$step} -= $minTime;
			if ($stepsInProgress->{$step} == 0) {
				foreach my $nextKey (@{$precedence->{$step}}) {
					$dependencies->{$nextKey}--;
				}

				delete $precedence->{$step};
				delete $stepsInProgress->{$step};
				$numWorkers++;
			}
		}
		$result += $minTime;
	} else {
		foreach my $step (@sortedSteps) {
			print "$i - Work to do for step $step\n";
			unless ($numWorkers == 0) {
				$numWorkers--;
				$stepsInProgress->{$step} = 60 + (ord($step) - 64);
				print "$i - $step will take ".$stepsInProgress->{$step}."\n";
				delete $dependencies->{$step};
			}
		}
	}
	$i++;
}

