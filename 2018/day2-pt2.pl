use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day2.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

my $twoCount = 0;
my $threeCount = 0;

for (my $i = 0; $i < scalar @lines; $i++) {
	my @firstChars = split //, $lines[$i];

	for (my $j = $i + 1; $j < scalar @lines; $j++) {
		my @secondChars = split //, $lines[$j];

		my $differences = 0;
		my $differentChar;
		for (my $k = 0; $k < 26; $k++) {
			if ($firstChars[$k] ne $secondChars[$k]) {
				$differences++;
				$differentChar = "$firstChars[$k].$secondChars[$k] @ $k";
			}
		}

		if ($differences == 1) {
			print ("FOUND IT\n");
			print ("First: ".$lines[$i]."\n");
			print ("second: ".$lines[$j]."\n");
			print ("Different char: $differentChar\n");
			exit;
		}
	}
}
