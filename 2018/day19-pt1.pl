use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day19.txt";

my @lines = CL::importLinesFromFile($filename);

my $boundPtr = 3;
my $instrPtr = 0;

my $result = 0;

my $registers = {};

for (my $i = 0; $i < 6; $i++) {
	$registers->{$i} = 0;
}

while (1) {
	my $line = $lines[$instrPtr];
	#print "\n---\n$instrPtr\n";
	if ($instrPtr >= (scalar @lines)) {
		last;
	}
	#print "$line\n";
	$registers->{$boundPtr} = $instrPtr;
	if ($line =~ /addi (\d+) (\d+) (\d+)/) {
		$registers->{$3} = $registers->{$1} + $2;
	} elsif ($line =~ /addr (\d+) (\d+) (\d+)/) {
		$registers->{$3} = $registers->{$1} + $registers->{$2};
	} elsif ($line =~ /seti (\d+) (\d+) (\d+)/) {
		$registers->{$3} = $1;
	} elsif ($line =~ /setr (\d+) (\d+) (\d+)/) {
		$registers->{$3} = $registers->{$1};
	} elsif ($line =~ /muli (\d+) (\d+) (\d+)/) {
		$registers->{$3} = $registers->{$1} * $2;
	} elsif ($line =~ /mulr (\d+) (\d+) (\d+)/) {
		$registers->{$3} = $registers->{$1} * $registers->{$2};
	} elsif ($line =~ /gtrr (\d+) (\d+) (\d+)/) {
		$registers->{$3} = ($registers->{$1} > $registers->{$2}) ? 1 : 0;
	} elsif ($line =~ /eqrr (\d+) (\d+) (\d+)/) {
		$registers->{$3} = ($registers->{$1} == $registers->{$2}) ? 1 : 0;
	} else {
		print "SOMETHING ELSE\n";
		print "$line\n";
		exit;
	}
	#print Dumper $registers;
	$instrPtr = $registers->{$boundPtr} + 1;
}

print Dumper $registers;

print("$registers->{0}\n");
