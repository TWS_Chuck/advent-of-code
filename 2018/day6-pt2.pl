use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day6.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

my $points = {};
my $grid = {};

my $smallestX = 99999;
my $smallestY = 99999;
my $largestX = 0;
my $largestY = 0;

my $currentPoint = 0;
for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
	$line =~ /(\d+), (\d+)/;
	my ($x, $y) = (int($1), int($2));

	$largestX = $x if $x > $largestX;
	$largestY = $y if $y > $largestY;
	$smallestX = $x if $x < $smallestX;
	$smallestY = $y if $y < $smallestY;

	$points->{$currentPoint}{'x'} = $x;
	$points->{$currentPoint}{'y'} = $y;
	$grid->{$x}{$y}{'dist'} = 0;

	$currentPoint++;
}

for (my $x = $smallestX; $x <= $largestX; $x++) {
	for (my $y = $smallestY; $y <= $largestY; $y++) {
		foreach my $point (keys $points) {
			my $distance = abs($x - $points->{$point}{'x'}) + abs($y - $points->{$point}{'y'});
			$grid->{$x}{$y}{'dist'} += $distance;
		}
		if ($grid->{$x}{$y}{'dist'} < 10000) {
			$result++;
		}
	}
}

print("$result\n");
