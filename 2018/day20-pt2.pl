use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day20.txt";

my @chars = CL::importSingleLineFromFile('', $filename);

my $grid = {};

my $startX = 0;
my $startY = 0;

my $result = 0;

my $startIndex = 1;
my $endIndex = (scalar @chars) - 2;

handleRegex($startIndex, $endIndex, $startX, $startY);

$result = exploreGrid();

sub handleRegex {
	my $start = shift;
	my $end = shift;
	my $startX = shift;
	my $startY = shift;

	my $parDepth = 0;
	my $startPar = -1;
	my $endPar = -1;

	my $x = $startX;
	my $y = $startY;
	for (my $i = $start; $i <= $end; $i++) {
		if ($chars[$i] eq '(') {
			$startPar = $i if ($parDepth == 0);
			$parDepth++;
		} elsif ($chars[$i] eq ')') {
			$parDepth--;
			$endPar = $i if ($parDepth == 0);
			# Handle sub group
			($x, $y) = handleRegex($startPar + 1, $endPar - 1, $x, $y);
		}

		if ($parDepth > 0) {
			next;
		}

		if ($chars[$i] eq '|') {
			$x = $startX;
			$y = $startY;
			next;
		}
		my $dir = $chars[$i];
		if ($dir eq 'N') {
			$grid->{$y}{$x}{0} = 1;
		} elsif ($dir eq 'E') {
			$grid->{$y}{$x}{1} = 1;
		} elsif ($dir eq 'S') {
			$grid->{$y}{$x}{2} = 1;
		} elsif ($dir eq 'W') {
			$grid->{$y}{$x}{3} = 1;
		}

		if ($dir eq 'N') {
			$y++;
		} elsif ($dir eq 'E') {
			$x++;
		} elsif ($dir eq 'S') {
			$y--;
		} elsif ($dir eq 'W') {
			$x--;
		}

		if ($dir eq 'N') {
			$grid->{$y}{$x}{2} = 1;
		} elsif ($dir eq 'E') {
			$grid->{$y}{$x}{3} = 1;
		} elsif ($dir eq 'S') {
			$grid->{$y}{$x}{0} = 1;
		} elsif ($dir eq 'W') {
			$grid->{$y}{$x}{1} = 1;
		}
	}

	return ($x, $y);
}

sub exploreGrid {
	$startX = 0;
	$startY = 0;

	my $distance = 0;
	my $numDoors = 0;

	my $queue = {
		$startY => {
			$startX => 1,
		},
	};

	my $searched = {};

	while (1) {
		my $nextQueue = {};
		$distance++;
		# Check if queue contains target
		foreach my $y (sort { $a <=> $b } (keys $queue)) {
			foreach my $x (sort { $a <=> $b } (keys $queue->{$y})) {
				$searched->{$y}{$x} = 1;
				# Explore in all possible directions
				for (my $i = 0; $i < 4; $i++) {
					if (defined $grid->{$y}{$x}{$i} && $grid->{$y}{$x}{$i} == 1) {
						if ($i == 0 && (not defined $searched->{$y+1}{$x})) {
							$nextQueue->{$y+1}{$x} = $distance;
							$numDoors++ if $distance >= 1000;
						} elsif ($i == 1 && (not defined $searched->{$y}{$x+1})) {
							$nextQueue->{$y}{$x+1} = 1;
							$numDoors++ if $distance >= 1000;
						} elsif ($i == 2 && (not defined $searched->{$y-1}{$x})) {
							$nextQueue->{$y-1}{$x} = 1;
							$numDoors++ if $distance >= 1000;
						} elsif ($i == 3 && (not defined $searched->{$y}{$x-1})) {
							$nextQueue->{$y}{$x-1} = 1;
							$numDoors++ if $distance >= 1000;
						}
					}
				}
			}
		}
		if (scalar (keys $nextQueue) == 0) {
			return $numDoors;
		}
		$queue = $nextQueue;
	}
}

print("$result\n");
