use strict;
use warnings;

use CL;

use Data::Dumper;

my $input = 7139;

my $grid = {};

for (my $i = 0; $i < 300; $i++) {
	for (my $j = 0; $j < 300; $j++) {
		my $rackID = $i + 10;
		$grid->{$i}{$j} = $rackID * $j;
		$grid->{$i}{$j} += $input;
		$grid->{$i}{$j} *= $rackID;
		$grid->{$i}{$j} = (($grid->{$i}{$j} / 100) % 10);
		$grid->{$i}{$j} -= 5;
	}
}

my ($xCoord, $yCoord, $maxPowerLevel) = CL::getLargestSectionOfMultiArray(3,3,$grid);

print("$maxPowerLevel $xCoord $yCoord\n");
