use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day3.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

my @fabric;

#Cut from fabric
for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
	$line =~ /(\d+),(\d+): (\d+)x(\d+)/;
	my $x = $1;
	my $y = $2;
	my $width = $3;
	my $height = $4;

	#print("$x $y $width $height\nCutting now\n");

	for (my $j = $x; $j < $x + $width; $j++) {
		for (my $k = $y; $k < $y + $height; $k++) {
			$fabric[$j][$k] += 1;
		}
	}
}

print ("Created fabric\n");

#Check fabric
for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
	$line =~ /(\d+),(\d+): (\d+)x(\d+)/;
	my $x = $1;
	my $y = $2;
	my $width = $3;
	my $height = $4;

	my $overlap = 0;
	for (my $j = $x; $j < $x + $width; $j++) {
		for (my $k = $y; $k < $y + $height; $k++) {
			if ($fabric[$j][$k] != 1) {
				$overlap = 1;
			}
		}
	}

	if ($overlap == 0) {
		print ("FOUND IT $i\n");
	}
}
