use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day14.txt";

my $input = 990941;

my $result = 0;

my $recipes = {
	0 => 3,
	1 => 7,
};

my $numRecipes = 2;

my $firstElf = 0;
my $secondElf = 1;

my $lastChecked = 0;

my $sequenceStartedAt = -1;
while (1) {
	my $sum = $recipes->{$firstElf} + $recipes->{$secondElf};
	my $tens = int($sum / 10);
	my $ones = $sum % 10;

	if ($tens != 0) {
		$recipes->{$numRecipes} = $tens;
		$numRecipes++;
	}
	$recipes->{$numRecipes} = $ones;
	$numRecipes++;

	$firstElf = ($firstElf + 1 + $recipes->{$firstElf}) % $numRecipes;
	$secondElf = ($secondElf + 1 + $recipes->{$secondElf}) % $numRecipes;

	if ($numRecipes - $lastChecked < 6) {
		next;
	}

	for (my $j = $lastChecked; $j < $numRecipes - 6; $j++) {
		if (
			$recipes->{$j} == 9 &&
			$recipes->{$j+1} == 9 &&
			$recipes->{$j+2} == 0 &&
			$recipes->{$j+3} == 9 &&
			$recipes->{$j+4} == 4 &&
			$recipes->{$j+5} == 1
			) {
				print "Found pattern after $j\n";
				exit;
			}
	}
	$lastChecked = $numRecipes - 6;
}
