use strict;
use warnings;

use CL;

use Data::Dumper;
use Storable qw(dclone);

my $filename = "day16.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

for (my $i = 0; $i < scalar @lines; $i+=4) {
	my $before = $lines[$i];
	my $instr = $lines[$i+1];
	my $after = $lines[$i+2];

	$before =~ /Before: \[(\d+), (\d+), (\d+), (\d+)\]/;
	my $actualRegisters = {
		0 => int($1),
		1 => int($2),
		2 => int($3),
		3 => int($4),
	};

	$after =~ /After:  \[(\d+), (\d+), (\d+), (\d+)\]/;
	my $expectedRegisters = {
		0 => int($1),
		1 => int($2),
		2 => int($3),
		3 => int($4),
	};

	$instr =~ /(\d+) (\d+) (\d+) (\d+)/;
	my $instruction = {
		op => int($1),
		a => int($2),
		b => int($3),
		c => int($4),
	};

	my $numOpsApply = 0;

	# Addition
	# addr
	my $addr = dclone($actualRegisters);
	$addr->{$instruction->{c}} = $actualRegisters->{$instruction->{a}} + $actualRegisters->{$instruction->{b}};
	$numOpsApply++ if compareHashes($addr, $expectedRegisters);

	# addi
	my $addi = dclone($actualRegisters);
	$addi->{$instruction->{c}} = $actualRegisters->{$instruction->{a}} + $instruction->{b};
	$numOpsApply++ if compareHashes($addi, $expectedRegisters);

	# Mult
	# mulr
	my $mulr = dclone($actualRegisters);
	$mulr->{$instruction->{c}} = $actualRegisters->{$instruction->{a}} * $actualRegisters->{$instruction->{b}};
	$numOpsApply++ if compareHashes($mulr, $expectedRegisters);

	# muli
	my $muli = dclone($actualRegisters);
	$muli->{$instruction->{c}} = $actualRegisters->{$instruction->{a}} * $instruction->{b};
	$numOpsApply++ if compareHashes($muli, $expectedRegisters);

	# Bitwise AND
	# banr
	my $banr = dclone($actualRegisters);
	$banr->{$instruction->{c}} = $actualRegisters->{$instruction->{a}} & $actualRegisters->{$instruction->{b}};
	$numOpsApply++ if compareHashes($banr, $expectedRegisters);

	# bani
	my $bani = dclone($actualRegisters);
	$bani->{$instruction->{c}} = $actualRegisters->{$instruction->{a}} & $instruction->{b};
	$numOpsApply++ if compareHashes($bani, $expectedRegisters);

	# Bitwise OR
	# borr
	my $borr = dclone($actualRegisters);
	$borr->{$instruction->{c}} = $actualRegisters->{$instruction->{a}} | $actualRegisters->{$instruction->{b}};
	$numOpsApply++ if compareHashes($borr, $expectedRegisters);

	# bori
	my $bori = dclone($actualRegisters);
	$bori->{$instruction->{c}} = $actualRegisters->{$instruction->{a}} | $instruction->{b};
	$numOpsApply++ if compareHashes($bori, $expectedRegisters);

	# Assignment
	# setr
	my $setr = dclone($actualRegisters);
	$setr->{$instruction->{c}} = $actualRegisters->{$instruction->{a}};
	$numOpsApply++ if compareHashes($setr, $expectedRegisters);

	# seti
	my $seti = dclone($actualRegisters);
	$seti->{$instruction->{c}} = $instruction->{a};
	$numOpsApply++ if compareHashes($seti, $expectedRegisters);

	# Greater than
	# gtir
	my $gtir = dclone($actualRegisters);
	$gtir->{$instruction->{c}} = ($instruction->{a} > $actualRegisters->{$instruction->{b}}) ? 1 : 0;
	$numOpsApply++ if compareHashes($gtir, $expectedRegisters);

	# gtri
	my $gtri = dclone($actualRegisters);
	$gtri->{$instruction->{c}} = ($actualRegisters->{$instruction->{a}} > $instruction->{b}) ? 1 : 0;
	$numOpsApply++ if compareHashes($gtri, $expectedRegisters);

	# gtrr
	my $gtrr = dclone($actualRegisters);
	$gtrr->{$instruction->{c}} = ($actualRegisters->{$instruction->{a}} > $actualRegisters->{$instruction->{b}}) ? 1 : 0;
	$numOpsApply++ if compareHashes($gtrr, $expectedRegisters);

	# Equality
	# eqir
	my $eqir = dclone($actualRegisters);
	$eqir->{$instruction->{c}} = ($instruction->{a} == $actualRegisters->{$instruction->{b}}) ? 1 : 0;
	$numOpsApply++ if compareHashes($eqir, $expectedRegisters);

	# eqri
	my $eqri = dclone($actualRegisters);
	$eqri->{$instruction->{c}} = ($actualRegisters->{$instruction->{a}} == $instruction->{b}) ? 1 : 0;
	$numOpsApply++ if compareHashes($eqri, $expectedRegisters);

	# eqrr
	my $eqrr = dclone($actualRegisters);
	$eqrr->{$instruction->{c}} = ($actualRegisters->{$instruction->{a}} == $actualRegisters->{$instruction->{b}}) ? 1 : 0;
	$numOpsApply++ if compareHashes($eqrr, $expectedRegisters);

	$result++ if $numOpsApply >= 3;
}

print("$result\n");

sub compareHashes {
	my $actual = shift;
	my $expected = shift;

	if (scalar (keys $actual) != scalar (keys $expected)) {
		print "Diff num keys\n";
		return 0;
	}

	foreach my $key (keys $expected) {
		if ($actual->{$key} != $expected->{$key}) {
			return 0;
		}
	}

	return 1;
}