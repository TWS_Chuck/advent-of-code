use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day15.txt";

my @multiArray = CL::importMultiArrayFromFile($filename);

my $result = 0;

my $elfDirs = {
	'E' => -1,
};

my $goblinDirs = {
	'G' => -1,
};

my $defaults = {
	health => 200,
	attack => 3,
};

my $locations = {};

my ($elves) = CL::GetAgentsAndLocations(\@multiArray, $elfDirs, $defaults, $locations);
my ($goblins) = CL::GetAgentsAndLocations(\@multiArray, $goblinDirs, $defaults, $locations, 100);


for (my $row = 0; $row < scalar @multiArray; $row++) {
	for (my $col = 0; $col < scalar @{$multiArray[$row]}; $col++) {
	}
}

my $rounds = 0;
while (1) {
	my @agentsToMove = @{CL::GetAgentsInReadOrder($locations)};

	foreach my $agent (@agentsToMove) {
		if ($agent >= 100) {
			next if not defined $goblins->{$agent};
			# Goblin
			my $distance;

			my $col = $goblins->{$agent}{x};
			my $row = $goblins->{$agent}{y};
			my $goblin = $goblins->{$agent};
			($goblin->{dir}, $distance) = CL::BreadthFirstSearchForTarget(\@multiArray, $goblin, 'E');

			if ($distance > 1) {
				# Move
				# TODO Replace old char as part of move
				my ($nextX, $nextY) = CL::MoveAgentAndUpdateDirection(\@multiArray, $goblin, $locations);

				unless (defined $locations->{$nextY} && defined $locations->{$nextY}{$nextX}) {
					$multiArray[$row][$col] = '.';
					delete $locations->{$row}{$col};
					if (scalar keys $locations->{$row} == 0) {
						delete $locations->{$row};
					}

					$goblin->{x} = $nextX;
					$goblin->{y} = $nextY;


					$multiArray[$goblin->{y}][$goblin->{x}] = 'G';
					$locations->{$goblin->{y}}{$goblin->{x}} = $agent;

					$distance--;
				}
			}
			if ($distance == 1) {
				# Where should we attack?
				my $elfID = CL::FindNeighbouringAgentWithLowestValue($goblin, $elves, $locations, 'health');
				my $elf = $elves->{$elfID};

				# Attack
				$elf->{health} -= $goblin->{attack};
				
				if ($elf->{health} <= 0) {
					# Elf is dead
					CL::RemoveAgent($elfID, $elves, $locations, \@multiArray);
				}

				if (keys $elves == 0) {
					my $totalHealth = 0;
					foreach my $goblin (keys $goblins) {
						$totalHealth += $goblins->{$goblin}{health};
					}
					$result = $rounds * $totalHealth;
					print "After $rounds rounds, the total health is $totalHealth. So $result\n";
					exit;
				}
			}
		} else {
			next if not defined $elves->{$agent};
			# Elf
			my $distance;

			my $col = $elves->{$agent}{x};
			my $row = $elves->{$agent}{y};
			my $elf = $elves->{$agent};
			($elf->{dir}, $distance) = CL::BreadthFirstSearchForTarget(\@multiArray, $elf, 'G');

			if ($distance > 1) {
				# Move
				my ($nextX, $nextY) = CL::MoveAgentAndUpdateDirection(\@multiArray, $elf, $locations);

				unless (defined $locations->{$nextY} && defined $locations->{$nextY}{$nextX}) {
					$multiArray[$row][$col] = '.';
					delete $locations->{$row}{$col};
					if (keys $locations->{$row} == 0) {
						delete $locations->{$row};
					}

					$elf->{x} = $nextX;
					$elf->{y} = $nextY;


					$multiArray[$elf->{y}][$elf->{x}] = 'E';
					$locations->{$elf->{y}}{$elf->{x}} = $agent;

					$distance--;
				}
			}
			if ($distance == 1) {
				# Where should we attack?
				my $goblinID = CL::FindNeighbouringAgentWithLowestValue($elf, $goblins, $locations, 'health');
				my $goblin = $goblins->{$goblinID};

				# Attack
				$goblin->{health} -= $elf->{attack};
				
				if ($goblin->{health} <= 0) {
					# Goblin is dead
					CL::RemoveAgent($goblinID, $goblins, $locations, \@multiArray);
				}

				if (keys $goblins == 0) {
					my $totalHealth = 0;
					foreach my $elf (keys $elves) {
						$totalHealth += $elves->{$elf}{health};
					}
					$result = $rounds * $totalHealth;
					print "After $rounds rounds, the total health is $totalHealth. So $result\n";
					exit;
				}
			}
		}
	}

	$rounds++;
}
