use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day7.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = "";

my $precedence = {};

my $dependencies = {};

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
	$line =~ /Step (\w) must be finished before step (\w) can begin./;
	if (not defined $precedence->{$1}) {
		$precedence->{$1} = [];
	}
	push $precedence->{$1}, $2;
	if (not defined ($dependencies->{$1})) {
		$dependencies->{$1} = 0;
	}
	$dependencies->{$2}++;
}

print Dumper $precedence;

while (1) {
	print Dumper $dependencies;
	my @possibleSteps;
	foreach my $step (keys $dependencies) {
		if ($dependencies->{$step} == 0) {
			push @possibleSteps, $step;
		}
	}

	my @sortedSteps = sort @possibleSteps;
	$result .= $sortedSteps[0];
	print Dumper $result;

	print Dumper ($precedence->{$sortedSteps[0]});
	foreach my $nextKey (@{$precedence->{$sortedSteps[0]}}) {
		print "Decrementing $nextKey\n";
		$dependencies->{$nextKey}--;
	}
	print Dumper $dependencies;
	delete $precedence->{$sortedSteps[0]};
	delete $dependencies->{$sortedSteps[0]};

	if (scalar (keys $dependencies) == 0) {
		print("$result\n");
		exit;
	}
}

