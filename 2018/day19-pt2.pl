use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day19.txt";

my @lines = CL::importLinesFromFile($filename);

my $factors = {};

my $result = 0;

for (my $i = 1; $i < sqrt(10551378); $i++) {
	if (10551378 % $i == 0) {
		$result += $i;
		$result += 10551378 / $i;
	}
}

print("$result\n");
