use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day4.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

my $guardSleep = {};
my $currentGuard;
my $sleepStart = -1;
my $mostSleptMinute = -1;
my $mostSleptMinuteAmount = 0;
my $mostSleptMinuteGuard;

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
	$line =~ /\[(\d+)-(\d+)-(\d+) (\d+):(\d+)\]/;
	my ($year, $month, $day, $hour, $minute) = (int($1), int($2), int($3), int($4), int($5));

	if ($line =~ /#(\d+) begins shift/) {
		$currentGuard = int($1);
	} elsif ($line =~ /falls asleep/) {
		$sleepStart = int($minute);
	} elsif ($line =~ /wakes up/ && $sleepStart != -1) {
		for (my $i = $sleepStart; $i < $minute; $i++) {
			$guardSleep->{$currentGuard}{$i}++;
			if ($guardSleep->{$currentGuard}{$i} > $mostSleptMinuteAmount) {
				$mostSleptMinute = $i;
				$mostSleptMinuteAmount = $guardSleep->{$currentGuard}{$i};
				$mostSleptMinuteGuard = $currentGuard;
			}
		}
		$sleepStart = -1;
	}
}

$result = $mostSleptMinute * $mostSleptMinuteGuard;

print("$mostSleptMinute $mostSleptMinuteGuard $result\n");
