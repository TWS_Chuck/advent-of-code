use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day23.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

my $bots = {};

my $smallestRange = 99999999999;
my $smallestRangeBot = -1;

my $minX = 99999999;
my $minY = 99999999;
my $minZ = 99999999;
my $maxX = -99999999;
my $maxY = -99999999;
my $maxZ = -99999999;

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
	$line =~ /pos=<(-?)(\d+),(-?)(\d+),(-?)(\d+)>, r=(\d+)/;
	my $x = $2;
	$x *= -1 if $1 eq '-';
	my $y = $4;
	$y *= -1 if $3 eq '-';
	my $z = $6;
	$z *= -1 if $5 eq '-';

	my $r = $7;

	$bots->{$i} = {
		x => $x,
		y => $y,
		z => $z,
		r => $r,
	};

	if ($r < $smallestRange) {
		$smallestRange = $r;
		$smallestRangeBot = $i;
	}
	print "This bot small - $i - $r\n" if ($r == $smallestRange);

	$minX = $x if $x < $minX;
	$minY = $y if $y < $minY;
	$minZ = $z if $z < $minZ;
	$maxX = $x if $x > $maxX;
	$maxY = $y if $y > $maxY;
	$maxZ = $z if $z > $maxZ;
}

my $targetBotID = $smallestRangeBot;
my $botToDieFor = $bots->{$targetBotID};
my $targetX = $botToDieFor->{x};
my $targetY = $botToDieFor->{y};
my $targetZ = $botToDieFor->{z};
my $targetRange = $botToDieFor->{r};

my $bestX;
my $bestY;
my $bestZ;

my $searched = {};

my $partRange = int ($targetRange/2);
my $otherPart = $targetRange - $partRange;

# Would periodically reinsert a point with decently high signal strength and low distance from origin, and play with scale to find a better point quickly
my ($firstX, $firstY, $firstZ) = (10591863, 41145435, 30273107); # ($targetX + $targetRange, $targetY, $targetZ);
my $scale = 1;

my $queue = {
	$firstZ => {
		$firstY => {
			$firstX => 0,
		} 
	}
};

my $bestDistance = 9999999999999;

while (1) {
	my $nextQueue = {};

	foreach my $z (sort { $a <=> $b } (keys $queue)) {
		foreach my $y (sort { $a <=> $b } (keys $queue->{$z})) {
			foreach my $x (sort { $a <=> $b } (keys $queue->{$z}{$y})) {
				next if (defined $searched->{$z} && defined $searched->{$z}{$y} && defined $searched->{$z}{$y}{$x});
				# Calculate strength
				my $localResult = 0;
				foreach my $botID (keys $bots) {
					my $botX = $bots->{$botID}{x};
					my $botY = $bots->{$botID}{y};
					my $botZ = $bots->{$botID}{z};
					my $botR = $bots->{$botID}{r};

					my $distance;
					$distance += abs($x - $botX);
					$distance += abs($y - $botY);
					$distance += abs($z - $botZ);

					$localResult++ if $distance <= $botR;
				}

				# Add it to searched
				$searched->{$z}{$y}{$x} = $localResult;

				if ($localResult > $result) {
					$bestDistance = 9999999999999;
				}
				my $manhattanDistance = abs($x) + abs($y) + abs($z);
				if ($localResult >= $result && $manhattanDistance < $bestDistance) {
						$result = $localResult;
						$bestX = $x;
						$bestY = $y;
						$bestZ = $z;

						$bestDistance = $manhattanDistance;
						print "New best! $result - $bestX, $bestY, $bestZ - $manhattanDistance\n";
				} else {
					next;
				}

				foreach my $offset (@CL::ThreeDAllNeighbours) {
					my $xO = $offset->{x} * $scale;
					my $yO = $offset->{y} * $scale;
					my $zO = $offset->{z} * $scale;

					next if (defined $searched->{$z+$zO}{$y+$yO}{$x+$xO});

					# my $distanceFromTarget = 0;
					# $distanceFromTarget += abs($targetX - ($x+$xO));
					# $distanceFromTarget += abs($targetY - ($y+$yO));
					# $distanceFromTarget += abs($targetZ - ($z+$zO));

					# next if $distanceFromTarget != $targetRange;

					$nextQueue->{$z+$zO}{$y+$yO}{$x+$xO} = 0;
				}
			}
		}
	}

	if (defined $searched->{$targetY} && defined $searched->{$targetY}{$targetX}) {
		print Dumper $searched->{$targetY}{$targetX}->{0};
	}

	if (scalar (keys $nextQueue) == 0) {
		print "Ran out of things\n";
		last;
	}

	$queue = $nextQueue;
}

print("$result - $bestX $bestY $bestZ + ".($bestX + $bestY + $bestZ)."\n");
