use strict;
use warnings;

use CL;

use Data::Dumper;
use Storable qw(dclone);

my $filename = "day25.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

my $starID = 0;
my $looseStars = {};
my $constellations = {};

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
	$line =~ /(-?)(\d+),(-?)(\d+),(-?)(\d+),(-?)(\d+)/;
	my $x = $2;
	$x *= -1 if $1 eq '-';
	my $y = $4;
	$y *= -1 if $3 eq '-';
	my $z = $6;
	$z *= -1 if $5 eq '-';
	my $t = $8;
	$t *= -1 if $7 eq '-';

	$looseStars->{$starID} = {
		x => $x,
		y => $y,
		z => $z,
		t => $t,
	};

	$starID++;
}

my $constellationID = 0;
while (1) {
	if (scalar (keys $looseStars) == 0) {
		print "Stars exhausted\n";
		last;
	}

	my $currentConstellation = {};

	foreach my $star (keys $looseStars) {
		# Just grab the first star we see
		$currentConstellation->{$star} = dclone($looseStars->{$star});
		delete $looseStars->{$star};
		last;
	}

	while (1) {
		# Keep grabbing stars that are close to the chosen stars
		my $starsAdded = 0;
		foreach my $star (keys $looseStars) {
			my $starX = $looseStars->{$star}{x};
			my $starY = $looseStars->{$star}{y};
			my $starZ = $looseStars->{$star}{z};
			my $starT = $looseStars->{$star}{t};

			# Just grab the first star we see
			foreach my $constellationStar (keys $currentConstellation) {
				my $constX = $currentConstellation->{$constellationStar}{x};
				my $constY = $currentConstellation->{$constellationStar}{y};
				my $constZ = $currentConstellation->{$constellationStar}{z};
				my $constT = $currentConstellation->{$constellationStar}{t};

				my $manhattanDistance = abs($starX - $constX) + abs($starY - $constY) + abs($starZ - $constZ) + abs($starT - $constT);

				if ($manhattanDistance <= 3) {
					# This one is pretty close
					$currentConstellation->{$star} = dclone($looseStars->{$star});
					delete $looseStars->{$star};
					$starsAdded = 1;
					last;
				}
			}
		}

		last if $starsAdded == 0;
	}

	$constellations->{$constellationID} = $currentConstellation;
	$constellationID++;
}

print("$constellationID\n");
