use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day12.txt";

my @lines = CL::importLinesFromFile($filename);

my @plantChars = split //, ".....#...#..###.#.###.####.####.#..#.##..#..##..#.....#.#.#.##.#...###.#..##..#.##..###..#..##.#..##.....";

my $offset = 5;

my $result = 0;

my $plants = {};
for (my $i = 0; $i < scalar @plantChars; $i++) {
	$plants->{$i} = $plantChars[$i];
}

my $smallest = 0;
my $largest = scalar @plantChars;

for (my $gen = 1; $gen <= 110; $gen++) {
	my $nextPlants = {};
	for (my $i = $smallest + 2; $i < scalar $largest - 2; $i++) {
		my $replaced = 0;

		$nextPlants->{$i} = '.';

		$plants->{$i-2} = '.' if (not defined $plants->{$i-2});
		$plants->{$i-1} = '.' if (not defined $plants->{$i-1});
		$plants->{$i} = '.' if (not defined $plants->{$i});
		foreach my $line (@lines) {
			$line =~ /([.#]{5}) => ([.#])/;
			my @plantsToSearch = split //, $1;
			my $replace = $2;

			if ($plantsToSearch[2] eq $plants->{$i}) {
				$plants->{$i+1} = '.' if (not defined $plants->{$i+1});
				$plants->{$i+2} = '.' if (not defined $plants->{$i+2});

				my $leftmost = ($plants->{$i-2} eq $plantsToSearch[0]) ? 1 : 0;
				my $cLeft = ($plants->{$i-1} eq $plantsToSearch[1]) ? 1 : 0;
				my $cRight = ($plants->{$i+1} eq $plantsToSearch[3]) ? 1 : 0;
				my $rightmost = ($plants->{$i+2} eq $plantsToSearch[4]) ? 1 : 0;
				if ($leftmost && $cLeft && $cRight && $rightmost) {
					$nextPlants->{$i} = $replace;
					$replaced = 1;
				}
			}
			last if $replaced;
		}
	}
	$plants = $nextPlants;
	for (my $i = $smallest; $i <= $largest; $i++) {
		$plants->{$i} = '.' if (not defined $plants->{$i});
		if ($plants->{$i} eq '#') {
			$smallest += ($i - $smallest) - 5;
			last;
		}
	}
	for (my $i = $largest; $i >= $smallest; $i--) {
		$plants->{$i} = '.' if (not defined $plants->{$i});
		if ($plants->{$i} eq '#') {
			$largest += 6 - ($largest - $i);
			last;
		}
	}
	for (my $i = $smallest; $i < $largest; $i++) {
		$plants->{$i} = '.' if (not defined $plants->{$i});
		print $plants->{$i};
	}
	print " - $smallest $largest $gen\n";
}

my $totalPlants = 0;
for (my $i = $smallest; $i < $largest; $i++) {
	if ($plants->{$i} eq "#") {
		$result += ($i - $offset + 50000000000 - 110);
		$totalPlants++;
		print "$i ";
	}
}
print "\n";

print("total plants: $totalPlants - $result\n");
