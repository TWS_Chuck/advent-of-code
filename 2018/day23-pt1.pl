use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day23.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

my $grid = {};
my $bots = {};

my $largestRange = 0;
my $largestRangeBot = -1;

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
	$line =~ /pos=<(-?)(\d+),(-?)(\d+),(-?)(\d+)>, r=(\d+)/;
	my $x = $2;
	$x *= -1 if $1 eq '-';
	my $y = $4;
	$y *= -1 if $3 eq '-';
	my $z = $6;
	$z *= -1 if $5 eq '-';

	my $r = $7;

	$grid->{$z}{$y}{$x} = $r;
	$bots->{$i} = {
		x => $x,
		y => $y,
		z => $z,
		r => $r,
	};

	if ($r > $largestRange) {
		$largestRange = $r;
		$largestRangeBot = $i;
	}
}

my $botToDieFor = $bots->{$largestRangeBot};
my $targetX = $botToDieFor->{x};
my $targetY = $botToDieFor->{y};
my $targetZ = $botToDieFor->{z};
my $targetRange = $botToDieFor->{r};

foreach my $botID (keys $bots) {
	my $currX = $bots->{$botID}{x};
	my $currY = $bots->{$botID}{y};
	my $currZ = $bots->{$botID}{z};

	my $distance;
	$distance += abs($targetX - $currX);
	$distance += abs($targetY - $currY);
	$distance += abs($targetZ - $currZ);

	$result++ if $distance <= $targetRange;
}

print("$result\n");
