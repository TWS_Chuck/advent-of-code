use strict;
use warnings;

use CL;

use Data::Dumper;

my $filename = "day4.txt";

my @lines = CL::importLinesFromFile($filename);

my $result = 0;

my $guardSleep = {};
my $currentGuard;
my $sleepStart = -1;

for (my $i = 0; $i < scalar @lines; $i++) {
	my $line = $lines[$i];
	$line =~ /\[(\d+)-(\d+)-(\d+) (\d+):(\d+)\]/;
	my ($year, $month, $day, $hour, $minute) = (int($1), int($2), int($3), int($4), int($5));

	if ($line =~ /#(\d+) begins shift/) {
		$currentGuard = int($1);
	} elsif ($line =~ /falls asleep/) {
		$sleepStart = int($minute);
	} elsif ($line =~ /wakes up/ && $sleepStart != -1) {
		for (my $i = $sleepStart; $i < $minute; $i++) {
			$guardSleep->{$currentGuard}{$i}++;
			$guardSleep->{$currentGuard}{'total'}++;
		}
		$sleepStart = -1;
	}
}

my $maxSleepGuard;
my $maxSleep = 0;

foreach my $guard (keys $guardSleep) {
	if ($guardSleep->{$guard}{'total'} > $maxSleep) {
		$maxSleep = $guardSleep->{$guard}{'total'};
		$maxSleepGuard = $guard;
	}
}

my $minuteSlept;
my $mostSlept = 0;

for (my $i = 0; $i < 60; $i++) {
	if (defined $guardSleep->{$maxSleepGuard}{$i} && $guardSleep->{$maxSleepGuard}{$i} > $mostSlept) {
		$mostSlept = $guardSleep->{$maxSleepGuard}{$i};
		$minuteSlept = $i;
	}
}

$result = $minuteSlept * $maxSleepGuard;

print("$minuteSlept $maxSleepGuard $result\n");
