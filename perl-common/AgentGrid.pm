package AgentGrid;

use strict;
use warnings;

use Data::Dumper;

#####################################
# Moving-on-a-grid helper functions #
#####################################

# Directions, 0 N, 1 E, 2 S, 3 W
# Inter, -1 L, 0 S, 1 R

our @ManhattanNeighbours = (
	{
		x => 0,
		y => -1,
		dir => 0,
	},
	{
		x => -1,
		y => 0,
		dir => 3,
	},
	{
		x => 1,
		y => 0,
		dir => 1,
	},
	{
		x => 0,
		y => 1,
		dir => 2,
	},
);
our @EightDirectionNeighbours = (
	{
		x => -1,
		y => -1,
		dir => 0,
	},
	{
		x => 0,
		y => -1,
		dir => 0,
	},
	{
		x => 1,
		y => -1,
		dir => 0,
	},
	{
		x => -1,
		y => 0,
		dir => 3,
	},
	{
		x => 1,
		y => 0,
		dir => 3,
	},
	{
		x => -1,
		y => 1,
		dir => 1,
	},
	{
		x => 0,
		y => 1,
		dir => 2,
	},
	{
		x => 1,
		y => 1,
		dir => 1,
	},
);
our @ThreeDAllNeighbours = (
	{
		x => -1,
		y => -1,
		z => -1,
		dir => 0,
	},
	{
		x => -1,
		y =>  0,
		z => -1,
		dir => 0,
	},
	{
		x => -1,
		y =>  1,
		z => -1,
		dir => 0,
	},
	{
		x =>  0,
		y => -1,
		z => -1,
		dir => 0,
	},
	{
		x =>  0,
		y =>  0,
		z => -1,
		dir => 0,
	},
	{
		x =>  0,
		y =>  1,
		z => -1,
		dir => 0,
	},
	{
		x =>  1,
		y => -1,
		z => -1,
		dir => 0,
	},
	{
		x =>  1,
		y =>  0,
		z => -1,
		dir => 0,
	},
	{
		x =>  1,
		y =>  1,
		z => -1,
		dir => 0,
	},
	{
		x => -1,
		y => -1,
		z =>  0,
		dir => 0,
	},
	{
		x => -1,
		y =>  0,
		z =>  0,
		dir => 0,
	},
	{
		x => -1,
		y =>  1,
		z =>  0,
		dir => 0,
	},
	{
		x =>  0,
		y => -1,
		z =>  0,
		dir => 0,
	},
	{
		x =>  0,
		y =>  1,
		z =>  0,
		dir => 0,
	},
	{
		x =>  1,
		y => -1,
		z =>  0,
		dir => 0,
	},
	{
		x =>  1,
		y =>  0,
		z =>  0,
		dir => 0,
	},
	{
		x =>  1,
		y =>  1,
		z =>  0,
		dir => 0,
	},
	{
		x => -1,
		y => -1,
		z =>  1,
		dir => 0,
	},
	{
		x => -1,
		y =>  0,
		z =>  1,
		dir => 0,
	},
	{
		x => -1,
		y =>  1,
		z =>  1,
		dir => 0,
	},
	{
		x =>  0,
		y => -1,
		z =>  1,
		dir => 0,
	},
	{
		x =>  0,
		y =>  0,
		z =>  1,
		dir => 0,
	},
	{
		x =>  0,
		y =>  1,
		z =>  1,
		dir => 0,
	},
	{
		x =>  1,
		y => -1,
		z =>  1,
		dir => 0,
	},
	{
		x =>  1,
		y =>  0,
		z =>  1,
		dir => 0,
	},
	{
		x =>  1,
		y =>  1,
		z =>  1,
		dir => 0,
	},
);
our @ThreeDNeighbours = (
	{
		x =>  0,
		y => -1,
		z => -1,
		dir => 0,
	},
	{
		x =>  0,
		y => -1,
		z =>  1,
		dir => 0,
	},
	{
		x =>  0,
		y =>  1,
		z => -1,
		dir => 0,
	},
	{
		x =>  0,
		y =>  1,
		z =>  1,
		dir => 0,
	},
	{
		x => -1,
		y =>  0,
		z => -1,
		dir => 0,
	},
	{
		x => -1,
		y =>  0,
		z =>  1,
		dir => 0,
	},
	{
		x =>  1,
		y =>  0,
		z => -1,
		dir => 0,
	},
	{
		x =>  1,
		y =>  0,
		z =>  1,
		dir => 0,
	},
	{
		x => -1,
		y => -1,
		z =>  0,
		dir => 0,
	},
	{
		x => -1,
		y =>  1,
		z =>  0,
		dir => 0,
	},
	{
		x =>  1,
		y => -1,
		z =>  0,
		dir => 0,
	},
	{
		x =>  1,
		y =>  1,
		z =>  0,
		dir => 0,
	},
);
our @ThreeDManhattanNeighbours = (
	{
		x => -1,
		y =>  0,
		z =>  0,
		dir => 0,
	},
	{
		x =>  1,
		y =>  0,
		z =>  0,
		dir => 0,
	},
	{
		x =>  0,
		y => -1,
		z =>  0,
		dir => 0,
	},
	{
		x =>  0,
		y =>  1,
		z =>  0,
		dir => 0,
	},
	{
		x =>  0,
		y =>  0,
		z => -1,
		dir => 0,
	},
	{
		x =>  0,
		y =>  0,
		z =>  1,
		dir => 0,
	},
);

sub GetAgentsAndLocations {
	my $multiArray = shift;
	my $agentCharsAndDirections = shift;
	my $defaults = shift // {};
	my $locations = shift // {};
	my $agent = shift // 0;

	my %agents;

	for (my $row = 0; $row < scalar @{$multiArray}; $row++) {
		for (my $col = 0; $col < scalar @{$multiArray->[$row]}; $col++) {
			my $currentChar = $multiArray->[$row][$col];
			if (defined $agentCharsAndDirections->{$currentChar}) {
				addAgentAndLocation(\%agents, $locations, $agent, $defaults, $col, $row);
				$agents{$agent}{dir} = $agentCharsAndDirections->{$currentChar};
				$agent++;
			}
		}
	}

	return (\%agents, $locations);
}

sub addAgentAndLocation {
	my $agentsRef = shift;
	my $locationsRef = shift;
	my $agentID = shift;
	my $defaults = shift;
	my $x = shift;
	my $y = shift;

	$agentsRef->{$agentID}{x} = $x;
	$agentsRef->{$agentID}{y} = $y;
	foreach my $key (keys %{$defaults}) {
		$agentsRef->{$agentID}{$key} = $defaults->{$key};
	}

	$locationsRef->{$y}{$x} = $agentID;
}

sub GetAgentsInReadOrder {
	my $locations = shift;

	my @agentOrder = ();

	foreach my $row (sort { $a <=> $b } (keys %{$locations})) {
		foreach my $col (sort { $a <=> $b } (keys %{$locations->{$row}})) {
			push @agentOrder, $locations->{$row}{$col};
		}
	}

	return \@agentOrder;
}

# Move the agent based on its direction, and update any of its statuses, if required
sub MoveAgentAndUpdateDirection {
	my $multiArray = shift;
	my $agentRef = shift;
	my $locations = shift;
	my $directionUpdates = shift // {};

	my $oldX = $agentRef->{x};
	my $oldY = $agentRef->{y};

	$multiArray->[$oldY][$oldX] = '.';
	delete $locations->{$oldY}{$oldX};
	if (keys %{$locations->{$oldY}} == 0) {
		delete $locations->{$oldY};
	}

	my $direction = $agentRef->{dir};

	$agentRef->{y}-- if $direction == 0;
	$agentRef->{x}++ if $direction == 1;
	$agentRef->{y}++ if $direction == 2;
	$agentRef->{x}-- if $direction == 3;

	my $x = $agentRef->{x};
	my $y = $agentRef->{y};

	my $currentChar = $multiArray->[$y][$x];

	if (defined $directionUpdates->{$currentChar}) {
		if (defined $directionUpdates->{$currentChar}{$direction}) {
			$directionUpdates->{$currentChar}{$direction}($agentRef);
		} elsif (defined $directionUpdates->{$currentChar}{default}) {
			$directionUpdates->{$currentChar}{default}($agentRef);
		}
	}

	return ($x, $y);
}

sub FindNeighbouringAgentWithComparison {
	my $agent = shift;
	my $targets = shift;
	my $locations = shift;
	my $key = shift;
	my $comparison = shift;
	my $valueSoFar = shift;

	my $valueSoFarID = -1;

	my $x = $agent->{x};
	my $y = $agent->{y};

	foreach my $offset (@CL::ManhattanNeighbours) {
		my $currentX = $x + $offset->{x};
		my $currentY = $y + $offset->{y};

		my $currentTargetID = $locations->{$currentY}{$currentX} if defined $locations->{$currentY};
		my $currentTarget = $targets->{$currentTargetID} if defined $currentTargetID;

		if (defined $currentTarget && $comparison->($currentTarget->{$key}, $valueSoFar)) {
			$valueSoFar = $currentTarget->{$key};
			$valueSoFarID = $currentTargetID;
		}
	}

	return $valueSoFarID;
}

sub FindNeighbouringAgentWithLowestValue {
	my $agent = shift;
	my $targets = shift;
	my $locations = shift;
	my $key = shift;

	my $lessThan = sub {
		my $a = shift;
		my $b = shift;

		return ($a < $b) ? 1 : 0;
	};

	my $startingValue = 99999999;

	return FindNeighbouringAgentWithComparison($agent, $targets, $locations, $key, $lessThan, $startingValue);
}

sub FindNeighbourWithLargestValue {
	my $agent = shift;
	my $targets = shift;
	my $locations = shift;
	my $key = shift;

	my $greaterThan = sub {
		my $a = shift;
		my $b = shift;

		return ($a > $b) ? 1 : 0;
	};

	my $startingValue = -99999999;

	return FindNeighbouringAgentWithComparison($agent, $targets, $locations, $key, $greaterThan, $startingValue);
}

sub RemoveAgent {
	my $agentID = shift;
	my $agents = shift;
	my $locations = shift;
	my $multiArray = shift;
	my $replacementChar = shift // '.';

	my $agent = $agents->{$agentID};

	my $x = $agent->{x};
	my $y = $agent->{y};

	$multiArray->[$y][$x] = $replacementChar;

	delete $agents->{$agentID};
	delete $locations->{$y}{$x};
	if (scalar (keys %{$locations->{$y}}) == 0) {
		delete $locations->{$y};
	}
}

sub BreadthFirstSearchForTarget {
	my $multiArray = shift;
	my $agent = shift;
	my $target = shift;

	my $startingX = $agent->{x};
	my $startingY = $agent->{y};

	my $distance = 0;

	my $queue = {
		$startingY => {
			$startingX => {
				distance => $distance,
				dir => -1,
				prev => {
					y => -1,
					x => -1,
				},
			},
		},
	};

	my $searched = {};

	while (1) {
		# Check if queue contains target
		foreach my $y (sort { $a <=> $b } (keys %{$queue})) {
			foreach my $x (sort { $a <=> $b } (keys %{$queue->{$y}})) {
				# Add this to searched
				$searched->{$y}{$x} = {
					distance => $queue->{$y}{$x}{distance},
					dir => $queue->{$y}{$x}{dir},
					prev => {
						x => $queue->{$y}{$x}{prev}{x},
						y => $queue->{$y}{$x}{prev}{y},
					},
				};

				if ($multiArray->[$y][$x] eq $target) {
					my $prevX = $x;
					my $prevY = $y;

					while ($searched->{$prevY}{$prevX}{distance} > 1) {
						my $currentX = $prevX;
						my $currentY = $prevY;

						$prevX = $searched->{$currentY}{$currentX}{prev}{x};
						$prevY = $searched->{$currentY}{$currentX}{prev}{y};
					}

					my $direction = $searched->{$prevY}{$prevX}{dir};

					return ($direction, $distance);
				}
			}
		}

		$distance++;

		my $nextQueue = {};
		# Expand queue for next step
		foreach my $y (sort { $a <=> $b } (keys %{$queue})) {
			foreach my $x (sort { $a <=> $b } (keys %{$queue->{$y}})) {
				# Add neighbours to nextQueue
				foreach my $offset (@CL::ManhattanNeighbours) {
					my $currentX = $x + $offset->{x};
					my $currentY = $y + $offset->{y};

					my $charAtLocation = $multiArray->[$currentY][$currentX];

					my $searchableOrTargetableLocation = ($charAtLocation eq '.' || $charAtLocation eq $target) ? 1 : 0;
					my $alreadySearched = (defined $searched->{$currentY} && defined $searched->{$currentY}{$currentX}) ? 1 : 0;
					my $alreadyQueued = (defined $nextQueue->{$currentY} && defined $nextQueue->{$currentY}{$currentX}) ? 1 : 0;

					if ($searchableOrTargetableLocation && (not $alreadySearched) && (not $alreadyQueued)) {
						$nextQueue->{$currentY}{$currentX} = {
							distance => $distance,
							dir => $offset->{dir},
							prev => {
								x => $x,
								y => $y,
							},
						};
					}
				}
			}
		}

		if (keys %{$nextQueue} == 0) {
			# No more grid to explore
			return (-1, -1);
		}

		$queue = $nextQueue;
	}
}

1;