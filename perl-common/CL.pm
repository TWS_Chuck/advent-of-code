package CL;

use strict;
use warnings;

use Data::Dumper;

sub insertArrayIntoMultiArray {
	my $arrayRef = shift;
	my $multiArrayRef = shift;
	my $valueToStore = shift // 1;
	my $index = shift // 0;

	if ( $index == scalar @$arrayRef - 1 ) {
		@$multiArrayRef[@$arrayRef[$index]] = $valueToStore;
		return $multiArrayRef;
	}

	@$multiArrayRef[@$arrayRef[$index]] = insertArrayIntoMultiArray($arrayRef, @$multiArrayRef[@$arrayRef[$index]], $valueToStore, $index + 1);

	return $multiArrayRef;
}

sub checkMultiArrayForArray {
	my $arrayRef = shift;
	my $multiArrayRef = shift;
	my $index = shift // 0;

	if ($index == scalar @$arrayRef - 1) {
		return @$multiArrayRef[@$arrayRef[$index]];
	} else {
		my $result = checkMultiArrayForArray($arrayRef, @$multiArrayRef[@$arrayRef[$index]], $index + 1);
		$result = -1 if (not defined $result);
		return $result;
	}
}

sub compareArrays {
	my $firstArrayRef = shift;
	my $secondArrayRef = shift;
	my $separator = "\t";

	my $firstArrayStr = join $separator, @$firstArrayRef;
	my $secondArrayStr = join $separator, @$secondArrayRef;

	return ($firstArrayStr eq $secondArrayStr) ? 1 : 0;
}

sub importLinesFromFile {
	my $filename = shift // "file.txt";
	my $removeCharacters = shift;

	my @lines;

	open(my $fh, "<", $filename) or die "Failed to open file: $!\n";

	while(<$fh>) {
		chomp;
		my $line = $_;
		$line =~ s/[$removeCharacters]//g if defined $removeCharacters;
		push @lines, $line;
	}

	close $fh;

	return @lines;
}

sub importMultiArrayFromFile {
	my $filename = shift;
	my $removeCharacters = shift;

	my @multiArray;

	my @lines = importLinesFromFile($filename, $removeCharacters);

	for (my $row = 0; $row < scalar @lines; $row++) {
		my @lineElements = split //, $lines[$row];

		for (my $col = 0; $col < scalar @lineElements; $col++) {
			$multiArray[$row][$col] = $lineElements[$col];
		}
	}

	return @multiArray;
}

sub import2DGridFromFile {
	my $filename = shift;
	my $removeCharacters = shift;

	my $grid = {};

	my @lines = importLinesFromFile($filename, $removeCharacters);

	for (my $y = 0; $y < scalar @lines; $y++) {
		my @lineElements = split //, $lines[$y];

		for (my $x = 0; $x < scalar @lineElements; $x++) {
			$grid->{$x}{$y} = $lineElements[$x];
		}
	}

	return $grid;
}

sub importSingleLineFromFile {
	my $separator = shift // '\t';
	my $filename = shift // "file.txt";
	my $removeCharacters = shift;

	my @lines;

	open(my $fh, "<", $filename) or die "Failed to open file: $!\n";

	while(<$fh>) {
		chomp;
		my $line = $_;
		$line =~ s/[$removeCharacters]//g if defined $removeCharacters;
		push @lines, $line;
	}

	close $fh;

	return split /$separator/, $lines[0] if defined $lines[0];
}

sub permute {
	my $parts = shift;

	use Algorithm::Permute;

	my $p_iterator = Algorithm::Permute->new ( $parts );

	return $p_iterator;
}

sub cToB {
	my $num = int(shift);
	my $zeroPadding = shift;

	if ($zeroPadding) {
		return sprintf("%0".$zeroPadding."b", $num);
	} else {
		return sprintf("%b", $num);
	}
}

sub cToH {
	my $num = int(shift);
	my $zeroPadding = shift;

	if ($zeroPadding) {
		return sprintf("%0".$zeroPadding."x", $num);
	} else {
		return sprintf("%x", $num);
	}
}

# Given a the width and height of a section, will return an object that indicates the bottom right corner, and the actual sum
sub getLargestSectionOfMultiArray {
	my ($width, $height, $grid) = @_;

	my $subGridSums = {};
	my $xCoord;
	my $yCoord;
	my $maxPowerLevel = -9999999;

	my $i = $width - 1;
	while (defined $grid->{$i}) {
		my $j = $height - 1;
		while (defined $grid->{$i}{$j}) {
			my $gridPowerLevel = 0;
			if (defined $subGridSums->{$i-1}{$j}) {
				$gridPowerLevel = $subGridSums->{$i-1}{$j};
				for (my $y = 0; $y < $height; $y++) {
					$gridPowerLevel -= $grid->{$i-$width}{$j-$y};
					$gridPowerLevel += $grid->{$i}{$j-$y};
				}
			} elsif (defined $subGridSums->{$i}{$j-1}) {
				$gridPowerLevel = $subGridSums->{$i}{$j-1};
				for (my $x = 0; $x < $width; $x++) {
					$gridPowerLevel -= $grid->{$i-$x}{$j-$height};
					$gridPowerLevel += $grid->{$i-$x}{$j};
				}
			} else {
				# Add up all the things
				for (my $x = 0; $x < $width; $x++) {
					for (my $y = 0; $y < $height; $y++) {
						$gridPowerLevel += $grid->{$i - $x}{$j - $y};
					}
				}
			}
			$subGridSums->{$i}{$j} = $gridPowerLevel;
			
			if ($gridPowerLevel > $maxPowerLevel) {
				$maxPowerLevel = $gridPowerLevel;
				$xCoord = $i;
				$yCoord = $j;
			}
			$j++;
		}
		$i++;
	}

	return ($xCoord, $yCoord, $maxPowerLevel);
}

1;